import { Component, OnInit } from '@angular/core';
import { HorarioAtencionService } from '../../shared/services/horario-atencion.service'
import { HorarioAtencion } from '../../shared/models/horario-atencion';
import { Observable } from 'rxjs';
import {map, startWith, debounceTime, share} from 'rxjs/operators';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogPersonaComponent} from '../../reserva/dialog-persona/dialog-persona.component';

//import { Persona } from '../../persona';
import swal from 'sweetalert2';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Persona } from 'src/app/persona';
import { Time } from '@angular/common';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';

declare var $: any;
export class TableData {
  headerRow: String[];
  dataRows: HorarioAtencion[];
}

export class PersonaData {
  idPersona: number;
  nombre: string;
}

@Component({
  selector: 'app-ver-horarios',
  templateUrl: './ver-horarios.component.html',
  styleUrls: ['./ver-horarios.component.css']
})

export class VerHorariosComponent implements OnInit {

  public tableData: TableData;
  public horarios: HorarioAtencion[];
  public selectedHorario : HorarioAtencion;


  personaAux : PersonaData;
  empleadoId = null;
  empleadoNombre = '';

  filtrar = false;

  Empleados : Persona[] = [];
  seleccionado: any[4]; // Guardar filtros de busqueda para separar el filtro del paginado

  //paginacion
  public inicio: number = 0;
  public cantidad: number = 5;
  public pagina: number = 1;
  public totalDatos: number;
  public numPaginas: number; 
  
  filtradoForm = new FormGroup({
    dia: new FormControl('')
  });

  //empleadoOptions : Observable<Persona[]>;
  //DiaOptions :  Observable<any[]>;
  selectedEmpleado: Persona ;
  selectedDia : number;

  dias = [
    {value: 0, viewValue: 'Domingo'},
    {value: 1, viewValue: 'Lunes'},
    {value: 2, viewValue: 'Martes'},
    {value: 3, viewValue: 'Miercoles'},
    {value: 4, viewValue: 'Jueves'},
    {value: 5, viewValue: 'Viernes'},
    {value: 6, viewValue: 'Sabado'},
  ];


  constructor(
    private horarioService: HorarioAtencionService,
    private router: Router,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    //this.getEmpleados();
    this.getHorarios();
  
  }

  goToPage(pagina: number){
    this.pagina = pagina;
    this.inicio = this.cantidad * (pagina - 1 );
    this.getHorarios();
  }

  getHorarios(){

    this.horarioService.getHorarios(this.inicio, this.cantidad,this.filtrar,this.filtradoForm.value.dia,this.empleadoId).subscribe(
      data => {
        this.tableData = {
          headerRow: ['ID', 'Dia','HoraApertura', 'HoraCierre','IntervaloMinutos','Empleado','Acción'],
          dataRows: data.lista,
        }

        this.totalDatos = data.totalDatos;
        //console.log(this.totalDatos)

        this.numPaginas = Math.ceil(this.totalDatos / this.cantidad);
        //console.log(this.numPaginas);
      },

    )
  }

  openModalEliminar(id){
    swal({
      title: 'Desea eliminar este horario?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.horarioService.DeleteHorario(id).subscribe(
          data => this.modalEliminadoConExito(),
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El horario de atención se ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar el horario!",
      text: "Ocurrio un error al intentar eliminar el horario.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  openDialog(): void {
    
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;
      }
    });
  }

  buscar() {
    this.filtrar = true;
    this.inicio = 0;
    this.pagina = 1;
    this.getHorarios();
  }

  onReset(){
    this.filtrar = false;
    this.personaAux = null;
    this.empleadoNombre = "";
    this.empleadoId = null;
    this.filtradoForm.reset();
    this.getHorarios();
  }

  redirigir(id:number, page: string){
    if(page == 'add'){
      this.router.navigate(['horario-atencion/crear-horario']);
    }else{
      this.router.navigate(['horario-atencion/editar-horario', id = id]);
    }
  }

}
