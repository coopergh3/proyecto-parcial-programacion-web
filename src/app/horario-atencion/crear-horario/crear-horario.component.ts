import { Component, OnInit } from '@angular/core';
import { HorarioAtencion } from '../../shared/models/horario-atencion';
import { HorarioAtencionService } from '../../shared/services/horario-atencion.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { ListaPersona } from 'src/app/listaPersona';
import { Persona } from 'src/app/persona';
import { Time } from '@angular/common';
import { Title } from '@angular/platform-browser';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogPersonaComponent} from '../../reserva/dialog-persona/dialog-persona.component';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';

export class PersonaData {
  idPersona: number;
  nombre: string;
}


@Component({
  selector: 'app-crear-horario',
  templateUrl: './crear-horario.component.html',
  styleUrls: ['./crear-horario.component.css']
})

export class CrearHorarioComponent implements OnInit {

  Empleados : Persona[];
  body : HorarioAtencion;
  apertura: Time;
  cierre: Time;
  intervalo: number;

  //dialog
  personaAux : PersonaData;
  empleadoId = null;
  empleadoNombre = '';

  crearHorarioForm = new FormGroup({
    dia: new FormControl(''),
    horaAperturaCadena: new FormControl(''),
    horaCierreCadena: new FormControl(''),
    intervaloMinutos: new FormControl(''),
    idEmpleado: new FormControl(''),
  });
 
  dias = [
    {value: 0, viewValue: 'Domingo'},
    {value: 1, viewValue: 'Lunes'},
    {value: 2, viewValue: 'Martes'},
    {value: 3, viewValue: 'Miercoles'},
    {value: 4, viewValue: 'Jueves'},
    {value: 5, viewValue: 'Viernes'},
    {value: 6, viewValue: 'Sabado'},
  ];


  constructor(
    private horarioAtencionService: HorarioAtencionService,
    private dialog: MatDialog,
    private tituloService: Title,) { }

    ngOnInit() {
      this.tituloService.setTitle("Crear Horario");
    }

  onSubmit() {
    //this.body = this.crearHorarioForm.value as HorarioAtencion;
    
    //this.body.horaAperturaCadena = this.body.horaAperturaCadena.replace(/:/g,''); 
    console.log(this.body);
    this.horarioAtencionService.addHorario(this.crearHorarioForm.value).subscribe(
      data => this.creadoCorrectamente(data),
      error => this.errorRecibido(error)
    )
  }

  timeFormat(time): String{
    return time.replace(/:/g,'');
  }

  creadoCorrectamente(data: HorarioAtencion){
    console.log("Se ha añadido correctamente nuevo horario de atención "+data.diaCadena);
    swal({
        title: "Horario añadido!",
        text: "Se añadido correctamente el horario"+ data.dia +"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear el Horario: "+error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }


  getEmpleado(id){
    this.horarioAtencionService.getEmpleado(id).subscribe(
      data => { this.crearHorarioForm.get("idEmpleado").setValue(data); },
      error => { console.log(error); }
    )
  }

  openDialog(): void {
    
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;
        this.getEmpleado(result.id);
        
      }
    });
  }

}
