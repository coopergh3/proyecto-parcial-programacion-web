import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HorarioAtencionRoutingModule } from './horario-atencion-routing.module';
import { CrearHorarioComponent } from './crear-horario/crear-horario.component';

import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerHorariosComponent } from './ver-horarios/ver-horarios.component';
import { SubcategoriaModule } from '../subcategoria/subcategoria.module';
import { FiltroPipe } from './filtro.pipe';
import { ReservaModule } from '../reserva/reserva.module';
import { EditarHorarioComponent } from './editar-horario/editar-horario.component';


@NgModule({
  declarations: [CrearHorarioComponent, VerHorariosComponent, FiltroPipe, EditarHorarioComponent],
  imports: [
    CommonModule,
    HorarioAtencionRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SubcategoriaModule,
    ReservaModule
  ]
})
export class HorarioAtencionModule { }
