import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HorarioAtencion } from '../../shared/models/horario-atencion';
import { ListaHorarios } from '../../shared/models/ListaHorarios';
import swal from 'sweetalert2';
import { Location } from '@angular/common';
import {HorarioAtencionService } from '../../shared/services/horario-atencion.service';
import { Persona } from '../../persona';

@Component({
  selector: 'app-editar-horario',
  templateUrl: './editar-horario.component.html',
  styleUrls: ['./editar-horario.component.css']
})
export class EditarHorarioComponent implements OnInit {

  editarHorarioForm = new FormGroup({
    idPersonaHorarioAgenda: new FormControl(''),
    dia : new FormControl(''),
    horaAperturaCadena: new FormControl(''),
    horaCierreCadena: new FormControl(''),
    intervaloMinutos: new FormControl(''),
    idEmpleado: new FormControl('')
  });

  id: number;
  Horario: HorarioAtencion;
  nombreEmpleado: String;

  dias = [
    {value: 0, viewValue: 'Domingo'},
    {value: 1, viewValue: 'Lunes'},
    {value: 2, viewValue: 'Martes'},
    {value: 3, viewValue: 'Miercoles'},
    {value: 4, viewValue: 'Jueves'},
    {value: 5, viewValue: 'Viernes'},
    {value: 6, viewValue: 'Sabado'},
  ];

  constructor(
    private horarioService: HorarioAtencionService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });

    this.getHorario(this.id);
  }

  timeFormat(time): String{
    return time.slice(0,5);
  }

  getHorario(id){
    this.horarioService.getHorario(id).subscribe(res => 
      { this.Horario = res;
        this.nombreEmpleado = res.idEmpleado.nombreCompleto;
        this.editarHorarioForm.setValue({
          idPersonaHorarioAgenda: res.idPersonaHorarioAgenda,
          dia: res.dia,
          horaAperturaCadena : this.timeFormat(res.horaApertura),
          horaCierreCadena: this.timeFormat(res.horaCierre),
          intervaloMinutos: res.intervaloMinutos,
          idEmpleado: res.idEmpleado,
        })
        console.log(this.editarHorarioForm.value);
  
      },
        error => {console.log(error);}
      )
  }

  onSubmit(){
    /*this.Horario.dia = this.editarHorarioForm.value.dia;
    this.Horario.horaAperturaCadena = this.editarHorarioForm.value.horaAperturaCadena ;
    this.Horario.horaCierreCadena = this.editarHorarioForm.value.horaCierreCadena;
    this.Horario.intervaloMinutos = this.editarHorarioForm.value.intervaloMinutos;*/
    //this.Horario.idEmpleado = this.editarHorarioForm.value.idEmpleado;

    this.horarioService.UpdateHorario(this.editarHorarioForm.value).subscribe(
      res => this.editadoCorrectamente(res),
      error => this.errorRecibido(error)
    )
  }

  editadoCorrectamente(data: HorarioAtencion){
    console.log("Se ha editado correctamente el horairo de atención"+ data);
    swal({
          title: "Horario Modificado!",
          text: "Se edito correctamente el horario+data.idPersonaHorarioAgenda",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al editar el horario.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  goBack(){
    this.location.back();
  }  

}
