import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearHorarioComponent } from './crear-horario/crear-horario.component';
import { VerHorariosComponent } from './ver-horarios/ver-horarios.component';
import { EditarHorarioComponent } from './editar-horario/editar-horario.component';

const routes: Routes = [
  {
    path: '',
    children: [ {
      path: 'crear-horario',
      component: CrearHorarioComponent
    }]
  },
  {
    path: '',
    children: [ {
      path: 'ver-horarios',
      component: VerHorariosComponent
    }]
  },
  {
    path: '',
    children: [ {
      path: 'editar-horario/:id',
      component: EditarHorarioComponent
    }]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HorarioAtencionRoutingModule { }
