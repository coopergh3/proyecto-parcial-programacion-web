import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajesProductosComponent } from './mensajes-productos.component';

describe('MensajesProductosComponent', () => {
  let component: MensajesProductosComponent;
  let fixture: ComponentFixture<MensajesProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MensajesProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajesProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
