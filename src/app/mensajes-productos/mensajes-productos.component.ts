import { Component, OnInit } from '@angular/core';
import { MensajesProductosService } from '../shared/services/mensajes-productos.service';

@Component({
  selector: 'app-mensajes-productos',
  templateUrl: './mensajes-productos.component.html',
  styleUrls: ['./mensajes-productos.component.css']
})
export class MensajesProductosComponent implements OnInit {

  //public el servicio de mensajes de producto porque se va a inyectar en el template
  //Angular only binds to public component properties.
  constructor(public mensajesProductosService: MensajesProductosService,
  ) {

  }

  ngOnInit() {
  }

}
