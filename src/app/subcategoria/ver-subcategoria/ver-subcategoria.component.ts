import { Component, OnInit } from '@angular/core';
import { SubcategoriaService } from 'src/app/shared/services/subcategoria.service';
import { ListaSubcategoria } from 'src/app/shared/models/lista-subcategoria';
import { Subcategoria } from 'src/app/shared/models/subcategoria';
import { Categoria } from '../../shared/models/categoria';
import swal from 'sweetalert2';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import { runInThisContext } from 'vm';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { Title } from '@angular/platform-browser';
declare var $: any;
export class TableData {
  headerRow: String[];
  dataRows: Subcategoria[];
}

@Component({
  selector: 'app-ver-subcategoria',
  templateUrl: './ver-subcategoria.component.html',
  styleUrls: ['./ver-subcategoria.component.css']
})
export class VerSubcategoriaComponent implements OnInit {

  public tableData: TableData;
  public subCategorias: Subcategoria[];
  public selectedSubcategoria : Subcategoria;

  //paginacion
  public inicio: number = 0;
  public cantidad: number = 5;
  public pagina: number = 1;
  public totalDatos: number;
  public numPaginas: number; 

  //orden
  public orden  = 'asc';

  filtrar = false;
  desc = "";
  cat: Categoria;

  filtradoForm = new FormGroup({
    descripcion: new FormControl(''),
    idCategoria: new FormControl('')
  });

  editarSubcategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
  });
  Categorias: any;
  constructor(
    private subcategoriaService: SubcategoriaService,
    private router: Router,
    private route: ActivatedRoute,
    private recibirCategoriaService: RecibirCategoriaService,
    private tituloService: Title
  ) { }

  ngOnInit() {
    this.tituloService.setTitle("Ver Subcategoria");
    this.getSubCategorias();
    this.getCategorias();

    this.filtradoForm.get('descripcion').valueChanges.subscribe(
      data => { this.desc = data; this.buscar(); }
    );
  }

  errorRecibido(error){
    swal({
      title: "No se pudo obtener las subcategorias!",
      text: "Ocurrio un error al intentar obtener las subcategorias.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  goToPage(pagina: number){
    this.pagina = pagina;
    this.inicio = this.cantidad * (pagina - 1 );
    this.getSubCategorias();
  }

  getCategorias(): void {
    this.recibirCategoriaService.getCategorias().subscribe(
      data => this.Categorias = data.lista
    );
  }

  getSubCategorias(){

    this.subcategoriaService.getSubCategorias(this.inicio,this.cantidad,this.filtrar,this.desc, this.filtradoForm.get('idCategoria').value).subscribe(
      data => {
        
        this.tableData = {
          headerRow: ['ID', 'Descripcion','Categoria', 'Opciones'],
          dataRows: data.lista,
        }
        this.totalDatos = data.totalDatos;
        this.numPaginas = Math.ceil(this.totalDatos / this.cantidad);
        //console.log("dataRow[0]",this.tableData.dataRows[0]);
      },
      error => this.errorRecibido(error)
    );
  }
  

  openModalEliminar(id){
    swal({
      title: 'Desea eliminar esta subcategoria?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.subcategoriaService.DeleteSubCategoria(id).subscribe(
          data => this.modalEliminadoConExito(),
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'La subcategoria ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar la categoria!",
      text: "Ocurrio un error al intentar eliminar la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  redirigir(id:number){
    this.router.navigate(['subcategoria/modificar-subcategoria', id = id]);
  }

  buscar() {
    this.filtrar = true;
    this.inicio = 0;
    this.pagina = 1;
    this.getSubCategorias();
  }

  onReset(){
    this.filtrar = false;
    this.filtradoForm.reset();
    this.getSubCategorias();
  }
}
