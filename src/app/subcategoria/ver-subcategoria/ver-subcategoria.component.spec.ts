import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerSubcategoriaComponent } from './ver-subcategoria.component';

describe('VerSubcategoriaComponent', () => {
  let component: VerSubcategoriaComponent;
  let fixture: ComponentFixture<VerSubcategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerSubcategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerSubcategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
