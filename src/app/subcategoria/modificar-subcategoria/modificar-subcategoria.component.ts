import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SubcategoriaService } from '../../shared/services/subcategoria.service';
import { Subcategoria } from '../../shared/models/subcategoria';
import swal from 'sweetalert2';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-modificar-subcategoria',
  templateUrl: './modificar-subcategoria.component.html',
  styleUrls: ['./modificar-subcategoria.component.css']
})
export class ModificarSubcategoriaComponent implements OnInit {

  editarSubcategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
    idCategoria: new FormControl({value:'', disabled: true})
  });
  id: number;
  subcategoria: Subcategoria;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private subCategoriaService: SubcategoriaService,
    private location: Location,
    private tituloService: Title
  ) {}

  
  ngOnInit() {
    this.tituloService.setTitle("Modificar Subcategoria");
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });

    this.getSubcategoria(this.id);
  }

  getSubcategoria(id){
    this.subCategoriaService.getSubCategoria(id).subscribe(res => 
    { this.subcategoria = res;
      this.editarSubcategoriaForm.setValue({
        descripcion: res.descripcion,
        idCategoria: res.idCategoria.descripcion
      })
      console.log(res);

    },
      error => this.errorRecibido(error)
    )
  }

  onSubmit(){
    this.subcategoria.descripcion = this.editarSubcategoriaForm.value.descripcion;
    this.subCategoriaService.UpdateSubCategoria(this.subcategoria).subscribe(
      res => this.editadoCorrectamente(res),
      error => this.errorRecibido(error)
    )
  }

  editadoCorrectamente(data: Subcategoria){
    console.log("Se ha editado correctamente la subcategoris"+ data);
    swal({
          title: "Subcategoria Modificada!",
          text: "Se edito correctamente la subcategoria2+data.descripcion",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al editar la subcategoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  goBack(){
    this.location.back();
  }  

}
