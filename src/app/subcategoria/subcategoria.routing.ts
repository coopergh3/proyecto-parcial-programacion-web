import { Routes } from '@angular/router';

import { CrearSubcategoriaComponent } from './crear-subcategoria/crear-subcategoria.component';
import { ModificarSubcategoriaComponent } from './modificar-subcategoria/modificar-subcategoria.component';
import { VerSubcategoriaComponent } from './ver-subcategoria/ver-subcategoria.component';

export const subCategoriaRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'crear-subcategoria',
            component: CrearSubcategoriaComponent
        }]
    }, {
        path: '',
        children: [ {
        path: 'modificar-subcategoria/:id',
        component: ModificarSubcategoriaComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'ver-subcategoria',
            component: VerSubcategoriaComponent
        }]
    }
];
