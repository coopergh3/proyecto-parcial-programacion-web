import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearSubcategoriaComponent } from './crear-subcategoria.component';

describe('CrearSubcategoriaComponent', () => {
  let component: CrearSubcategoriaComponent;
  let fixture: ComponentFixture<CrearSubcategoriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearSubcategoriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearSubcategoriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
