import { Component, OnInit } from '@angular/core';
import { SubcategoriaService } from '../../shared/services/subcategoria.service';
import { Subcategoria } from '../../shared/models/subcategoria';
import { Categoria } from '../../shared/models/categoria';
import { ListaCategoria } from '../../shared/models/lista-categorias';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';

import { RecibirCategoriaService } from '../../shared/services/recibir-categoria.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-crear-subcategoria',
  templateUrl: './crear-subcategoria.component.html',
  styleUrls: ['./crear-subcategoria.component.css']
})

export class CrearSubcategoriaComponent implements OnInit {

  Categorias: any[];

  crearSubCategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
    idCategoria: new FormControl('')
  })

  constructor(
    private subCategoriaService : SubcategoriaService,
    private recibirCategoriaService: RecibirCategoriaService,
    private tituloService: Title
  ) { }

  onSubmit(){
    this.subCategoriaService.crearSubCategoria(this.crearSubCategoriaForm.value).subscribe(
      data => this.creadoCorrectamente(data),
      error => this.errorRecibido(error)
    )
  }

  creadoCorrectamente(data: Subcategoria){
    console.log("Se ha creado correctamente la subcategoris"+ data);
    swal({
          title: "Subcategoria creada!",
          text: "Se creo correctamente la subcategoria2+data.descripcion",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success"
    }).catch(swal.noop)
  } 

  errorRecibido(error){
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear la subcategoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  getCategorias(): void {
    this.recibirCategoriaService.getCategorias().subscribe(
      data => this.Categorias = data.lista
    );
  }
  ngOnInit() {
    this.tituloService.setTitle("Crear Subcategoria");
    this.getCategorias();
  }

}
