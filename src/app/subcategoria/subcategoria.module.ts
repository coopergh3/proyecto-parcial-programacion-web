import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { subCategoriaRoutes } from './subcategoria.routing';
import { CrearSubcategoriaComponent } from './crear-subcategoria/crear-subcategoria.component';
import { ModificarSubcategoriaComponent } from './modificar-subcategoria/modificar-subcategoria.component';
import { VerSubcategoriaComponent } from './ver-subcategoria/ver-subcategoria.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginacionComponent } from './paginacion/paginacion.component';

@NgModule({
  declarations: [CrearSubcategoriaComponent, ModificarSubcategoriaComponent, VerSubcategoriaComponent, PaginacionComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(subCategoriaRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [PaginacionComponent]
})
export class SubcategoriaModule { }
