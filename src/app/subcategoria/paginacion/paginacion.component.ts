import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.css']
})
export class PaginacionComponent implements OnInit {

  @Input() private pagina: number;
  @Input() private numPaginas: number;
  @Input() private totalDatos: number;
  @Output() paginaEmitter: EventEmitter<number> = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  siguiente(){
    this.pagina++;
    this.pasarPagina();
  }
  anterior(){
    this.pagina--;
    this.pasarPagina();
  }
  pasarPagina(){
    this.paginaEmitter.emit(this.pagina);
  }
}
