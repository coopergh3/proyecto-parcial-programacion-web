import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {
  ListaServicios, Servicio, IdFichaClinica, IdCliente, IdLocal, IdEmpleado, IdTipoProducto, DetalleServicio
} from '../shared/models/lista-servicios';
import { Observable } from 'rxjs';
import { Persona } from '../persona';
import { ListaPersona } from '../listaPersona';
import { ListaProductos } from '../shared/models/producto';
import { ListaExistenciaProductos } from '../shared/models/producto';
import { Subcategoria } from '../shared/models/subcategoria';
import { ListaSubcategoria } from '../shared/models/lista-subcategoria';

@Injectable({
  providedIn: 'root'
})
export class ListarServiciosService {

  private servicioUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/servicio";  // URL to web api
  private personaUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/persona";
  private productoUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto";

  constructor(private http: HttpClient) { }


  getServicioPorId(idServicio:number): Observable<Servicio> {
    let url = `${this.servicioUrl}/${idServicio}`
    return this.http.get<Servicio>(url);
  }

  getListaServicios(): Observable<ListaServicios> {
    //return this.http.get<ListaProductos>(this.productoUrlOrderByIdProducto);
    return this.http.get<ListaServicios>(this.servicioUrl, {
      params: new HttpParams().set('orderBy', 'idFichaClinica').set('orderDir', 'asc')
    });
  }

  getlistaServiciosAllParams(empleado: Persona, cliente: Persona, fecha: string): Observable<ListaServicios> {
    return this.http.get<ListaServicios>(this.servicioUrl,
      {
        params: new HttpParams()
          .set('ejemplo', `{"idFichaClinica":{"idEmpleado":{"idPersona":${empleado.idPersona}}, "idCliente":{"idPersona":${cliente.idPersona}}}, "fechaHastaCadena":"${fecha}"}`)
          .set('orderBy', 'idServicio')
          .set('orderDir', 'asc')
      }
    );

  }

  crearServicioAllParams(idFichaClinica: number, observacion: string): Observable<any> {
    let obj = {
      "idFichaClinica": {
        "idFichaClinica": `${idFichaClinica}`
      },
      "observacion": `${observacion}`
    };
    let json = JSON.stringify(obj);

    return this.http.post(this.servicioUrl, json,
      {
        headers: new HttpHeaders().
          set('usuario', 'ana').
          set('Content-Type', 'application/json')
      }
    );
  }

  getServiciosFicha(idFicha): Observable<ListaServicios>{
    let params = new HttpParams();
    params = params.set("ejemplo",`{
      "idFichaClinica":{
        "idFichaClinica": ${idFicha}
      }
    }`)
    return this.http.get<ListaServicios>(this.servicioUrl, {params});
  }

  getListaPersonas(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.personaUrl);
  }

  getListaServiciosParaServicioNuevo(idFichaClinica: number): Observable<ListaServicios> {
    return this.http.get<ListaServicios>(this.servicioUrl, {
      params: new HttpParams()
        .set('ejemplo', `{"idFichaClinica":{"idFichaClinica":${idFichaClinica}}}`)
    });
  }

  //
  getServiciosEmpleadosParams(empleado: Persona, fechaDesdeCadena: string, fechaHastaCadena: string): Observable<ListaServicios> {
    return this.http.get<ListaServicios>(this.servicioUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"idEmpleado":{"idPersona": ${empleado.idPersona}}, 
      "fechaDesdeCadena":"${fechaDesdeCadena}","fechaHastaCadena":"${fechaHastaCadena}"}`)
      /*
      ,headers: new HttpHeaders().
        set('usuario', 'ana')
        */
    });
  }

  getServiciosClientesParams(cliente: Persona, fechaDesdeCadena: string, fechaHastaCadena: string): Observable<ListaServicios> {
    return this.http.get<ListaServicios>(this.servicioUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"idFichaClinica":{"idCliente":{"idPersona": ${cliente.idPersona}}}, 
      "fechaDesdeCadena":${fechaDesdeCadena},"fechaHastaCadena":${fechaHastaCadena}}`)
    });
  }

  getListaEmpleados(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.personaUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"soloUsuariosDelSistema":true}`)
        .set("orderBy", "nombre")
        .set("orderDir", "asc")
    }
    )
  }


  getListaProductos(idCategoria:number, idTipoProducto:number): Observable<ListaProductos> {
    //return this.http.get<ListaProductos>(this.productoUrlOrderByIdProducto);
    return this.http.get<ListaProductos>(this.productoUrl, {
      params: new HttpParams()
        .set('ejemplo',`{"idProducto":{"idTipoProducto":{"idTipoProducto":${idTipoProducto}, "idCategoria":{"idCategoria":${idCategoria}}}}}}`)
        .set('orderBy', 'idPresentacionProducto')
        .set('orderDir', 'asc')
    });
  }

  getPrecioProducto(idPresentacionProducto: number): Observable<ListaExistenciaProductos> {
    //console.log(`idPresentacionProducto llego ${idPresentacionProducto}`);
    let url = 'https://gy7228.myfoscam.org:8443/stock-pwfe/existenciaProducto';
    return this.http.get<ListaExistenciaProductos>(url, {
      params: new HttpParams().
        set('ejemplo', `{"idPresentacionProductoTransient":${idPresentacionProducto}}`),
      headers: new HttpHeaders().
        set('usuario', 'ana')
    }
    );
  }

  agregarDetalleServicio(cantidad: number, idPresentacionProducto: number, idServicio: number): Observable<any> {
    let url = `https://gy7228.myfoscam.org:8443/stock-pwfe/servicio/${idServicio}/detalle`;
    console.log('cantidad=',cantidad);
    let obj = {
      "cantidad": `${cantidad}`,
      "idPresentacionProducto":
      {
        "idPresentacionProducto": `${idPresentacionProducto}`
      },
      "idServicio": {
        "idServicio": `${idServicio}`
      }
    };
    let json = JSON.stringify(obj);
    console.log(json);
    return this.http.post(url, json, {
      headers: new HttpHeaders()
        .set('usuario', 'ana')
        .set('Content-Type', 'application/json')
    });
  }

  getListaDetalleServicio(idServicio:number): Observable<DetalleServicio[]>{
    let url = `https://gy7228.myfoscam.org:8443/stock-pwfe/servicio/${idServicio}/detalle`;
    return this.http.get<DetalleServicio[]>(url, {
      params: new HttpParams()
      .set('orderBy','idServicioDetalle')
      .set('orderDir','asc')
    });
  }

  getListaDetalleParaReporte(idServicio:number):Observable<any[]>{
    let url = `https://gy7228.myfoscam.org:8443/stock-pwfe/servicio/${idServicio}/detalle`;
    return this.http.get<any[]>(url, {
      params: new HttpParams()
      .set('orderBy','idServicioDetalle')
      .set('orderDir','asc')
    });
  }

  getListaSubcategoriasPorIdCategoria(idCategoria:number): Observable<ListaSubcategoria>{
    let url = 'https://gy7228.myfoscam.org:8443/stock-pwfe/tipoProducto';
    return this.http.get<ListaSubcategoria>(url,{
      params: new HttpParams()
      .set('ejemplo',`{"idCategoria":{"idCategoria":${idCategoria}}}`)
    });
  }



  eliminarDetalleServicio(idServicio:number, idDetalleServicio:number): Observable<Number>{
    let url = `https://gy7228.myfoscam.org:8443/stock-pwfe/servicio/${idServicio}/detalle/${idDetalleServicio}`;
    return this.http.delete<number>(url);
  }


  getlistaServiciosParaPantallaVerServicios(empleado: Persona, cliente: Persona, fechaDesdeCadena: string, fechaHastaCadena:string): Observable<ListaServicios> {
    return this.http.get<ListaServicios>(this.servicioUrl,
      {
        params: new HttpParams()
          .set('ejemplo', `{"idFichaClinica":{"idEmpleado":{"idPersona":${empleado.idPersona}}, "idCliente":{"idPersona":${cliente.idPersona}}}, "fechaDesdeCadena":"${fechaDesdeCadena}" ,"fechaHastaCadena":"${fechaHastaCadena}"}`)
          .set('orderBy', 'idServicio')
          .set('orderDir', 'asc')
      }
    );

  }

}
