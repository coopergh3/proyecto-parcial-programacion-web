import { Component, OnInit, ViewChild, ElementRef, Inject, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ListarServiciosService } from '../listar-servicios.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ListaServicios, Servicio, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria
} from '../../shared/models/lista-servicios';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona'
import { ListaPersona } from '../../listaPersona'
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { DialogPersonaComponent } from 'src/app/reserva/dialog-persona/dialog-persona.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-ver-servicios',
  templateUrl: './ver-servicios.component.html',
  styleUrls: ['./ver-servicios.component.css']
})
export class VerServiciosComponent implements OnInit {
  displayedColumns: string[] = ['fecha', 'idFicha', 'idServicio' ,'fechaFicha', 'profesional', 'cliente',
    'categoria', 'subcategoria', 'acciones'];
  dataSource: MatTableDataSource<Servicio>;
  listaServicios: ListaServicios;
  serviciosArray: Servicio[];
  formGroup: FormGroup;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;

  /*Filtrado */
  /*Filtrado */
  opcionSeleccionada = 'Empleado';
  empleadoSeleccionado: Persona;
  clienteSeleccionado: Persona;
  empleadoFiltrado: Persona;
  clienteFiltrado: Persona;

  public listaFiltro = [
    { value: 'Empleado', viewValue: 'Empleado' },
    { value: 'Cliente', viewValue: 'Cliente' }
  ];

  mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic']; // Enum para los meses del 1 al 12 el 0 esta de relleno
  seleccionado: any[4]; // Guardar filtros de busqueda para separar paginado del filtro
  fechaDesde: Date;
  fechaHasta: Date;
  observacion: string;
  /*End Filtrado */
  seleccionarIdFichaFlag: boolean;
  /*End Seleccion de fila*/
  hoy: Date;



  /*End Filtrado */

  constructor(private listarServicioService: ListarServiciosService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public tituloService: Title) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista de Servicios");
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      fecha: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
    });
    this.listarServicioService.getListaServicios().subscribe(
      data => {
        this.listaServicios = {} as ListaServicios;
        this.listaServicios.lista = {} as Servicio[];
        this.listaServicios.lista = data.lista;
        this.serviciosArray = this.listaServicios.lista;
        //console.log('arrayServicios=>', this.serviciosArray);
        this.dataSource = new MatTableDataSource(this.serviciosArray);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => console.log(error)
    );
  }

  buscar() {
    const desde = this.getFormatoFecha(this.formGroup.get('fechaDesde').value.toString());
    const hasta = this.getFormatoFecha(this.formGroup.get('fechaHasta').value.toString());
    console.log(`fechaDesde=${desde} , fechaHasta=${hasta} , this.empleadoSeleccionado=${this.empleadoSeleccionado}, this.clienteSeleccionado=${this.clienteSeleccionado}`);
    this.listarServicioService.getlistaServiciosParaPantallaVerServicios(
      this.empleadoSeleccionado, this.clienteSeleccionado, desde, hasta).subscribe(
      data => {
        console.log(`data de Empleado = ${data}`);
        this.dataSource.data = data.lista;

      },
      error => console.log("el error es =====>", error)
    );
  }

  agregarServicio(): void {
    console.log("agregando Servicio...");
    this.router.navigate(['servicio', 'crear-servicios', '']);
  }

  reset() {
    if (this.opcionSeleccionada === 'Empleado') {
      this.clienteFiltrado = null;
    } else {
      this.empleadoFiltrado = null;
    }
  }

  limpiar() {
    this.clienteFiltrado = null;
    this.empleadoFiltrado = null;
    console.log('antes del patchValue...');
    console.log('fecha desde=', this.formGroup.controls['fechaDesde'].value);
    console.log('fecha hasta=', this.formGroup.controls['fechaHasta'].value);
    this.formGroup.patchValue({
      fechaDesde: '',
      fechaHasta: ''
    });
    console.log('Despues del patchValue....');
    console.log('fecha desde=', this.formGroup.controls['fechaDesde'].value);
    console.log('fecha hasta=', this.formGroup.controls['fechaHasta'].value);
  }

  checkFechaDesde(e) {
    const desde = new Date(e.value.toString());
    const hasta = new Date(this.formGroup.get('fechaHasta').value.toString());

    if (desde > hasta) {
      this.formGroup.get('fechaHasta').setValue(desde);
    }
    this.fechaDesde = e.value;
  }

  getFormatoFecha(s: string): string {
    const anho = s.toString().substring(11, 15);
    let mesElegido = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mesElegido = (+mesElegido < 10) ? '0' + mesElegido : mesElegido;
    const dia = s.toString().substring(8, 10);
    return (anho + mesElegido + dia);
  }

  /**Parte delos dialogs**/
  openDialog(opcionSeleccionada: string): void {
    this.opcionSeleccionada = opcionSeleccionada;
    if (this.opcionSeleccionada === "Empleado") {
      this.empleadoSeleccionado = {} as Persona;
      const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      //this.dialog.open(SeleccionarEmpleadoDialog, {
        width: '250px',
        data: {id: this.empleadoSeleccionado.idPersona, nombre: this.empleadoSeleccionado.nombre}
        //data: { persona: this.empleadoSeleccionado }/*{name: this.name, animal: this.animal}*/
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('Empleado =>', result);
        this.empleadoFiltrado = {} as Persona;
        this.empleadoFiltrado.idPersona = result.id;
        this.empleadoFiltrado.nombre = result.nombre;
        this.empleadoSeleccionado = this.empleadoFiltrado;
      });
      //si no se trata de una persona normal
    } else if (this.opcionSeleccionada === "Cliente") {
      this.clienteSeleccionado = {} as Persona;
      const dialogRef = this.dialog.open(DialogPersonaComponent, {
      //const dialogRef = this.dialog.open(SeleccionarPersonaDialog, {
        width: '250px',
        data: { id: this.clienteSeleccionado.idPersona, nombre: this.clienteSeleccionado.nombre }/*{name: this.name, animal: this.animal}*/
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Cliente =>', result);
        this.clienteFiltrado = {} as Persona;
        this.clienteFiltrado.idPersona = result.id;
        this.clienteFiltrado.nombre = result.nombre;
        this.clienteSeleccionado = this.clienteFiltrado;
        //this.clienteSeleccionado = this.clienteFiltrado = result;
      });
    }

  }

  pantallaEdicion(servicio:Servicio){
    console.log(servicio);
    this.router.navigate(['servicio', 'editar-servicios', servicio.idServicio]);
  }

}

/**
 * Dialog de Cliente */
@Component({
  selector: 'app-seleccionar-persona-dialog',
  templateUrl: './seleccionar-persona-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarPersonaDialog {

  listaPersonas: ListaPersona;
  arrayClientes: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarPersonaDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarServiciosService: ListarServiciosService
  ) { }

  ngOnInit(): void {
    this.listarServiciosService.getListaPersonas().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayClientes = [] as Persona[];
        this.listaPersonas.lista.forEach(element => {
          if (!element.usuarioLogin) {
            this.arrayClientes.push(element);
          }
        })
        console.log(this.arrayClientes);
      },
      error => console.log("error al listar clientes:", error)
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}


/**
 * Dialog de Empleado
 */
@Component({
  selector: 'app-seleccionar-empleado-dialog',
  templateUrl: './seleccionar-empleado-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarEmpleadoDialog implements OnInit {

  listaPersonas: ListaPersona;
  arrayEmpleados: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarEmpleadoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarServiciosService: ListarServiciosService
  ) { }

  ngOnInit(): void {
    this.listarServiciosService.getListaEmpleados().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayEmpleados = this.listaPersonas.lista;
        console.log(this.arrayEmpleados);
      },
      error => console.log("error al listar Empleados:", error)
    )
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
}

