import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VerServiciosComponent, SeleccionarPersonaDialog, SeleccionarEmpleadoDialog } from './ver-servicios/ver-servicios.component';
import { ServiciosDatasourceComponent } from './servicios-datasource/servicios-datasource.component';
import { ServicioRoutes } from './servicio.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule} from '@angular/material';
import { CrearServiciosComponent } from './crear-servicios/crear-servicios.component';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { EditarServiciosComponent } from './editar-servicios/editar-servicios.component';
import {ReservaModule} from '../reserva/reserva.module';

@NgModule({
  declarations: [VerServiciosComponent, ServiciosDatasourceComponent, CrearServiciosComponent, EditarServiciosComponent],
  entryComponents: [VerServiciosComponent, CrearServiciosComponent, EditarServiciosComponent],
  providers: [DatePipe],
//
  imports: [
    CommonModule,
    RouterModule.forChild(ServicioRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    //BrowserAnimationsModule
        //BrowserAnimationsModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        AngularDateTimePickerModule,
    ReservaModule,
  ]
})
export class ServicioModule { }
