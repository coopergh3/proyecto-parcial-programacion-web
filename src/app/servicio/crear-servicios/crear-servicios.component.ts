import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef, Inject, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ListarServiciosService } from '../listar-servicios.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ListaServicios, Servicio, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria, DetalleServicio
} from '../../shared/models/lista-servicios';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona';
import { ListaPersona } from '../../listaPersona';
import { SeleccionarEmpleadoDialog, SeleccionarPersonaDialog } from '../ver-servicios/ver-servicios.component';
import { DatePipe } from '@angular/common';
import swal from 'sweetalert2';
import { Producto } from 'src/app/shared/models/producto';
import { Categoria } from 'src/app/shared/models/categoria';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { SubcategoriaService } from 'src/app/shared/services/subcategoria.service';
import { Subcategoria } from 'src/app/shared/models/subcategoria';
import { Title } from '@angular/platform-browser';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { DialogPersonaComponent } from 'src/app/reserva/dialog-persona/dialog-persona.component';

@Component({
  selector: 'app-crear-servicios',
  templateUrl: './crear-servicios.component.html',
  styleUrls: ['./crear-servicios.component.css']
})
export class CrearServiciosComponent implements OnInit {

  displayedColumns: string[] = ['idFicha', 'fechaFicha', 'profesional', 'cliente',
    'categoria', 'subcategoria', 'acciones'];
  dataSource: MatTableDataSource<Servicio>;
  listaServicios: ListaServicios;
  serviciosArray: Servicio[];
  idFichaClinicaParam: number;
  formGroup: FormGroup;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;

  /*Filtrado */
  opcionSeleccionada = 'Empleado';
  empleadoSeleccionado: Persona;
  clienteSeleccionado: Persona;
  empleadoFiltrado: Persona;
  clienteFiltrado: Persona;

  public listaFiltro = [
    { value: 'Empleado', viewValue: 'Empleado' },
    { value: 'Cliente', viewValue: 'Cliente' }
  ];

  mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic']; // Enum para los meses del 1 al 12 el 0 esta de relleno
  seleccionado: any[4]; // Guardar filtros de busqueda para separar paginado del filtro
  fechaDesde: Date;
  fechaHasta: Date;
  observacion: string;
  /*End Filtrado */
  seleccionarIdFichaFlag: boolean;
  idServicioCreado: number;
  /*End Seleccion de fila*/

  /* Detalles Servicio */
  subcategoriaDetalleServicio: Subcategoria;
  subcategoriaDetalleServicioFlag: boolean = false;
  arraySubcategorias: Subcategoria[];
  categoriaDetalleServicio: Categoria;
  categoriaDetalleServicioFlag: boolean = false;
  arrayCategorias: Categoria[];
  idPresentacionProductoDetalleServicio: number;
  precioPresentacionProductoDetalleServicio: number;
  arrayProductos: Producto[];
  cantidadProductoDetalleServicio: number;
  IdServicioSeleccionado: number;
  arrayDetalleServicio: DetalleServicio[];
  displayedColumnsDetallesServicio: string[] = ['idDetalle', 'presentacion', 'precioUnitario',
    'cantidad', 'total', 'acciones'];
  dataSourceDetalleServicio: MatTableDataSource<DetalleServicio>;
  seleccionarIdServicioFlag: boolean = false;
  @ViewChild(MatPaginator, { static: false }) detalleServicioPaginator: MatPaginator;
  servicioSeleccionado: Servicio;
  
  /*End Detalles Servicio */

  constructor(
    private listarServicioService: ListarServiciosService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public datepipe: DatePipe,
    private listarCategoriaService: RecibirCategoriaService,
    private listarSubcategoriaService: SubcategoriaService,
    private tituloService: Title

  ) { }//fin constructor

  ngOnInit() {
    this.tituloService.setTitle("Crear Servicio");
    this.listarCategoriaService.getCategorias().subscribe(
      data => {
        this.arrayCategorias = data.lista;
        console.log(this.arrayCategorias);
      }
      , error => console.log(error.error)
    );

    this.route.params.subscribe(params => {
      this.idFichaClinicaParam = params.idFichaClinica;
      console.log(params);
      //si llego un parametro de id ficha
      if (this.idFichaClinicaParam) {
        this.seleccionarIdFichaFlag = false;
      } else {
        this.seleccionarIdFichaFlag = true;
      }
    });
    console.log(`idFichaClinica llego:${this.idFichaClinicaParam}`);
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
      observacion: new FormControl(''),
    });
    if (!this.idFichaClinicaParam) {
      this.listarServicioService.getListaServicios().subscribe(
        data => {
          this.listaServicios = {} as ListaServicios;
          this.listaServicios.lista = {} as Servicio[];
          this.listaServicios.lista = data.lista;
          this.serviciosArray = this.listaServicios.lista;
          this.dataSource = new MatTableDataSource(this.serviciosArray);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error => console.log(error)
      );
    } else {
      this.listarServicioService.getListaServiciosParaServicioNuevo(this.idFichaClinicaParam).subscribe(
        data => {
          this.listaServicios = {} as ListaServicios;
          this.listaServicios.lista = {} as Servicio[];
          this.listaServicios.lista = data.lista;
          this.serviciosArray = this.listaServicios.lista;
          this.dataSource = new MatTableDataSource(this.serviciosArray);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error => console.log(error)
      );
    }

  }//fin ngOnInit()

  onDateSelect(event) {
    console.log(event);
    console.log('fecha elegida=', event.value);
  }

  agregarServicio() {
    /*
    let fechaActualStr = this.datepipe.transform(fechaActual,'yyyy-mm-dd hh:mm:ss');
    console.log(`fechaActualStr=${fechaActualStr}`);
    */
    this.observacion = this.formGroup.controls['observacion'].value;
    console.log(`idFichaClinicaParam =${this.idFichaClinicaParam} ,
    observacion = ${this.observacion}`);
    this.listarServicioService.crearServicioAllParams(this.idFichaClinicaParam, this.observacion).subscribe(
      data => {
        this.idServicioCreado = data;
        console.log(this.idServicioCreado);
        this.creadoCorrectamente(data);
        this.listarServicioService.getListaServiciosParaServicioNuevo(this.idFichaClinicaParam).subscribe(
          data => {
            this.listaServicios = {} as ListaServicios;
            this.listaServicios.lista = {} as Servicio[];
            this.listaServicios.lista = data.lista;
            this.serviciosArray = this.listaServicios.lista;
            this.dataSource.data = this.serviciosArray;
          },
          error => console.log(error.error)
        );
      },
      error => {
        this.errorRecibido(error);
        console.log(error.error);
      }
    );
  }

  getFormatoFecha(s: string): string {
    const anho = s.toString().substring(11, 15);
    let mesElegido = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mesElegido = (+mesElegido < 10) ? '0' + mesElegido : mesElegido;
    const dia = s.toString().substring(8, 10);
    return (anho + mesElegido + dia);
  }

  /**Parte delos dialogs**/
  /**Parte delos dialogs**/
  openDialog(opcionSeleccionada: string): void {
    this.opcionSeleccionada = opcionSeleccionada;
    if (this.opcionSeleccionada === "Empleado") {
      this.empleadoSeleccionado = {} as Persona;
      const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      //this.dialog.open(SeleccionarEmpleadoDialog, {
        width: '250px',
        data: {id: this.empleadoSeleccionado.idPersona, nombre: this.empleadoSeleccionado.nombre}
        //data: { persona: this.empleadoSeleccionado }/*{name: this.name, animal: this.animal}*/
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('Empleado =>', result);
        this.empleadoFiltrado = {} as Persona;
        this.empleadoFiltrado.idPersona = result.id;
        this.empleadoFiltrado.nombre = result.nombre;
        this.empleadoSeleccionado = this.empleadoFiltrado;
      });
      //si no se trata de una persona normal
    } else if (this.opcionSeleccionada === "Cliente") {
      this.clienteSeleccionado = {} as Persona;
      const dialogRef = this.dialog.open(DialogPersonaComponent, {
      //const dialogRef = this.dialog.open(SeleccionarPersonaDialog, {
        width: '250px',
        data: { id: this.clienteSeleccionado.idPersona, nombre: this.clienteSeleccionado.nombre }/*{name: this.name, animal: this.animal}*/
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Cliente =>', result);
        this.clienteFiltrado = {} as Persona;
        this.clienteFiltrado.idPersona = result.id;
        this.clienteFiltrado.nombre = result.nombre;
        this.clienteSeleccionado = this.clienteFiltrado;
        //this.clienteSeleccionado = this.clienteFiltrado = result;
      });
    }

  }


  buscar() {
    const fechaHasta = this.getFormatoFecha(this.formGroup.get('fechaDesde').value.toString());
    console.log(`fechaDesde=${fechaHasta}`);
    this.listarServicioService.getlistaServiciosAllParams(this.empleadoFiltrado, this.clienteFiltrado, fechaHasta).subscribe(
      data => {
        this.dataSource.data = data.lista;
      },
      error => console.log('El error es:', error)
    )

  }

  verDetalles(servicio: Servicio) {
    this.IdServicioSeleccionado = servicio.idServicio;
    this.servicioSeleccionado = servicio;
    console.log('servicio.idServicio=',servicio.idServicio);
    this.seleccionarIdServicioFlag = true;
    this.listarServicioService.getListaDetalleServicio(servicio.idServicio).subscribe(
      data => {
        this.arrayDetalleServicio = data;
        console.log('detalles servicio=>', data);
        for (let i = 0; i < data.length; i++) {
          this.listarServicioService.getPrecioProducto(data[i].idPresentacionProducto.idPresentacionProducto).subscribe(
            data2 => {
              data[i].precioVenta = data2.lista[0].precioVenta;
            }, error => console.log(error.error)
          );
        }
        this.arrayDetalleServicio = data;
        if(!this.dataSourceDetalleServicio){
          this.dataSourceDetalleServicio = new MatTableDataSource(this.arrayDetalleServicio);
          this.dataSourceDetalleServicio.paginator = this.detalleServicioPaginator;
        }else{
          this.dataSourceDetalleServicio.data = this.arrayDetalleServicio;
        }
      }, error => console.log(error.error)
    );
  }

  seleccionarIdFicha(servicio: Servicio) {
    console.log(`servicio.idFichaClinica=>${servicio.idFichaClinica.idFichaClinica}`);
    this.idFichaClinicaParam = servicio.idFichaClinica.idFichaClinica;
    this.seleccionarIdFichaFlag = false;
  }

  creadoCorrectamente(data: number) {
    swal({
      title: "Servicio creado!",
      text: "Se creo correctamente el servicio:" + data + "!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error) {
    swal({
      title: "No se pudo crear el servicio!",
      text: "No se pudo crear el servicio: " + error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }

  botonCambiarIdFichaClicked() {
    this.seleccionarIdFichaFlag = true;
    this.idFichaClinicaParam = undefined;
    this.listarServicioService.getListaServicios().subscribe(
      data => {
        this.listaServicios = {} as ListaServicios;
        this.listaServicios.lista = {} as Servicio[];
        this.listaServicios.lista = data.lista;
        this.serviciosArray = this.listaServicios.lista;
        this.dataSource.data = this.serviciosArray;
      },
      error => console.log(error)
    );
  }

  cargarDatosProducto(subcategoria: Subcategoria) {
    this.subcategoriaDetalleServicioFlag = true;
    this.subcategoriaDetalleServicio = subcategoria;
    this.listarServicioService.getListaProductos(this.categoriaDetalleServicio.idCategoria, this.subcategoriaDetalleServicio.idTipoProducto).subscribe(
      data => {
        this.arrayProductos = data.lista;
        console.log('data.lista=', data.lista);
      },
      error => console.log(error.error)
    );

  }

  cargarPrecioProducto(producto: Producto) {
    this.listarServicioService.getPrecioProducto(producto.idPresentacionProducto).subscribe(
      data => {
        this.precioPresentacionProductoDetalleServicio = data.lista[0].precioVenta;
        console.log(`data.lista[0].precioVenta: ${data.lista[0].precioVenta}`);
        console.log(`this.precioPresentacionProductoDetalleServicio:${this.precioPresentacionProductoDetalleServicio}`);
      },
      error => console.log(error.error)
    );
  }

  agregarDetalle() {

    console.log(`this.cantidadProductoDetalleServicio=${this.cantidadProductoDetalleServicio}`);
    console.log(`this.idPresentacionProductoDetalleServicio=${this.idPresentacionProductoDetalleServicio}`);
    console.log(`this.IdServicioSeleccionado=${this.IdServicioSeleccionado}`);
    
    this.listarServicioService.agregarDetalleServicio(this.cantidadProductoDetalleServicio,
      this.idPresentacionProductoDetalleServicio, this.IdServicioSeleccionado).subscribe(
        data => {
          this.detalleCreadoCorrectamente(data);
          this.verDetalles(this.servicioSeleccionado);
        },
        error => {
          console.log(error.error);
          this.detalleErrorRecibido(error);
        }
      );
  }


  detalleCreadoCorrectamente(data: number) {
    swal({
      title: "Detalle de servicio creado!",
      text: "Se creo correctamente el detalle de servicio:" + data + "!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
    }).catch(swal.noop)
  }

  detalleErrorRecibido(error) {
    swal({
      title: "No se pudo crear el detalle de servicio!",
      text: "No se pudo crear el detalle de servicio: " + error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }

  cargarDatosSubCategoria(categoria: Categoria) {
    this.categoriaDetalleServicioFlag = true;
    this.categoriaDetalleServicio = categoria;
    this.listarServicioService.getListaSubcategoriasPorIdCategoria(categoria.idCategoria).subscribe(
      data => {
        console.log(data);
        this.arraySubcategorias = data.lista;
      }
      , error => console.log(error.error)
    );
  }

  botonCambiarIdServicioClicked() {
    this.seleccionarIdServicioFlag = false;
    this.IdServicioSeleccionado = undefined;
  }


  /* Eliminacion de detalle servicio */
  openModalEliminarDetalleServicio(detalleServicio:DetalleServicio){
    console.log(detalleServicio);
    swal({
      title: 'Desea eliminar esta detalle de servicio?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.listarServicioService.eliminarDetalleServicio(this.IdServicioSeleccionado, detalleServicio.idServicioDetalle).subscribe(
          data => {
            console.log('eliminacion correcta:',data);
            this.modalEliminadoConExito();
            this.verDetalles(this.servicioSeleccionado);
          },
          error => this.errorEliminacion(error)
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El detalle ha sido eliminado con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(error){
    swal({
      title: "No se pudo eliminar el detalle!",
      text: "Ocurrio un error al intentar eliminar el detalle de servicio, error="+error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }



  /* Edicion de servicios */
  pantallaEdicion(){
    
  }



}
