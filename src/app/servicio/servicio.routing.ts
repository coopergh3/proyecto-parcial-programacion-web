import { Routes } from '@angular/router';
import { VerServiciosComponent } from './ver-servicios/ver-servicios.component';
import {CrearServiciosComponent} from './crear-servicios/crear-servicios.component';
import { EditarServiciosComponent } from './editar-servicios/editar-servicios.component';

export const ServicioRoutes: Routes = 
[
    {
        path: '',
        children: [{
            path: 'ver-servicios',
            component: VerServiciosComponent
        }],
    },
    {
        path: '',
        children: [{
            path: 'crear-servicios/:idFichaClinica',
            component: CrearServiciosComponent
        }],
    },
    {
        path: '',
        children: [{
            path: 'editar-servicios/:idServicio',
            component: EditarServiciosComponent
        },
        ]
    },

];