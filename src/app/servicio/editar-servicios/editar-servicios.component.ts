import { Component, OnInit } from '@angular/core';
import { ViewChild, ElementRef, Inject, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ListarServiciosService } from '../listar-servicios.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ListaServicios, Servicio, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria, DetalleServicio
} from '../../shared/models/lista-servicios';

import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona';
import { ListaPersona } from '../../listaPersona';
import { SeleccionarEmpleadoDialog, SeleccionarPersonaDialog } from '../ver-servicios/ver-servicios.component';
import { DatePipe } from '@angular/common';
import swal from 'sweetalert2';
import { Producto } from 'src/app/shared/models/producto';
import { Categoria } from 'src/app/shared/models/categoria';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { SubcategoriaService } from 'src/app/shared/services/subcategoria.service';
import { Subcategoria } from 'src/app/shared/models/subcategoria';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-editar-servicios',
  templateUrl: './editar-servicios.component.html',
  styleUrls: ['./editar-servicios.component.css']
})
export class EditarServiciosComponent implements OnInit {

  displayedColumns: string[] = ['idFicha', 'fechaFicha', 'profesional', 'cliente',
    'categoria', 'subcategoria'];
  dataSource: MatTableDataSource<Servicio>;
  listaServicios: ListaServicios;
  serviciosArray: Servicio[];
  idFichaClinicaParam: number;
  formGroup: FormGroup;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;

  /*Filtrado */
  opcionSeleccionada = 'Empleado';
  empleadoSeleccionado: Persona;
  clienteSeleccionado: Persona;
  empleadoFiltrado: Persona;
  clienteFiltrado: Persona;

  public listaFiltro = [
    { value: 'Empleado', viewValue: 'Empleado' },
    { value: 'Cliente', viewValue: 'Cliente' }
  ];

  mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic']; // Enum para los meses del 1 al 12 el 0 esta de relleno
  seleccionado: any[4]; // Guardar filtros de busqueda para separar paginado del filtro
  fechaDesde: Date;
  fechaHasta: Date;
  observacion: string;
  /*End Filtrado */
  seleccionarIdFichaFlag: boolean;
  idServicioCreado: number;
  /*End Seleccion de fila*/

  /* Detalles Servicio */
  subcategoriaDetalleServicio: Subcategoria;
  subcategoriaDetalleServicioFlag: boolean = false;
  arraySubcategorias: Subcategoria[];
  categoriaDetalleServicio: Categoria;
  categoriaDetalleServicioFlag: boolean = false;
  arrayCategorias: Categoria[];
  idPresentacionProductoDetalleServicio: number;
  precioPresentacionProductoDetalleServicio: number;
  arrayProductos: Producto[];
  cantidadProductoDetalleServicio: number;
  IdServicioSeleccionado: number;
  arrayDetalleServicio: DetalleServicio[];
  displayedColumnsDetallesServicio: string[] = ['idDetalle', 'presentacion', 'precioUnitario',
    'cantidad', 'total', 'acciones'];
  dataSourceDetalleServicio: MatTableDataSource<DetalleServicio>;
  seleccionarIdServicioFlag: boolean = false;
  @ViewChild(MatPaginator, { static: false }) detalleServicioPaginator: MatPaginator;
  servicioSeleccionado: Servicio;
  idServicioParam: number;
  
  
  /*End Detalles Servicio */

  constructor(
    private listarServicioService: ListarServiciosService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    public datepipe: DatePipe,
    private listarCategoriaService: RecibirCategoriaService,
    private listarSubcategoriaService: SubcategoriaService,
    private tituloService: Title
  ) { }//fin constructor

  ngOnInit() {
    this.tituloService.setTitle("Editar Servicio");
    this.listarCategoriaService.getCategorias().subscribe(
      data => {
        this.arrayCategorias = data.lista;
        console.log(this.arrayCategorias);
      }
      , error => console.log(error.error)
    );

    this.route.params.subscribe(params => {
      this.idServicioParam = params.idServicio;
      console.log(params);
    });
    console.log(`idServicio llego:${this.idServicioParam}`);
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
      observacion: new FormControl(''),
    });
    this.listarServicioService.getServicioPorId(this.idServicioParam).subscribe(
        data => {
          console.log('llego bien el servicio:',data);
          this.listaServicios = {} as ListaServicios;
          this.listaServicios.lista = [] as Servicio[];
          this.listaServicios.lista.push(data);
          this.serviciosArray = this.listaServicios.lista;
          
          this.IdServicioSeleccionado = data.idServicio;
          this.servicioSeleccionado = data;
          this.verDetalles(this.servicioSeleccionado);
          console.log('this.serviciosArray=',this.serviciosArray);
          this.dataSource = new MatTableDataSource(this.serviciosArray);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error => console.log(error.error)
      );
    
  }//fin ngOnInit()

  onDateSelect(event) {
    console.log(event);
    console.log('fecha elegida=', event.value);
  }

  getFormatoFecha(s: string): string {
    const anho = s.toString().substring(11, 15);
    let mesElegido = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mesElegido = (+mesElegido < 10) ? '0' + mesElegido : mesElegido;
    const dia = s.toString().substring(8, 10);
    return (anho + mesElegido + dia);
  }

  /**Parte delos dialogs**/
  openDialog(opcionSeleccionada: string): void {
    this.opcionSeleccionada = opcionSeleccionada;
    if (this.opcionSeleccionada === "Empleado") {
      const dialogRef = this.dialog.open(SeleccionarEmpleadoDialog, {
        width: '250px',
        data: { persona: this.empleadoSeleccionado }/*{name: this.name, animal: this.animal}*/
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('Empleado =>', result);
        this.empleadoSeleccionado = this.empleadoFiltrado = result;

      });
      //si no se trata de una persona normal
    } else if (this.opcionSeleccionada === "Cliente") {
      const dialogRef = this.dialog.open(SeleccionarPersonaDialog, {
        width: '250px',
        data: { persona: this.clienteSeleccionado }/*{name: this.name, animal: this.animal}*/
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Cliente =>', result);
        this.clienteSeleccionado = this.clienteFiltrado = result;
      });
    }

  }



  verDetalles(servicio: Servicio) {
    this.IdServicioSeleccionado = this.idServicioParam;
    
    console.log('servicio.idServicio=',servicio.idServicio);
    this.seleccionarIdServicioFlag = true;
    this.listarServicioService.getListaDetalleServicio(servicio.idServicio).subscribe(
      data => {
        this.arrayDetalleServicio = data;
        console.log('detalles servicio=>', data);
        for (let i = 0; i < data.length; i++) {
          this.listarServicioService.getPrecioProducto(data[i].idPresentacionProducto.idPresentacionProducto).subscribe(
            data2 => {
              data[i].precioVenta = data2.lista[0].precioVenta;
            }, error => console.log(error.error)
          );
        }
        this.arrayDetalleServicio = data;
        if(!this.dataSourceDetalleServicio){
          this.dataSourceDetalleServicio = new MatTableDataSource(this.arrayDetalleServicio);
          this.dataSourceDetalleServicio.paginator = this.detalleServicioPaginator;
        }else{
          this.dataSourceDetalleServicio.data = this.arrayDetalleServicio;
        }
      }, error => console.log(error.error)
    );
  }


  creadoCorrectamente(data: number) {
    swal({
      title: "Servicio creado!",
      text: "Se creo correctamente el servicio:" + data + "!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error) {
    swal({
      title: "No se pudo crear el servicio!",
      text: "No se pudo crear el servicio: " + error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }


  cargarDatosProducto(subcategoria: Subcategoria) {
    this.subcategoriaDetalleServicioFlag = true;
    this.subcategoriaDetalleServicio = subcategoria;
    this.listarServicioService.getListaProductos(this.categoriaDetalleServicio.idCategoria, this.subcategoriaDetalleServicio.idTipoProducto).subscribe(
      data => {
        this.arrayProductos = data.lista;
        console.log('data.lista=', data.lista);
      },
      error => console.log(error.error)
    );

  }

  cargarPrecioProducto(producto: Producto) {
    this.listarServicioService.getPrecioProducto(producto.idPresentacionProducto).subscribe(
      data => {
        this.precioPresentacionProductoDetalleServicio = data.lista[0].precioVenta;
        console.log(`data.lista[0].precioVenta: ${data.lista[0].precioVenta}`);
        console.log(`this.precioPresentacionProductoDetalleServicio:${this.precioPresentacionProductoDetalleServicio}`);
      },
      error => console.log(error.error)
    );
  }

  agregarDetalle() {
    console.log(`this.cantidadProductoDetalleServicio=${this.cantidadProductoDetalleServicio}`);
    console.log(`this.idPresentacionProductoDetalleServicio=${this.idPresentacionProductoDetalleServicio}`);
    console.log(`this.IdServicioSeleccionado=${this.IdServicioSeleccionado}`);
    
    this.listarServicioService.agregarDetalleServicio(this.cantidadProductoDetalleServicio,
      this.idPresentacionProductoDetalleServicio, this.IdServicioSeleccionado).subscribe(
        data => {
          this.detalleCreadoCorrectamente(data);
          this.verDetalles(this.servicioSeleccionado);
        },
        error => {
          console.log(error.error);
          this.detalleErrorRecibido(error);
        }
      );
  }


  detalleCreadoCorrectamente(data: number) {
    swal({
      title: "Detalle de servicio creado!",
      text: "Se creo correctamente el detalle de servicio:" + data + "!",
      buttonsStyling: false,
      confirmButtonClass: "btn btn-success",
      type: "success"
    }).catch(swal.noop)
  }

  detalleErrorRecibido(error) {
    swal({
      title: "No se pudo crear el detalle de servicio!",
      text: "No se pudo crear el detalle de servicio: " + error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }

  cargarDatosSubCategoria(categoria: Categoria) {
    this.categoriaDetalleServicioFlag = true;
    this.categoriaDetalleServicio = categoria;
    this.listarServicioService.getListaSubcategoriasPorIdCategoria(categoria.idCategoria).subscribe(
      data => {
        console.log(data);
        this.arraySubcategorias = data.lista;
      }
      , error => console.log(error.error)
    );
  }

  botonCambiarIdServicioClicked() {
    this.seleccionarIdServicioFlag = false;
    this.IdServicioSeleccionado = undefined;
  }


  /* Eliminacion de detalle servicio */
  openModalEliminarDetalleServicio(detalleServicio:DetalleServicio){
    console.log(detalleServicio);
    swal({
      title: 'Desea eliminar esta detalle de servicio?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.listarServicioService.eliminarDetalleServicio(this.IdServicioSeleccionado, detalleServicio.idServicioDetalle).subscribe(
          data => {
            console.log('eliminacion correcta:',data);
            this.modalEliminadoConExito();
            this.verDetalles(this.servicioSeleccionado);
          },
          error => this.errorEliminacion(error)
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El detalle ha sido eliminado con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(error){
    swal({
      title: "No se pudo eliminar el detalle!",
      text: "Ocurrio un error al intentar eliminar el detalle de servicio, error="+error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }



  /* Edicion de servicios */
  pantallaEdicion(){
    
  }





}
