import { Routes } from '@angular/router';

import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { EliminarPacienteComponent } from './eliminar-paciente/eliminar-paciente.component';
import { ModificarPacienteComponent } from './modificar-paciente/modificar-paciente.component';
import { VerPacienteComponent } from './ver-paciente/ver-paciente.component';

export const PacienteRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'crear-paciente',
            component: CrearPacienteComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'eliminar-paciente',
            component: EliminarPacienteComponent
        }]
    }, {
      path: '',
      children: [ {
        path: 'editar-paciente/:id',
        component: ModificarPacienteComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'ver-paciente',
            component: VerPacienteComponent
        }]
    }
];
