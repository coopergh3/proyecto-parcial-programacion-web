import { Component, OnInit, ViewChild } from '@angular/core';
import { RecibirPacienteService } from 'src/app/shared/services/recibir-paciente.service';
import { EliminarPacienteService } from 'src/app/shared/services/eliminar-paciente.service';
import { ListaPaciente } from 'src/app/shared/models/lista-pacientes';
import { Paciente } from 'src/app/shared/models/paciente';
import swal from 'sweetalert2';
import { FormGroup, FormControl } from '@angular/forms';
import { EditarPacienteService } from 'src/app/shared/services/editar-paciente.service';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator, MatTab } from '@angular/material';
import { Title } from '@angular/platform-browser';
declare var $: any;

export class TableData {
  headerRow: String[];
  dataRows: String[][];
}


@Component({
  selector: 'app-ver-paciente',
  templateUrl: './ver-paciente.component.html',
  styleUrls: ['./ver-paciente.component.css']
})
export class VerPacienteComponent implements OnInit {
  public tableData: TableData;
  public searchTerm: string;
  public orderBy: string = 'idPaciente';
  public sentido: string = 'desc';
  public displayedColumns: string[] = ['idPaciente', 'nombre', 'apellido','telefono','cedula','tipoPersona', 'email', 'ruc', 'fechaNacimiento', 'acciones']
  dataSource: MatTableDataSource<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  public pacientes: Paciente[];
  editarPacienteForm = new FormGroup({
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    telefono: new FormControl(''),
    email: new FormControl(''),
    ruc: new FormControl(''),
    cedula: new FormControl(''),
    tipoPersona: new FormControl(''),
    fechaNacimiento: new FormControl(''),
  });
 
  constructor(
    private recibirPacienteService: RecibirPacienteService,
    private eliminarPacienteService: EliminarPacienteService,
    private editarPacienteService: EditarPacienteService,
    private route: Router,
    private tituloService: Title
    ) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista de Pacientes");
      this.recibirPacienteService.getPacientes().subscribe(
        data => {
          console.log("data = ", data);
          this.pacientes = data.lista;
          this.dataSource = new MatTableDataSource(this.pacientes);
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
        },
        error => this.errorRecibido(error)
      );
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  errorRecibido(error){
    swal({
      title: "No se pudo obtener los pacientes.",
      text: "Ocurrio un error al intentar obtener los pacientes.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  getPacientes(data): String[][]{
    return data.lista.map((cat: Paciente)=>{
      return [cat.idPersona, cat.nombre, cat.apellido, cat.cedula];
    });
  }

  openModalEliminar(id){
    swal({
      title: 'Desea eliminar este paciente?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.eliminarPacienteService.eliminarPaciente(id).subscribe(
          data =>{
            this.modalEliminadoConExito();
            window.location.reload();
          },
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El Paciente ha sido eliminado con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar el Paciente!",
      text: "Ocurrio un error al intentar eliminar el Paciente.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  pantallaEdicion(idPersona){
    this.route.navigate(['paciente','editar-paciente', idPersona]);
  }

}
