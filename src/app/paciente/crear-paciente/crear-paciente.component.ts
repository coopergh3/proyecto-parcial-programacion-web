import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../shared/models/paciente';
import { CrearPacienteService } from 'src/app/shared/services/crear-paciente.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-crear-paciente',
  templateUrl: './crear-paciente.component.html',
  styleUrls: ['./crear-paciente.component.css']
})
export class CrearPacienteComponent implements OnInit {
  crearPacienteForm = new FormGroup({
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    telefono: new FormControl(''),
    email: new FormControl(''),
    ruc: new FormControl(''),
    cedula: new FormControl(''),
    tipoPersona: new FormControl(''),
    fechaNacimiento: new FormControl(''),
  });

  constructor(private crearPacienteService: CrearPacienteService, private route:Router,
    private tituloService: Title
    ) { }
  
  onSubmit() {
    this.crearPacienteForm.value.fechaNacimiento= this.crearPacienteForm.value.fechaNacimiento+ " 00:00:00";
    console.log(this.crearPacienteForm);
    this.crearPacienteService.crearPaciente(this.crearPacienteForm.value).subscribe(
      data => {
        this.creadoCorrectamente(data),
        this.route.navigate(['paciente','ver-paciente']);;
      },
      error => this.errorRecibido(error)
    )
  }

  creadoCorrectamente(data: Paciente) {
    console.log("Se creo correctamente el Paciente"+data);
    swal({
        title: "Paciente creado exitosamente!",
        text: "Se creo correctamente el Paciente "+data.apellido+"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo crear, vuelva a intentarlo",
      text: "Ocurrio un error al crear el Paciente.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  ngOnInit() {
    this.tituloService.setTitle("Crear Paciente");
  }

}