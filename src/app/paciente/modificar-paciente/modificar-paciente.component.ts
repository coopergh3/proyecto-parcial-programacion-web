import { Component, OnInit } from '@angular/core';
import { Paciente, PacientePut, PacientePutEnvio } from '../../shared/models/paciente';
import { EditarPacienteService } from 'src/app/shared/services/editar-paciente.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { RecibirPacienteService } from 'src/app/shared/services/recibir-paciente.service';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-modificar-paciente',
  templateUrl: './modificar-paciente.component.html',
  styleUrls: ['./modificar-paciente.component.css']
})
export class ModificarPacienteComponent implements OnInit {
  private id: number;
  private cat: Paciente;
  private pacienteParaEnviar: PacientePut;
  private pacienteEnvio: PacientePutEnvio;
  editarPacienteForm = new FormGroup({
    nombre: new FormControl(''),
    apellido: new FormControl(''),
    telefono: new FormControl(''),
    email: new FormControl(''),
    ruc: new FormControl(''),
    cedula: new FormControl(''),
    tipoPersona: new FormControl(''),
    fechaNacimiento: new FormControl(''),
  });

  constructor(private editarPacienteService: EditarPacienteService,
    private obtenerPacienteServicio: RecibirPacienteService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private tituloService: Title
  ) { }

  onSubmit() {
    console.log(this.pacienteParaEnviar);
    this.pacienteEnvio = new PacientePutEnvio(this.pacienteParaEnviar.idPersona,
      this.editarPacienteForm.value.nombre, this.editarPacienteForm.value.apellido,
      this.editarPacienteForm.value.email, this.editarPacienteForm.value.telefono,
      this.pacienteParaEnviar.seguroMedico, this.pacienteParaEnviar.seguroMedicoNumero, 
      this.editarPacienteForm.value.ruc, this.editarPacienteForm.value.cedula,
      this.editarPacienteForm.value.tipoPersona, this.pacienteParaEnviar.usuarioLogin,
      this.pacienteParaEnviar.idLocalDefecto, this.pacienteParaEnviar.flagVendedor,
      this.pacienteParaEnviar.observacion, this.pacienteParaEnviar.tipoCliente,
      this.pacienteParaEnviar.fechaHoraAprobContrato, this.pacienteParaEnviar.soloUsuariosDelSistema,
      this.pacienteParaEnviar.nombreCompleto, this.pacienteParaEnviar.limiteCredito,
      this.editarPacienteForm.value.fechaNacimiento+ " 00:00:00", this.pacienteParaEnviar.soloProximosCumpleanhos,
      this.pacienteParaEnviar.todosLosCampos
      );
      console.log(this.pacienteEnvio);
    /*this.pacienteEnvio.idPersona = this.pacienteParaEnviar.idPersona;
    console.log(this.pacienteEnvio);
      this.pacienteEnvio.nombre = this.editarPacienteForm.value.nombre;
      this.pacienteEnvio.apellido = this.editarPacienteForm.value.apellido;
      this.pacienteEnvio.telefono = this.editarPacienteForm.value.telefono;
      this.pacienteEnvio.email = this.editarPacienteForm.value.email;
      this.pacienteEnvio.ruc = this.editarPacienteForm.value.ruc;
      this.pacienteEnvio.cedula = this.editarPacienteForm.value.cedula;
      this.pacienteEnvio.tipoPersona = this.editarPacienteForm.value.tipoPersona;
      this.pacienteEnvio.fechaNacimiento= this.editarPacienteForm.value.fechaNacimiento+ " 00:00:00";*/
      this.editarPacienteService.editarPaciente(this.pacienteEnvio).subscribe(
        data => {
          this.editadoCorrectamente(data);
          this.router.navigate(['paciente','ver-paciente']);
        },
        error => this.errorRecibido(error)
      )
  }

  ngOnInit() {
    this.tituloService.setTitle("Editar Paciente");
      this.route.params.subscribe( params => this.id = params.id);
      this.obtenerPacienteServicio.getPaciente(this.id).subscribe(
        data =>{
          this.pacienteParaEnviar = data;
          this.editarPacienteForm = this.formBuilder.group({
            nombre: this.pacienteParaEnviar.nombre,
            apellido: this.pacienteParaEnviar.apellido,
            telefono: this.pacienteParaEnviar.telefono,
            email: this.pacienteParaEnviar.email,
            ruc: this.pacienteParaEnviar.ruc,
            cedula: this.pacienteParaEnviar.cedula,
            tipoPersona: this.pacienteParaEnviar.tipoPersona,
            fechaNacimiento: this.pacienteParaEnviar.fechaNacimiento
          });
          console.log(this.editarPacienteForm);
        }  
      );
  }

  editadoCorrectamente(data: Paciente) {
      console.log("Se editó correctamente el paciente ");
      swal({
          title: "Paciente modificado!",
          text: "Se editó correctamente el paciente!",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success"
      }).catch(swal.noop)
  }
  
  errorRecibido(error){
      swal({
        title: "No se pudo editar!",
        text: "Ocurrio un error al modificar el paciente.",
        showConfirmButton: true
      }).catch(swal.noop)
  }



}
