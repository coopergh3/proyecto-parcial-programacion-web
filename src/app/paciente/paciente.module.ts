import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PacienteRoutes } from './paciente.routing';
import { CrearPacienteComponent } from './crear-paciente/crear-paciente.component';
import { VerPacienteComponent } from './ver-paciente/ver-paciente.component';
import { EliminarPacienteComponent } from './eliminar-paciente/eliminar-paciente.component';
import { ModificarPacienteComponent } from './modificar-paciente/modificar-paciente.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatSortModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PacienteRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
  ],
  declarations: [
    CrearPacienteComponent, 
    VerPacienteComponent, 
    EliminarPacienteComponent, 
    ModificarPacienteComponent,
  ]
})
export class PacienteModule { }
