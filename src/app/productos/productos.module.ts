import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ProductosRoutes } from './productos.routing';
import { ProductosComponent } from './productos.component';
import { MensajesProductosComponent } from '../mensajes-productos/mensajes-productos.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearProductosComponent } from './crear-productos/crear-productos.component';
//import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
  MatSortModule, MatTableModule} from '@angular/material';

import { ProductosFilterPipe } from './productos-filter.pipe';
import { ProductosDataSourceComponent } from './productos-data-source/productos-data-source.component';
import { EditarProductosComponent } from './editar-productos/editar-productos.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ProductosRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    //BrowserAnimationsModule
        //BrowserAnimationsModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule
  ],
  declarations: [
    ProductosComponent, 
    MensajesProductosComponent, 
    CrearProductosComponent, 
    ProductosFilterPipe, ProductosDataSourceComponent, EditarProductosComponent,
  ]
})

export class ProductosModule { }
