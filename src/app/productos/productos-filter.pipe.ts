import {PipeTransform, Pipe} from '@angular/core';
import {Producto} from '../shared/models/producto';

@Pipe({
    name:'productoFilter',
})
export class ProductosFilterPipe implements PipeTransform {
    transform(productos: Producto[], searchTerm: string): Producto[]{
        console.log('Comienza el filtrado, searchTerm=>', searchTerm);
        if(!productos || !searchTerm) {
            return productos;
        }
        console.log(productos.filter(producto =>
            producto.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
             producto.idProducto.idTipoProducto.idTipoProducto == +searchTerm
        ));
        return productos.filter(producto =>
            producto.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
            producto.idProducto.idTipoProducto.idTipoProducto == +searchTerm
        );

    }
}
