import { Component, OnInit } from '@angular/core';
import {Producto, ListaProductos, IdTipoProducto, IdProducto} from '../../shared/models/producto';
import {CrearProductoService} from '../../shared/services/crear-producto.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-crear-productos',
  templateUrl: './crear-productos.component.html',
  styleUrls: ['./crear-productos.component.css']
})
export class CrearProductosComponent implements OnInit {
  productoNuevo: Producto;
  precioVenta: number;
  listaIdProducto: IdProducto [];
  codigoProducto: number;
  crearProductoForm = new FormGroup({
    codigoProducto: new FormControl(''),
    nombre: new FormControl(''),
    descripcion: new FormControl(''),
    idProducto: new FormControl(''),
    idTipoProducto: new FormControl(''),
    precioVenta: new FormControl(''),
  });

  constructor(private crearProductoService: CrearProductoService,
    private rutaDeEscape:Router,
    private tituloService: Title) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista Producto");
  }

  onSubmit() {
    this.productoNuevo = {} as Producto;
    this.productoNuevo.idProducto = {} as IdProducto;
    this.productoNuevo.idProducto.idTipoProducto = {} as IdTipoProducto;
    this.productoNuevo.descripcion = this.crearProductoForm.value.descripcion;
    this.productoNuevo.idProducto.idProducto = this.crearProductoForm.value.idProducto;
    
    this.productoNuevo.nombre = this.crearProductoForm.value.nombre;

    this.productoNuevo.idProducto.idTipoProducto.idTipoProducto = this.crearProductoForm.value.idTipoProducto;
    this.precioVenta = this.crearProductoForm.controls['precioVenta'].value;
    this.codigoProducto = this.crearProductoForm.controls['codigoProducto'].value;
    console.log('codigo producto:', this.codigoProducto);
    console.log(this.productoNuevo);
    console.log(this.precioVenta);
    this.crearProductoService.crearProducto(this.productoNuevo, this.precioVenta, this.codigoProducto).subscribe(
      data => {
        this.creadoCorrectamente(data);
        this.crearProductoForm.reset();
        this.rutaDeEscape.navigate(['productos','productos']);
      },
      error => console.log(error)
    )
  }

  creadoCorrectamente(data: Producto){
    console.log('Se creo correctamente el producto'+data);
    swal({
        title: "Producto creado!",
        text: "Se creo correctamente el Producto"+data.nombre+"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear el producto.",
      showConfirmButton: true
    }).catch(swal.noop)
  }




}
