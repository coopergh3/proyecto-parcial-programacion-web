import { Component, OnInit } from '@angular/core';
import { Producto, ListaProductos, IdTipoProducto, IdProducto } from '../../shared/models/producto';
import { ProductoService } from '../../shared/services/producto.service';
import { CollectionViewer, DataSource } from '@angular/cdk/collections';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-productos-data-source',
  templateUrl: './productos-data-source.component.html',
  styleUrls: ['./productos-data-source.component.css']
})
export class ProductosDataSourceComponent implements OnInit, DataSource<ListaProductos> {
  private listaProductosSubject = new BehaviorSubject<ListaProductos[]>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingSubject.asObservable();

  constructor(private productoService: ProductoService) { }

  ngOnInit() {
  }

  connect(collectionViewer: CollectionViewer): Observable<ListaProductos[]> {
    return this.listaProductosSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.listaProductosSubject.complete();
    this.loadingSubject.complete();
  }

  loadListaProductos(inicio: number, cantidad: number, orderBy: string, orderDir: string, ejemploString: string, opcionFiltrado?:string) {
    this.loadingSubject.next(true);
    this.productoService.getListaProductosPaginated(inicio,cantidad,orderBy,orderDir, ejemploString, opcionFiltrado).pipe(
        catchError(() => of([])),
        finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(listaProductos => this.listaProductosSubject.next(listaProductos.lista));
  }
  

}
