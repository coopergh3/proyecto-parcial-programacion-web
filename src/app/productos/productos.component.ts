import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Producto, ListaProductos, IdTipoProducto, IdProducto } from '../shared/models/producto';
import { ProductoService } from '../shared/services/producto.service';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ProductosDataSourceComponent } from './productos-data-source/productos-data-source.component';
import { ActivatedRoute, Router } from '@angular/router';
import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import swal from 'sweetalert2';
import {EliminarProductoService} from '../shared/services/eliminar-producto.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit, AfterViewInit {

  ejemploString: string;
  displayedColumns: string[] = ['idPresentacionProducto', 'nombre', 'descripcion', 'idProducto', 'idTipoProducto','acciones'];
  dataSource: ProductosDataSourceComponent;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;
  listaProductos: ListaProductos;
  productos: Producto[];
  productosLength: number;
  opcionFiltrado: string;
  /*
    lista productos obtiene el objeto json puro
    los datos utiles estan en su compenente lista
    searchTerm: string;
    dataSource: MatTableDataSource<any>;
    
    
  */

  constructor(private productoService: ProductoService, 
    private route: ActivatedRoute,
    private router: Router,
    private eliminarProductoService: EliminarProductoService,
    private tituloService: Title
    ) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista de Productos");
    this.router.onSameUrlNavigation = "reload";
    console.log(this.route.toString());
    this.productoService.getListaProductos().subscribe(
      data => {
      this.listaProductos = data;
        this.productosLength = this.listaProductos.totalDatos;
      },
      error => console.log(error)
    )
    //this.listaProductos = this.route.snapshot.data["listaProductos"];
    //this.productos = this.listaProductos.lista;
    this.dataSource = new ProductosDataSourceComponent(this.productoService);
    this.dataSource.loadListaProductos(0, 10, 'idPresentacionProducto', 'asc', `{}`);
  }

  ngAfterViewInit() {
    
    // server-side search
    fromEvent(this.input.nativeElement,'keyup')
    .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
            this.paginator.pageIndex = 0;
            this.loadListaProductosPage();
        })
    )
    .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page).pipe(
      tap(() => this.loadListaProductosPage())
    )
      .subscribe();
  }

  loadListaProductosPage() {
    console.log('this.paginator.pageIndex=', this.paginator.pageIndex, ',this.paginator.pageSize=', this.paginator.pageSize,
    ", opcionFiltado=",this.opcionFiltrado);
    this.dataSource.
      loadListaProductos(
        this.paginator.pageIndex * this.paginator.pageSize,//inicio  //probar despues this.listaProductos.lista[0].idPresentacionProducto,
        this.paginator.pageSize,//cantidad
        'idPresentacionProducto',//orderBy
        this.sort.direction,//orderDir
        this.input.nativeElement.value,//ejemploString
        this.opcionFiltrado,//tipo de filtrado
      );
  }

  /*Modals para edicion y delete */
  openModalEliminar(idPresentacionProducto:number){
    swal({
      title: 'Desea eliminar esta producto?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.eliminarProductoService.eliminarProducto(idPresentacionProducto).subscribe(
          data => {
            this.modalEliminadoConExito();
            window.location.reload();
          },
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El producto ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar el producto!",
      text: "Ocurrio un error al intentar eliminar la producto.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  pantallaEdicion(idPresentacionProducto:number){
    this.router.navigate(['productos','editar-productos', idPresentacionProducto]);
  }

}
