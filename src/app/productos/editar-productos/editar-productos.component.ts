import { Component, OnInit } from '@angular/core';
import {Producto, IdTipoProducto, IdProducto} from '../../shared/models/producto';
import {ListaExistenciaProductos, ExistenciaProducto, EditProducto} from '../../shared/models/producto';
import {EditarProductoService} from '../../shared/services/editar-producto.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-editar-productos',
  templateUrl: './editar-productos.component.html',
  styleUrls: ['./editar-productos.component.css']
})
export class EditarProductosComponent implements OnInit {
  productoEdit : EditProducto;
  precioVenta: number;
  codigoProducto: number;
  idPresentacionProducto:number;
  editarProductoForm = new FormGroup({
    nombre: new FormControl(''),
    descripcion: new FormControl(''),
    idProducto: new FormControl(''),
    idTipoProducto: new FormControl(''),
    precioVenta: new FormControl(''),
    codigoProducto: new FormControl('')
  });

  constructor(private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private editarProductoService: EditarProductoService,
    private rutaDeEscape:Router,
    private tituloService: Title) { }

  ngOnInit() {
    this.tituloService.setTitle("Editar Producto");
    this.route.params.subscribe( params => this.idPresentacionProducto = params.idPresentacionProducto);
    this.editarProductoService.getProductoByIdPresentacionProducto(this.idPresentacionProducto).subscribe(
      data=>{
        console.log("EditProductoLista=",data);
        this.productoEdit = {} as EditProducto;
        this.productoEdit.idProducto = {} as IdProducto;
        this.productoEdit.idProducto.idTipoProducto = {} as IdTipoProducto;
        /*cargar los datos */
        this.productoEdit.idPresentacionProducto = this.idPresentacionProducto;
        this.productoEdit.nombre = data.lista[0].nombre;
        this.productoEdit.descripcion = data.lista[0].descripcion;
        this.productoEdit.codigo = data.lista[0].codigo;
        this.productoEdit.idProducto.idProducto = data.lista[0].idProducto.idProducto;
        this.productoEdit.idProducto.idTipoProducto.idTipoProducto = data.lista[0].idProducto.idTipoProducto.idTipoProducto;
        console.log("productoLista=",this.productoEdit);
        this.codigoProducto = this.productoEdit.codigo; 
      },
      error=>console.log(error)
    );    
    this.editarProductoService.getPrecioProducto(this.idPresentacionProducto).subscribe(
      data=>{
        console.log('precioVenta=',data);
        this.precioVenta=data.lista[0].precioVenta;
        this.editarProductoForm.controls['precioVenta'].setValue(this.precioVenta);
        this.editarProductoForm = this.formBuilder.group({
          nombre: this.productoEdit.nombre,
          descripcion: this.productoEdit.descripcion,
          idProducto: this.productoEdit.idProducto.idProducto,
          idTipoProducto: this.productoEdit.idProducto.idTipoProducto.idTipoProducto,
          codigoProducto: this.productoEdit.codigo,
          precioVenta: this.precioVenta
        })
        console.log(this.editarProductoForm);
      },
      error=>console.log(error)
    );
  }

  onSubmit() {
    this.productoEdit.nombre = this.editarProductoForm.value.nombre;
    this.productoEdit.descripcion = this.editarProductoForm.value.descripcion;
    this.productoEdit.idProducto.idProducto = this.editarProductoForm.value.idProducto;
    this.productoEdit.idProducto.idTipoProducto.idTipoProducto = this.editarProductoForm.value.idTipoProducto;
    this.precioVenta = this.editarProductoForm.controls['precioVenta'].value;
    this.codigoProducto = this.editarProductoForm.controls['codigoProducto'].value;
    console.log("codigo producto:",this.codigoProducto);
    console.log(this.productoEdit);
    console.log(this.precioVenta);
    this.editarProductoService.editarProducto(this.idPresentacionProducto,
      this.productoEdit, this.precioVenta, this.codigoProducto).subscribe(
      data => {
        this.EditadoCorrectamente(data);
        this.rutaDeEscape.navigate(['productos','productos']);
      },
      error => console.log(error)
    )
  }

  EditadoCorrectamente(data: Producto){
    console.log("Se edito correctamente el producto"+data);
    swal({
        title: "Producto editado!",
        text: "Se edito correctamente el Producto"+data.nombre+"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al editar el producto.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

}
