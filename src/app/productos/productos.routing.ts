import { Routes } from '@angular/router';
import { ProductosComponent } from "../productos/productos.component";
import { CrearProductosComponent } from "./crear-productos/crear-productos.component";
import {EditarProductosComponent} from "./editar-productos/editar-productos.component"
export const ProductosRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'productos',
            component: ProductosComponent
        }],
    },
    {
        path: '',
        children: [{
            path: 'crear-productos',
            component: CrearProductosComponent
        },
        ]
    },
    {
        path: '',
        children: [{
            path: 'editar-productos/:idPresentacionProducto',
            component: EditarProductosComponent
        },
        ]
    },

];
//falta completar