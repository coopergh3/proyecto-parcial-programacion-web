import {Persona} from './persona';

export interface ListaPersona {
    lista: Persona[];
    totalDatos: number;
}
