export class User {
    usuario: string;

    constructor (usuario: string) {
        this.usuario = usuario;
    }

    setUsuario(usuario: string) {
        this.usuario = usuario;
    }
}

export class Empleado {
    idEmpleado: {idPersona: number};
}
