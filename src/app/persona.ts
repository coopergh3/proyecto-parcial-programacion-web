import {IdLocalDefecto} from './idLocalDefecto';

export interface Persona {
    idPersona: number;
    nombre: String;
    apellido: String;
    email: String;
    telefono: String;
    ruc: String;
    cedula: String;
    tipoPersona: String;
    usuarioLogin: String;
    idLocalDefecto: IdLocalDefecto;
    flagVendedor: String;
    observacion: String;
    tipoCliente: String;
    fechaHoraAprobContrato: String;
    soloUsuariosDelSistema: String;
    nombreCompleto: String;
    limiteCredito: number;
    fechaNacimiento: String;
    soloProximosCumpleanhos: String;
    todosLosCampos: String;
}
