import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { VerReportesComponent} from './ver-reportes/ver-reportes.component';
import { ReporteRoutes } from './reporte.routing';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, 
  MatSortModule, MatTableModule} from '@angular/material';
import { VerReportesdComponent } from './ver-reportesd/ver-reportesd.component';

@NgModule({
  declarations: [VerReportesComponent, VerReportesdComponent],
  entryComponents: [VerReportesComponent, VerReportesdComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ReporteRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    //BrowserAnimationsModule
        //BrowserAnimationsModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule
  
  ]
})
export class ReporteModule { }
