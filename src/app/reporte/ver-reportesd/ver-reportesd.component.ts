import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Inject, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ListarReportesService } from '../listar-reportes.service';
import {
  ListaReportes, Reporte, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria, DetalleReporte
} from '../../shared/models/lista-reportes';

import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona'
import { ListaPersona } from '../../listaPersona'
import * as jsPDF from 'jspdf';
import { ListarServiciosService } from 'src/app/servicio/listar-servicios.service';
import { DetalleServicio } from 'src/app/shared/models/lista-servicios';
/*import { ExporterService } from 'src/app/shared/services/exporter.service';*/


@Component({
  selector: 'app-ver-reportesd',
  templateUrl: './ver-reportesd.component.html',
  styleUrls: ['./ver-reportesd.component.css']
})
export class VerReportesdComponent implements OnInit {
  //displayedColumns: string[] = ['total'];
  dataSource: MatTableDataSource<DetalleReporte>;
  listaReportes: ListaReportes;
  reportesArray: Reporte[];
  formGroup: FormGroup;
  @ViewChild('input', { static: false }) input: ElementRef;
  @ViewChild('content',{ static: false }) content: ElementRef;
  arrayDetalleServicio: DetalleServicio[];
  arrayDetalleReporte: DetalleReporte[]; 

  constructor(private excelService:ListarReportesService,
    private listarReporteService: ListarReportesService,
    private route: ActivatedRoute,
    private router: Router,
    private changeDetectorRefs: ChangeDetectorRef,
    private listarServicioService:ListarServiciosService) { }
  
  downloadPDF() {
    const doc = new jsPDF();
    
    let specialElementHandlers = {
      '#editor': function(element, renderer){
        return true;
      }
    };

    let content = this.content.nativeElement;

    doc.fromHTML(content.innerHTML, 15, 15, {
      'width':100,
      'elementHandelers': specialElementHandlers
    });

    doc.save('ReportesResumidos.pdf');
  }

  exportAsXLSX():void{
    this.excelService.exportToExcel(this.dataSource.data,'ReportesResumidos');
  }

  verDetalles(idServicio: number) {
    //console.log('servicio.idServicio=',idServicio);
    this.listarServicioService.getListaDetalleParaReporte(idServicio).subscribe(
      data => {
        this.arrayDetalleServicio = data;
        //console.log('detalles servicio=>', data);

        for (let i = 0; i < data.length; i++) {
          let reported= {} as DetalleReporte;
          reported.cantidad= data[i].cantidad;
          reported.idPresentacionProducto= data[i].idPresentacionProducto;
          reported.idServicioDetalle= data[i].idServicioDetalle;
          reported.reporte= {} as Reporte;
          reported.reporte.idFichaClinica= {} as IdFichaClinica;
          reported.reporte.idFichaClinica.idCliente= {} as IdCliente;
          reported.reporte.idFichaClinica.idEmpleado= {} as IdEmpleado;
          reported.reporte.idFichaClinica = data[i].idServicio.idFichaClinica;
          reported.reporte.idFichaClinica.idCliente = data[i].idServicio.idFichaClinica.idCliente;
          reported.reporte.idFichaClinica.idEmpleado = data[i].idServicio.idFichaClinica.idEmpleado;
         
          reported.reporte.idFichaClinica.idCliente.nombre= data[i].idServicio.idFichaClinica.idCliente.nombre;
          reported.reporte.idFichaClinica.idCliente.apellido= data[i].idServicio.idFichaClinica.idCliente.apellido;
          reported.reporte.idFichaClinica.idEmpleado.nombre = data[i].idServicio.idFichaClinica.idEmpleado.nombre;
          reported.reporte.idFichaClinica.idEmpleado.apellido  = data[i].idServicio.idFichaClinica.idEmpleado.apellido;
          this.listarServicioService.getPrecioProducto(data[i].idPresentacionProducto.idPresentacionProducto).subscribe(
            data2 => {
              data[i].precioVenta = data2.lista[0].precioVenta;
              reported.precioVenta = data[i].precioVenta;
              reported.total= reported.cantidad * reported.precioVenta;
            }, error => console.log(error.error)
          );
          this.arrayDetalleReporte.push(reported);
        }
      }, error => console.log(error.error)
    );
  }


  ngOnInit() {
    this.arrayDetalleReporte = [] as DetalleReporte[];
    this.arrayDetalleServicio  = [] as DetalleServicio[];
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      fecha: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
    });
    this.listarReporteService.getListaReportes().subscribe(
      data => {
        console.log('data_visualizar',data);
        this.listaReportes = {} as ListaReportes;
        this.listaReportes.lista = {} as Reporte[];
        this.listaReportes.lista = data.lista;
        this.reportesArray = this.listaReportes.lista;
        for (let i = 0; i < this.reportesArray.length; i++){
          //console.log(this.reportesArray[i]);
          this.verDetalles(this.reportesArray[i].idServicio); 
        }
        console.log('arrayDetalleReporte=>', this.arrayDetalleReporte);
        this.dataSource = new MatTableDataSource(this.arrayDetalleReporte);
        //this.dataSource.data = this.arrayDetalleReporte;
        console.log('dataSource>>>',this.dataSource.data);
      },
      error => console.log(error)
    );
  }



}

/*
@Component({
  selector: 'app-seleccionar-persona-dialog',
  templateUrl: './seleccionar-persona-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarPersonaDialog {

  listaPersonas: ListaPersona;
  arrayClientes: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarPersonaDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarReportesService: ListarReportesService
  ) { }

  ngOnInit(): void {
    this.listarReportesService.getListaPersonas().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayClientes = [] as Persona[];
        this.listaPersonas.lista.forEach(element => {
          if (!element.usuarioLogin) {
            this.arrayClientes.push(element);
          }
        })
        console.log(this.arrayClientes);
      },
      error => console.log("error al listar clientes:", error)
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}



@Component({
  selector: 'app-seleccionar-empleado-dialog',
  templateUrl: './seleccionar-empleado-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarEmpleadoDialog implements OnInit {

  listaPersonas: ListaPersona;
  arrayEmpleados: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarEmpleadoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarReportesService: ListarReportesService
  ) { }

  ngOnInit(): void {
    this.listarReportesService.getListaEmpleados().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayEmpleados = this.listaPersonas.lista;
        console.log(this.arrayEmpleados);
      },
      error => console.log("error al listar Empleados:", error)
    )
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
} */
