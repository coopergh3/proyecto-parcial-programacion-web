import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Inject, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { ListarReportesService } from '../listar-reportes.service';
import {
  ListaReportes, Reporte, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria
} from '../../shared/models/lista-reportes';

import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona'
import { ListaPersona } from '../../listaPersona'
import * as jsPDF from 'jspdf';
import { Title } from '@angular/platform-browser';
/*import { ExporterService } from 'src/app/shared/services/exporter.service';*/


@Component({
  selector: 'app-ver-reportes',
  templateUrl: './ver-reportes.component.html',
  styleUrls: ['./ver-reportes.component.css']
})
export class VerReportesComponent implements OnInit {
  displayedColumns: string[] = ['fecha', 'Nprofesional', 'Aprofesional', 'Ncliente', 'Acliente',
    'presupuesto', 'subcategoria'];
  dataSource: MatTableDataSource<Reporte>;
  listaReportes: ListaReportes;
  reportesArray: Reporte[];
  formGroup: FormGroup;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;
  @ViewChild('content',{ static: false }) content: ElementRef;

  constructor(private excelService:ListarReportesService,
    private listarReporteService: ListarReportesService,
    private route: ActivatedRoute,
    private router: Router,
    private changeDetectorRefs: ChangeDetectorRef,
    private tituloService: Title) { }
  
  downloadPDF() {
    const doc = new jsPDF();
    
    let specialElementHandlers = {
      '#editor': function(element, renderer){
        return true;
      }
    };

    let content = this.content.nativeElement;

    doc.fromHTML(content.innerHTML, 15, 15, {
      'width':100,
      'elementHandelers': specialElementHandlers
    });

    doc.save('ReportesResumidos.pdf');
  }

  exportAsXLSX():void{
    this.excelService.exportToExcel(this.dataSource.data,'ReportesResumidos');
  }




  ngOnInit() {
    this.tituloService.setTitle("Ver Reportes");
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      fecha: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
    });
    this.listarReporteService.getListaReportes().subscribe(
      data => {
        this.listaReportes = {} as ListaReportes;
        this.listaReportes.lista = {} as Reporte[];
        this.listaReportes.lista = data.lista;
        this.reportesArray = this.listaReportes.lista;
        //console.log('arrayReportes=>', this.reportesArray);
        this.dataSource = new MatTableDataSource(this.reportesArray);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => console.log(error)
    );
  }
}

/*
@Component({
  selector: 'app-seleccionar-persona-dialog',
  templateUrl: './seleccionar-persona-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarPersonaDialog {

  listaPersonas: ListaPersona;
  arrayClientes: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarPersonaDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarReportesService: ListarReportesService
  ) { }

  ngOnInit(): void {
    this.listarReportesService.getListaPersonas().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayClientes = [] as Persona[];
        this.listaPersonas.lista.forEach(element => {
          if (!element.usuarioLogin) {
            this.arrayClientes.push(element);
          }
        })
        console.log(this.arrayClientes);
      },
      error => console.log("error al listar clientes:", error)
    )
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}



@Component({
  selector: 'app-seleccionar-empleado-dialog',
  templateUrl: './seleccionar-empleado-dialog.html',
  styleUrls: ['./seleccionar-persona-dialog.css']
})
export class SeleccionarEmpleadoDialog implements OnInit {

  listaPersonas: ListaPersona;
  arrayEmpleados: Persona[];

  constructor(
    public dialogRef: MatDialogRef<SeleccionarEmpleadoDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Persona,
    private listarReportesService: ListarReportesService
  ) { }

  ngOnInit(): void {
    this.listarReportesService.getListaEmpleados().subscribe(
      data => {
        this.listaPersonas = data;
        this.arrayEmpleados = this.listaPersonas.lista;
        console.log(this.arrayEmpleados);
      },
      error => console.log("error al listar Empleados:", error)
    )
  }


  onNoClick(): void {
    this.dialogRef.close();
  }
} */
