import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {
  ListaReportes, Reporte, IdFichaClinica, IdCliente, IdLocal, IdEmpleado, IdTipoProducto
} from '../shared/models/lista-reportes';
import { Observable } from 'rxjs';
import { Persona } from '../persona';
import { ListaPersona } from '../listaPersona';
import * as FileSarver from 'file-saver';
import * as XLSX from 'xlsx';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXCEL_EXT = 'xlsx';

@Injectable({
  providedIn: 'root'
})
export class ListarReportesService {

  private reporteUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/servicio";  // URL to web api
  private personaUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/persona";
  constructor(private http: HttpClient) { }
  
  exportToExcel(json:any[], excelFileName:string):void{
    //console.log(json);
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { 'data': worksheet},
      SheetNames: ['data']
    };

    const excelBuffer: any= XLSX.write(workbook, { bookType: 'xlsx', type: 'array'});
    this.saveAsExcel(excelBuffer, excelFileName);
  }

  private saveAsExcel(buffer:any, fileName:string): void{
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSarver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXT);

  }

  getListaReportes(): Observable<ListaReportes> {
    //return this.http.get<ListaProductos>(this.productoUrlOrderByIdProducto);
    return this.http.get<ListaReportes>(this.reporteUrl);
  }

  getListaPersonas(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.personaUrl);
  }

  //
  getReportesEmpleadosParams(empleado: Persona, fechaDesdeCadena: string, fechaHastaCadena: string): Observable<ListaReportes> {
    return this.http.get<ListaReportes>(this.reporteUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"idEmpleado":{"idPersona": ${empleado.idPersona}}, 
      "fechaDesdeCadena":"${fechaDesdeCadena}","fechaHastaCadena":"${fechaHastaCadena}"}`)
      /*
      ,headers: new HttpHeaders().
        set('usuario', 'ana')
        */
    });
  }

  getReportesClientesParams(cliente: Persona, fechaDesdeCadena: string, fechaHastaCadena: string): Observable<ListaReportes> {
    return this.http.get<ListaReportes>(this.reporteUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"idFichaClinica":{"idCliente":{"idPersona": ${cliente.idPersona}}}, 
      "fechaDesdeCadena":${fechaDesdeCadena},"fechaHastaCadena":${fechaHastaCadena}}`)
    });
  }

  getListaEmpleados(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.personaUrl, {
      params: new HttpParams()
        .set("ejemplo", `{"soloUsuariosDelSistema":true}`)
        .set("orderBy", "nombre")
        .set("orderDir", "asc")
    }
    )
  }

}
