import { Routes } from '@angular/router';
import { VerReportesComponent } from './ver-reportes/ver-reportes.component';
import { VerReportesdComponent } from './ver-reportesd/ver-reportesd.component';

export const ReporteRoutes: Routes = 
[
    {
        path: '',
        children: [{
            path: 'ver-reportes',
            component: VerReportesComponent
        }],
    },
    {
        path: '',
        children: [{
            path: 'ver-reportesd',
            component: VerReportesdComponent
        }],
    },
];