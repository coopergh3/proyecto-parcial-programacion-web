import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import 'rxjs/add/operator/map';

import { AgendaService } from '../../servicios/agenda.service';
import {Observable} from 'rxjs';
import {ListaReserva} from '../../shared/models/listaReserva';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Subject } from 'rxjs';
import {Reserva} from '../../shared/models/reserva';
import {Persona} from '../../persona';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {PersonaData, ReservarComponent} from '../reservar/reservar.component';
import {DialogPersonaComponent} from '../dialog-persona/dialog-persona.component';
import {DialogEmpleadoComponent} from '../dialog-empleado/dialog-empleado.component';
import {DialogEditarComponent} from '../dialog-editar/dialog-editar.component';
import { Router } from '@angular/router';
import {ReservaEdit} from '../../shared/models/reservaEdit';
import { Title } from '@angular/platform-browser';

declare class DataTable {
  headerRow: string[];
  dataRows: string[][];
}

export interface EditarDatos {
    observacion: string;
    asistio: boolean;
}

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css'],
})
export class AgendaComponent implements OnInit {

  headerRow: string[] = ['Fecha', 'Hora', 'Profesional', 'Cliente', ''];

  personaAux: PersonaData;

  empleadoId = null;
  empleadoNombre = '';

  clienteId = null;
  clienteNombre = '';

  mes; // Enum para los meses

  seleccionado: any[4]; // Guardar filtros de busqueda para separar paginado del filtro

  fechaDesde: Date;
  fechaHasta: Date;
  hoy: Date;

  public inicio = 0;
  public cantidad = 5;
  public pagina = 1;
  public totalDatos: number;
  public numPaginas: number;

  formGroup: FormGroup;
  reservas: Reserva[];

  public dataTable: DataTable;
  public lista: {}[];
  public listaFiltro = [
    {value: 'Empleado', viewValue: 'Empleado'},
    {value: 'Cliente', viewValue: 'Cliente'}
  ];

  constructor(
      private agendaService: AgendaService,
      private formBuilder: FormBuilder,
      public dialog: MatDialog,
      private router: Router,
      private tituloService: Title) { }

/*    @Input() private pagina: number;
    @Input() private numPaginas: number;
    @Input() private totalDatos: number;*/
    @Output() paginaEmitter: EventEmitter<number> = new EventEmitter();

    ngOnInit() {
      this.tituloService.setTitle("Agenda");
      this.mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'];

      this.fechaDesde = new Date();
      this.fechaDesde = this.inicializarTiempo(this.fechaDesde);

      this.fechaHasta = new Date();
      this.fechaHasta = this.inicializarTiempo(this.fechaHasta);

      this.hoy = new Date();
      this.hoy = this.inicializarTiempo(this.hoy);

      this.formGroup = new FormGroup({
          fechaDesde: new FormControl(''),
          fechaHasta: new FormControl(''),
      });
      this.formGroup.get('fechaDesde').setValue(this.fechaDesde);
      this.formGroup.get('fechaHasta').setValue(this.fechaHasta);

      this.dataTable = {
        headerRow: this.headerRow,
        dataRows: []
      };

      this.agendaService.getEmpleado().subscribe(data => {
          this.lista = this.getPersonas(data);
      }, error => {
          console.log(error);
      });

      this.reset();
    }

    openDialog(tipo: string): void {
        if (tipo === 'Empleado') {
            const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
                width: '250px',
                data: {id: this.empleadoId, nombre: this.empleadoNombre}
            });

            dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed in empleado');
                if (result !== undefined) {
                    this.personaAux = result;
                    this.empleadoNombre = result.nombre;
                    this.empleadoId = result.id;
                    /*this.personaNombre = this.empleadoNombre;*/
                }
            });
        } else {
            const dialogRef = this.dialog.open(DialogPersonaComponent, {
                width: '250px',
                data: {id: this.clienteId, nombre: this.clienteNombre}
            });

            dialogRef.afterClosed().subscribe(result => {
                console.log('The dialog was closed in cliente');
                if (result !== undefined) {
                    this.personaAux = result;
                    this.clienteNombre = result.nombre;
                    this.clienteId = result.id;
                    /*this.personaNombre = this.clienteNombre;*/
                }
            });
        }
    }

    siguiente() {
        this.pagina++;
        this.pasarPagina();
    }

    anterior() {
        this.pagina--;
        this.pasarPagina();
    }

    pasarPagina() {
        this.inicio = this.cantidad * (this.pagina - 1);
        this.solicitud(
            this.seleccionado[0],
            this.seleccionado[1],
            this.seleccionado[2],
            this.seleccionado[3],
            false
        );
    }

    inicializarTiempo(tiempo: Date): Date {
        tiempo.setHours(0);
        tiempo.setMinutes(0);
        tiempo.setSeconds(0);
        tiempo.setMilliseconds(0);
        return tiempo;
    }

    getReservas(data): string[][] {
    return data.lista.map((res: Reserva) => {
      return [
          res.fecha,
          res.horaInicio.substring(0, 5),
          res.horaFin.substring(0, 5),
          res.idEmpleado.nombreCompleto,
          res.idCliente.nombreCompleto,
          res.idReserva,
          res.observacion,
          res.flagAsistio,
          res.flagEstado,
      ];
    });
  }

  getPersonas(data) {
    return data.lista.map((pers: Persona) => {
      return {value: pers.idPersona, viewValue: pers.nombreCompleto};
    });
  }

  reset() {
      this.empleadoId = null;
      this.empleadoNombre = null;

      this.clienteId = null;
      this.clienteNombre = null;

      this.formGroup.get('fechaDesde').setValue(this.hoy);
      this.formGroup.get('fechaHasta').setValue(this.hoy);

      this.seleccionado = [null, null,
          this.getFormatoFecha(this.formGroup.get('fechaDesde').value.toString()),
          this.getFormatoFecha(this.formGroup.get('fechaHasta').value.toString())];

      this.solicitud(
          this.seleccionado[0],
          this.seleccionado[1],
          this.seleccionado[2],
          this.seleccionado[3],
          true
      );
  }

  checkFechaDesde(e) {
    const desde = new Date(e.value.toString());
    const hasta = new Date(this.formGroup.get('fechaHasta').value.toString());

    if (desde > hasta) {
        this.formGroup.get('fechaHasta').setValue(desde);
    }
    this.fechaDesde = e.value;
  }

  getFormatoFecha(s: string): string {
    const anho = s.toString().substring(11, 15);
    let mes = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mes = (mes < 10) ? '0' + mes : mes;
    const dia = s.toString().substring(8, 10);

    return (anho + mes + dia);
  }

  buscar() {
      const idEmp = this.empleadoId;
      const idCli = this.clienteId;

      const desde = this.getFormatoFecha(this.formGroup.get('fechaDesde').value.toString());
      const hasta = this.getFormatoFecha(this.formGroup.get('fechaHasta').value.toString());

      this.seleccionado = [idEmp, idCli, desde, hasta];

      this.solicitud(idEmp, idCli, desde, hasta, true);
  }

  refrescar() {
      setTimeout(() => {
          this.solicitud(this.seleccionado[0], this.seleccionado[1], this.seleccionado[2], this.seleccionado[3], false);
      }, 500);
  }

  solicitud(idEmp: any, idCli: any, desde: string, hasta: string, actualizarCantPagina: boolean) {

      if (actualizarCantPagina)  {
          this.inicio = 0;
      }

      this.agendaService.getReservasFiltro(idEmp, idCli, desde, hasta, this.inicio, this.cantidad).subscribe(data => {
          this.reservas = data.lista;

          this.dataTable = {
              headerRow: this.headerRow,
              dataRows: this.getReservas(data),
          };

          if (actualizarCantPagina) {
              this.totalDatos = data.totalDatos;
              this.pagina = (this.totalDatos > this.cantidad) ? 1 : 0;
              this.numPaginas = Math.round(this.totalDatos / this.cantidad);
          }
      }, err => {
          console.log(err);
      });
  }

  openDialogEditar(id: number, observacion: string, asistio: boolean): void {
    const dialogRef = this.dialog.open(DialogEditarComponent, {
        width: '250px',
        data: {observacion: observacion, asistio: asistio}
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed in editar');
        if (result !== undefined) {
            const aux: EditarDatos = result;

            let asistencia = 'N';
            if (aux.asistio) {
                asistencia = 'S';
            }

            let obs = '';
            if (aux.observacion !== null) {
                obs = aux.observacion;
            }
            const reservaEdit: ReservaEdit = new ReservaEdit(id, obs, asistencia);
            this.agendaService.editReserva(reservaEdit).subscribe(data => console.log(data));

            this.refrescar();

            return;
        }
    });
  }

  eliminar(id: any) {
      this.agendaService.deleteReserva(id).subscribe(data => console.log(data));

      this.refrescar();
  }

  editar(id: any, observacion: string, asistio: string) {
      const auxAsist = (asistio === 'S');
      this.openDialogEditar(id, observacion, auxAsist);
  }

  nuevaFicha(id: any) {
      this.router.navigate(['reserva',id,'registrar-ficha']);
  }
}
