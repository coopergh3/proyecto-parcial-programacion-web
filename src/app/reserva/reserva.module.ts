import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AgendaComponent } from './agenda/agenda.component';
import { ReservarComponent } from './reservar/reservar.component';

import { ReservaRoutes } from './reserva.routing';

import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { DialogPersonaComponent } from './dialog-persona/dialog-persona.component';
import { DialogEmpleadoComponent } from './dialog-empleado/dialog-empleado.component';
import { DialogEditarComponent } from './dialog-editar/dialog-editar.component';
import { RegistrarFichaComponent } from '../ficha/registrar-ficha/registrar-ficha.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReservaRoutes),
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        NgxMaterialTimepickerModule,
    ],
  declarations: [AgendaComponent, ReservarComponent, DialogEditarComponent, RegistrarFichaComponent, DialogPersonaComponent, DialogEmpleadoComponent],
  entryComponents: [DialogPersonaComponent, DialogEditarComponent, DialogEmpleadoComponent]
})
export class ReservaModule { }
