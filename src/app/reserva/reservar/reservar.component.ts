import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormControl, FormGroup} from '@angular/forms';
import {AgendaService} from '../../servicios/agenda.service';
import {Persona} from '../../persona';
import {Reserva} from '../../shared/models/reserva';
import { Agenda } from '../../shared/models/agenda';
import { DialogPersonaComponent} from '../dialog-persona/dialog-persona.component';
import {DialogEmpleadoComponent} from '../dialog-empleado/dialog-empleado.component';
import {ReservaPost, PersonaPost} from '../../shared/models/reservaPost';

import {Observable, Subject} from 'rxjs';
import { Title } from '@angular/platform-browser';

declare class DataTable {
  headerRow: string[];
  dataRows: string[][];
}

export class PersonaData {
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-reservar',
  templateUrl: './reservar.component.html',
  styleUrls: ['./reservar.component.css']
})
export class ReservarComponent implements OnInit {

  headerRow: string[] = ['HoraIni', 'HoraFin', 'Cliente', 'Seleccionar'];

  personaAux: PersonaData;

  empleadoId = null;
  empleadoNombre = '';

  clienteId = null;
  clienteNombre = '';

  campoObservacion = '';

  formGroup: FormGroup;
  hoy: Date;
  horaSeleccionada: string = null;

  agenda: Agenda[];

  mes;
  seleccionado: any[4]; // Guardar filtros de busqueda para separar el filtro del paginado

  public inicio = 0;
  public cantidad = 5;
  public pagina = 0;
  public totalDatos: number;
  public numPaginas: number;

  public listaPersonas: {}[];
  public dataTable: DataTable;
  public lista: string[][] = [];

  constructor(private agendaService: AgendaService,
     public dialog: MatDialog,
     private tituloService: Title
     ) { }

  ngOnInit() {
    this.tituloService.setTitle("Reservar Turno");
    this.mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic'];

    this.hoy = new Date();
    this.hoy.setHours(0); this.hoy.setMinutes(0); this.hoy.setSeconds(0); this.hoy.setMilliseconds(0);

    this.formGroup = new FormGroup({
      fecha: new FormControl(''),
      checked: new FormControl(false)
    });

    this.formGroup.get('fecha').setValue(this.hoy);

    this.dataTable = {
      headerRow: this.headerRow,
      dataRows: []
    };

    this.agendaService.getPersona().subscribe(data => {
      this.listaPersonas = this.getPersonas(data);
    }, error => {console.log(error); });

    this.seleccionado = [null, null, null, true];
  }

  openDialog(tipo: string): void {
    if (tipo === 'empleado') {
      const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
        width: '250px',
        data: {id: this.empleadoId, nombre: this.empleadoNombre}
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed in empleado');
        if (result !== undefined) {
          this.personaAux = result;
          this.empleadoNombre = result.nombre;
          this.empleadoId = result.id;

          this.buscar();
        }
      });
    } else {
      const dialogRef = this.dialog.open(DialogPersonaComponent, {
        width: '250px',
        data: {id: this.clienteId, nombre: this.clienteNombre}
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed in cliente');
        if (result !== undefined) {
          this.personaAux = result;
          this.clienteNombre = result.nombre;
          this.clienteId = result.id;
        }
      });
    }
  }

  siguiente() {
    this.pagina++;
    this.pasarPagina();
  }

  anterior() {
    this.pagina--;
    this.pasarPagina();
  }

  pasarPagina() {
    // this.inicio = this.cantidad * this.pagina;
    this.solicitud(this.seleccionado[0], this.seleccionado[2], this.seleccionado[3], false);
  }

  getPersonas(data) {
    return data.lista.map((pers: Persona) => {
      return {value: pers.idPersona, viewValue: pers.nombreCompleto};
    });
  }

  reservar() {
    const inicio = this.horaSeleccionada.substring(0, 4);
    const fin = this.horaSeleccionada.substring(4, 8);
    const fecha = this.getFormatoFecha(this.formGroup.get('fecha').value.toString());

    const emp = new PersonaPost(this.empleadoId);
    const cli = new PersonaPost(this.clienteId);
    const reserva = new ReservaPost(fecha, inicio, fin, emp, cli, this.campoObservacion);

    this.agendaService.postReserva(reserva).subscribe(data => console.log(data));

    this.clienteId = null;
    this.clienteNombre = '';

    this.campoObservacion = '';

    setTimeout(() => {
      this.solicitud(this.seleccionado[0], this.seleccionado[2], this.seleccionado[3], false);
    }, 500);
  }

  getReservas(data): string[][] {
    return data.map((res: Reserva) => {
      const cliente = (res.idCliente !== null) ? res.idCliente.nombreCompleto : null;
      const valor = (res.idCliente === null) ? (res.horaInicioCadena + res.horaFinCadena) : null;

      return [
        res.horaInicioCadena.substring(0, 2) + ':' + res.horaInicioCadena.substring(2, 4),
        res.horaFinCadena.substring(0, 2) + ':' + res.horaFinCadena.substring(2, 4),
        cliente,
        valor
      ];
    });
  }

  buscar() {
    const idEmp = this.empleadoId;
    const idCli = this.clienteId;
    const fecha = this.getFormatoFecha(this.formGroup.get('fecha').value.toString());
    const check = this.formGroup.get('checked').value;

    this.seleccionado = [idEmp, idCli, fecha, check];

    this.solicitud(idEmp, fecha, check, true);
  }

  solicitud(idEmp: any, fecha: string, check: boolean, actualizarCantPagina: boolean) {

    this.horaSeleccionada = null;

    this.agendaService.getReservasAgregar(idEmp, fecha, check).subscribe(data => {
      this.agenda = data;

      this.dataTable = {
        headerRow: this.headerRow,
        dataRows: (data.length === 0) ? [] : this.getReservas(data)
      };

      if (actualizarCantPagina) {
        this.inicio = 0;
        this.totalDatos = data.length;
        this.pagina = (this.totalDatos > this.cantidad) ? 1 : 0;
        this.numPaginas = Math.round(this.totalDatos / this.cantidad);

        this.lista = [];
        for (let i = 0; i < this.cantidad && i < this.totalDatos; i++) {
          this.lista.push(this.dataTable.dataRows[this.inicio + i]);
        }
      } else {
        this.inicio = this.cantidad * (this.pagina - 1);

        this.lista = [];
        for (let i = 0; i < (this.cantidad + this.inicio) && (this.inicio + i) < this.totalDatos; i++) {
          this.lista.push(this.dataTable.dataRows[this.inicio + i]);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  getFormatoFecha(s: string): string {
    const anho = s.toString().substring(11, 15);
    let mes = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mes = (mes < 10) ? '0' + mes : mes;
    const dia = s.toString().substring(8, 10);

    return (anho + mes + dia);
  }
}
