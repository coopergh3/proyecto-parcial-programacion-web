import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {Persona} from '../../persona';
import {PersonaData} from '../dialog-persona/dialog-persona.component';

export interface DialogData {
  observacion: string;
  asistio: boolean;
}

@Component({
  selector: 'app-dialog-editar',
  templateUrl: './dialog-editar.component.html',
  styleUrls: ['./dialog-editar.component.css']
})
export class DialogEditarComponent implements OnInit {

  formGroup: FormGroup;

  constructor(
      public dialogRef: MatDialogRef<DialogEditarComponent>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
