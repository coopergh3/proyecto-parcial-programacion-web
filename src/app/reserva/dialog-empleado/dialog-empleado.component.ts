import {Component, Inject, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {AgendaService} from '../../servicios/agenda.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {map, startWith} from 'rxjs/operators';
import {Persona} from '../../persona';
import {PersonaData} from '../dialog-persona/dialog-persona.component';

@Component({
  selector: 'app-dialog-empleado',
  templateUrl: './dialog-empleado.component.html',
  styleUrls: ['./dialog-empleado.component.css']
})
export class DialogEmpleadoComponent implements OnInit {

  personas: PersonaData[] = [];
  myControl = new FormControl();

  esObjeto = false;

  filteredOptions: Observable<PersonaData[]>;

  constructor(
      private agendaService: AgendaService,
      public dialogRef: MatDialogRef<DialogEmpleadoComponent>,
      @Inject(MAT_DIALOG_DATA) public data: PersonaData) {}

  ngOnInit() {
    this.agendaService.getEmpleado().subscribe(data => {
      this.personas = this.getPersonas(data);
    }, error => {console.log(error); });

    this.filteredOptions = this.myControl.valueChanges
        .pipe(
            startWith(''),
            map(value => typeof value === 'string' ? value : value.name),
            map(name => name ? this._filter(name) : this.personas.slice())
        );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  aceptar() {
    let aux: PersonaData;
    aux = this.myControl.value;

    this.data.id = aux.id;
    this.data.nombre = aux.nombre;

    this.dialogRef.close(this.data);
  }

  getPersonas(data) {
    return data.lista.map((pers: Persona) => {
      return {id: pers.idPersona, nombre: pers.nombreCompleto};
    });
  }

  onChange(e) {
    const aux = this.myControl.value.toString();

    this.esObjeto = (aux  === '[object Object]');
  }

  displayFn(user?: PersonaData): string | undefined {
    return user ? user.nombre : undefined;
  }

  private _filter(name: string): PersonaData[] {
    const filterValue = name.toLowerCase();
    let spl;

    return this.personas.filter(option => {
      spl = option.nombre.toLowerCase().split(/ /g);
      return spl[0].indexOf(filterValue) === 0 || spl[1].indexOf(filterValue) === 0 || option.nombre.toLowerCase().indexOf(filterValue)=== 0;
    });
  
  }

}
