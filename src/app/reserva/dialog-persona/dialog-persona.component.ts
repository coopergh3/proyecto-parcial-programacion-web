import {Component, Inject, OnInit} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormBuilder, FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { AgendaService } from '../../servicios/agenda.service';
import {Persona} from '../../persona';

export interface PersonaData {
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-dialog-persona',
  templateUrl: './dialog-persona.component.html',
  styleUrls: ['./dialog-persona.component.css']
})
export class DialogPersonaComponent {

    personas: PersonaData[] = [];
    myControl = new FormControl();

    esObjeto = false;

    filteredOptions: Observable<PersonaData[]>;

    constructor(
        private agendaService: AgendaService,
        public dialogRef: MatDialogRef<DialogPersonaComponent>,
        @Inject(MAT_DIALOG_DATA) public data: PersonaData) {}

    ngOnInit() {
        this.agendaService.getPersona().subscribe(data => {
            this.personas = this.getPersonas(data);
        }, error => {console.log(error); });

        this.filteredOptions = this.myControl.valueChanges
            .pipe(
                startWith(''),
                map(value => typeof value === 'string' ? value : value.name),
                map(name => name ? this._filter(name) : this.personas.slice())
            );
    }

    onNoClick(): void {
        this.dialogRef.close();
    }


    aceptar() {
        let aux: PersonaData;
        aux = this.myControl.value;

        this.data.id = aux.id;
        this.data.nombre = aux.nombre;

        this.dialogRef.close(this.data);
    }

    getPersonas(data) {
        return data.lista.map((pers: Persona) => {
            return {id: pers.idPersona, nombre: pers.nombreCompleto};
        });
    }

    onChange(e) {
        const aux = this.myControl.value.toString();
        this.esObjeto = (aux  === '[object Object]');
    }

    displayFn(user?: PersonaData): string | undefined {
        return user ? user.nombre : undefined;
    }

    private _filter(name: string): PersonaData[] {
        const filterValue = name.toLowerCase();

        return this.personas.filter(option => option.nombre.toLowerCase().indexOf(filterValue) === 0);
    }
}
