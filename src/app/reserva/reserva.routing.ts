import { Routes } from '@angular/router';

import { AgendaComponent } from './agenda/agenda.component';
import { ReservarComponent } from './reservar/reservar.component';
import { RegistrarFichaComponent } from '../ficha/registrar-ficha/registrar-ficha.component';

export const ReservaRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'agenda',
            component: AgendaComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'reservar',
            component: ReservarComponent
        }]
    },{
        path: ':id/registrar-ficha',
        component: RegistrarFichaComponent
    }
];
