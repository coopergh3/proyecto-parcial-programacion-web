import { Injectable } from '@angular/core';
import { Categoria, CategoriaPut } from '../models/categoria';
import { ListaCategoria } from '../models/lista-categorias';
import { Observable, of } from 'rxjs';
import {environment} from "../../../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecibirCategoriaService {
  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/categoria/";
  private basePath: string="/stock-pwfe/categoria/";

  constructor(private httpClient: HttpClient) { }

  getCategorias(): Observable<ListaCategoria> {
    let params = new HttpParams();
    return this.httpClient.get<ListaCategoria>(this.basePath);
  }

  getCategorias2(inicio,cantidad,orderBy,orderDir,filtro): Observable<any> {
    let params = new HttpParams();
    let parametros: string;
    params = params.append('inicio', inicio);
    params = params.append('cantidad', cantidad);
    params = params.append('orderBy', orderBy);
    params = params.append('orderDir', orderDir);
    params = params.append('like','S');
    params = params.append('ejemplo', '\{\"descripcion\":\"'+filtro+'\"\}');
    parametros = params.toString();
    console.log(parametros);

    return this.httpClient.get<any>(this.basePath, {params});
  }

  getCategoria(id): Observable<CategoriaPut> {
    return this.httpClient.get<CategoriaPut>(this.basePath+id);
  }
}
