import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import {Categoria} from "../models/categoria";
import {Observable} from "rxjs";
import { RecibirCategoriaService } from "./recibir-categoria.service";



@Injectable()
export class CategoriaResolver implements Resolve<Categoria> {

    constructor(private categoriaService:RecibirCategoriaService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Categoria> {
        return this.categoriaService.getCategoria(route.params['id']);
    }

}
