import { TestBed } from '@angular/core/testing';

import { CrearPacienteService } from './crear-paciente.service';

describe('CrearPacienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrearPacienteService = TestBed.get(CrearPacienteService);
    expect(service).toBeTruthy();
  });
});
