import { TestBed } from '@angular/core/testing';

import { EditarPacienteService } from './editar-paciente.service';

describe('EditarPacienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditarPacienteService = TestBed.get(EditarPacienteService);
    expect(service).toBeTruthy();
  });
});