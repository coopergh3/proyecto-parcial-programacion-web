import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HorarioExcepcion, HorarioBody1, HorarioBody2 } from '../models/horario-excepcion';
import { ListaHorariosExcepcion } from '../models/lista-horarios-excepcion';
import { Observable } from 'rxjs';
import { ListaPersona } from 'src/app/listaPersona';
import {LoginService} from '../../servicios/login.service';
import { Persona } from 'src/app/persona';

@Injectable({
  providedIn: 'root'
})
export class HorarioExcepcionService {

  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/horarioExcepcion";
  private basePath: string="/stock-pwfe/horarioExcepcion";

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      //'Access-Control-Allow-Headers': 'Content-Type',
      //'Authorization': 'AppiKey'
    })
  };

  constructor(
    private http: HttpClient,
    private login: LoginService
  ) { }

  getHorarios(inicio, cantidad,filtrar,fecha,empleado): Observable<ListaHorariosExcepcion>{
    
    if(filtrar == true){
      let ejemplo = '{';

      if(fecha != ""){
        ejemplo += '"fechaCadena":'+fecha;
        if(empleado){
          ejemplo += ',';
        }
      }

      if(empleado){
        ejemplo += '"idEmpleado":{"idPersona":' + empleado + '}'; 
      }

      ejemplo += '}'
      console.log(ejemplo);
      return this.http.get<ListaHorariosExcepcion>(this.basePath,{params:{
        inicio:inicio,cantidad:cantidad,
        orderBy:'fecha', orderDir:'asc',
        ejemplo: ejemplo}});

    }else{
      return this.http.get<ListaHorariosExcepcion>(this.basePath,{params:{
        inicio:inicio,cantidad:cantidad,
        orderBy:'fecha', orderDir:'asc'}});
    }
    
  }

  getHorario(id): Observable<HorarioExcepcion>{
    
    return this.http.get<HorarioExcepcion>(`${this.basePath}/${id}`);
    
  }
  //getHorarioFilter(inicio, cantidad, filter): Observable<>;

  getEmpleado(id): Observable<Persona>{
    return this.http.get<Persona>(`${"/stock-pwfe/persona"}/${id}`);
    //{params: {ejemplo: JSON.stringify(this.ejemplo)}});
  }

  agregarRegistro(horarioExcepcion: HorarioBody1  ): Observable<HorarioBody1>{

    const user: string = this.login.getCurrentUser();

    horarioExcepcion.horaAperturaCadena =  horarioExcepcion.horaAperturaCadena.replace(/:/g,'');
    horarioExcepcion.horaCierreCadena = horarioExcepcion.horaCierreCadena.replace(/:/g,'');
   
    //añadimos el usuario correspondiente : medico
    this.httpOptions.headers =  this.httpOptions.headers.set('usuario',user);
    console.log(this.httpOptions);
    console.log(horarioExcepcion);

    //transformamos los horarios a formato militar
    //horarioExcepcion.horaAperturaCadena = horario.horaAperturaCadena.replace(/:/g,'');
    //horarioExcepcion.horaCierreCadena = horario.horaCierreCadena.replace(/:/g,'');

    return this.http.post<HorarioBody1>(this.basePath, horarioExcepcion, this.httpOptions);
    
  }

  UpdateHorario(horario: HorarioExcepcion): Observable<HorarioExcepcion>{
    //.idTipoProducto= id;
    //console.log('subcat: ',subcategoria);
    const user: string = this.login.getCurrentUser();

    this.httpOptions.headers =  this.httpOptions.headers.set('usuario',user);
    console.log(this.httpOptions.headers);
    horario.horaAperturaCadena = horario.horaAperturaCadena.replace(/:/g,'');
    horario.horaCierreCadena = horario.horaCierreCadena.replace(/:/g,'');
    console.log(horario);
    return this.http.put<HorarioExcepcion>(this.basePath,horario, this.httpOptions);
  }

  DeleteHorarioExcepcion(id:number):Observable<{}>{
    return this.http.delete(`${this.basePath}/${id}`);
  }


}
