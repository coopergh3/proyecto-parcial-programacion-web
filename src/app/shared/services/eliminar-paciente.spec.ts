import { TestBed } from '@angular/core/testing';

import { EliminarPacienteService } from './eliminar-paciente.service';

describe('EliminarPacienteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EliminarPacienteService = TestBed.get(EliminarPacienteService);
    expect(service).toBeTruthy();
  });
});