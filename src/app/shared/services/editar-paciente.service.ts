import { Injectable } from '@angular/core';
import { PacientePut, PacientePutEnvio } from '../models/paciente';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EditarPacienteService {
  private basePath: string="/stock-pwfe/persona/";
  constructor(private http: HttpClient) { }
  
  editarPaciente(paciente:PacientePutEnvio): Observable<PacientePut>{
    return this.http.put<PacientePut>(this.basePath, paciente)
  }
}
