import { Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ListaProductos,Producto,IdProducto,IdTipoProducto } from '../models/producto';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CrearProductoService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private basePath: string="/stock-pwfe/presentacionProducto";
  
  constructor(private http: HttpClient) { }
  

  crearProducto(producto:Producto, precioVenta:number, codigoProducto:number): Observable<any>{
    //return this.http.post<Producto>(this.basePath, producto);
    //let obj =  {"producto":producto,"precioVenta":precioVenta};
    let obj =  {
        "codigo": codigoProducto,
        "flagServicio": "S",
        "nombre": producto.nombre,
        "descripcion": producto.descripcion,
        "idProducto": {
          "idProducto": producto.idProducto.idProducto,
          "idTipoProducto": {
            "idTipoProducto":producto.idProducto.idTipoProducto.idTipoProducto
          }
        },
        "existenciaProducto": {
            "precioVenta": precioVenta
        }
      }
      let json = JSON.stringify(obj);
      console.log(`json creado = ${json}`);
    //El backend recogerá un parametro json
    //let params = "json="+json;
    return this.http.post(this.basePath, json, this.httpOptions);
  }
}
