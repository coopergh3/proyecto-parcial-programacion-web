import { TestBed } from '@angular/core/testing';

import { RecibirCategoriaService } from './recibir-categoria.service';

describe('RecibirCategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RecibirCategoriaService = TestBed.get(RecibirCategoriaService);
    expect(service).toBeTruthy();
  });
});
