import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {environment} from "../../../environments/environment";
import { ListaArchivos } from '../models/archivo';

@Injectable({
  providedIn: 'root'
})
export class EnviarArchivoService {

  private basePath: string="/stock-pwfe/fichaArchivo";
  constructor(private http: HttpClient) { }


  enviarArchivo(archivo: File, idFichaClinica): Observable<any>{
    let formData:FormData = new FormData();
    formData.append('file', archivo);
    formData.append('nombre', archivo.name);
    formData.append('idFichaClinica', idFichaClinica);
    console.log("archivo: ",archivo)
    console.log("enviar-archivo.service envia:", formData);
        
    return this.http.post(this.basePath+'/archivo', formData);
  }

  getArchivos(id):Observable<ListaArchivos>{
    let params = new HttpParams();
    params = params.append('idFichaClinica', id);
    return this.http.get<ListaArchivos>(this.basePath,{params});
  }

  eliminarArchivos(id):Observable<any>{
      return this.http.delete<any>(this.basePath+'/'+id);
  }
}