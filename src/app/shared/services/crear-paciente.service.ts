import { Injectable } from '@angular/core';
import { Paciente } from '../models/paciente';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrearPacienteService {
  private basePath: string="/stock-pwfe/persona";
  constructor(private http: HttpClient) { }
  
  crearPaciente(paciente:Paciente): Observable<Paciente>{
    return this.http.post<Paciente>(this.basePath, paciente)
  }
}
