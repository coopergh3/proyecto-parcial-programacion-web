import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HorarioAtencion } from '../models/horario-atencion';
import { ListaHorarios } from '../models/ListaHorarios';
import { Observable } from 'rxjs';
import { ListaPersona } from 'src/app/listaPersona';
import {LoginService} from '../../servicios/login.service';
import { Persona } from 'src/app/persona';

@Injectable({
  providedIn: 'root'
})
export class HorarioAtencionService {

  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/personaHorarioAgenda";
  private basePath: string="/stock-pwfe/personaHorarioAgenda";

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      //'Access-Control-Allow-Headers': 'Content-Type',
      //'Authorization': 'AppiKey'
    })
  };

  constructor(
    private http: HttpClient,
    private login: LoginService
  ) { }

  getHorarios(inicio, cantidad, filtrar, dia, empleado): Observable<ListaHorarios>{
    
    //const filtro = JSON.stringify(filter);
    //console.log(filtro);

    if(filtrar == true){
      let ejemplo = '{';

      if(dia != ""){
        ejemplo += '"dia":'+dia;
        if(empleado){
          ejemplo += ',';
        }
      }

      if(empleado){
        ejemplo += '"idEmpleado":{"idPersona":' + empleado + '}'; 
      }

      ejemplo += '}'
      console.log(ejemplo);
      return this.http.get<ListaHorarios>(this.basePath,{params:{
        inicio:inicio,cantidad:cantidad,
        orderBy:'idPersonaHorarioAgenda', orderDir:'asc',
        ejemplo: ejemplo}});

    }else{
      return this.http.get<ListaHorarios>(this.basePath,{params:{
        inicio:inicio,cantidad:cantidad,
        orderBy:'idPersonaHorarioAgenda', orderDir:'asc'}});
    }
    
  }
  //getHorarioFilter(inicio, cantidad, filter): Observable<>;

  getEmpleado(id): Observable<Persona>{
    return this.http.get<Persona>(`${"https://gy7228.myfoscam.org:8443/stock-pwfe/persona"}/${id}`);
    //{params: {ejemplo: JSON.stringify(this.ejemplo)}});
  }

  addHorario(horario: HorarioAtencion): Observable<HorarioAtencion>{
    const user: string = this.login.getCurrentUser();

    //añadimos el usuario correspondiente : medico
    this.httpOptions.headers =  this.httpOptions.headers.set('usuario',user);
    console.log(this.httpOptions);

    //transformamos los horarios a formato militar
    horario.horaAperturaCadena = horario.horaAperturaCadena.replace(/:/g,'');
    horario.horaCierreCadena = horario.horaCierreCadena.replace(/:/g,'');

    return this.http.post<HorarioAtencion>(this.basePath, horario, this.httpOptions);
  }

  UpdateHorario(horario: HorarioAtencion): Observable<HorarioAtencion>{
    //.idTipoProducto= id;
    //console.log('subcat: ',subcategoria);
    this.httpOptions.headers =  this.httpOptions.headers.set('usuario',horario.idEmpleado.nombre.toLowerCase());
    console.log(this.httpOptions.headers);
    console.log(horario);
    horario.horaAperturaCadena = horario.horaAperturaCadena.replace(/:/g,'');
    horario.horaCierreCadena = horario.horaCierreCadena.replace(/:/g,'');
    return this.http.put<HorarioAtencion>(this.basePath,horario, this.httpOptions);
  }

  DeleteHorario(id:number):Observable<{}>{
    return this.http.delete(`${this.basePath}/${id}`);
  }

  getHorario(id): Observable<HorarioAtencion>{
    return this.http.get<HorarioAtencion>(`${this.basePath}/${id}`);
  }
}
