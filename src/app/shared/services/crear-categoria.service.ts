import { Injectable } from '@angular/core';
import { Categoria } from '../models/categoria';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrearCategoriaService {
  private basePath: string="/stock-pwfe/categoria";
  constructor(private http: HttpClient) { }

  crearCategoria(categoria: Categoria): Observable<Categoria> {
    return this.http.post<Categoria>(this.basePath, categoria)
  }
}
