import { Injectable } from '@angular/core';
import { Paciente } from '../models/paciente';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EliminarPacienteService {
  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/persona/";
  private basePath: string="/stock-pwfe/persona/";

  constructor(private http: HttpClient) { }
  eliminarPaciente(id): Observable<Number>{
    return this.http.delete<Number>(this.basePath+id);
  }
}
