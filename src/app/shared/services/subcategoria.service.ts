import { Injectable } from '@angular/core';
import { Subcategoria } from '../models/subcategoria';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Categoria } from '../models/categoria';
import { ListaSubcategoria } from '../models/lista-subcategoria';

@Injectable({
  providedIn: 'root'
})
export class SubcategoriaService {
  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/tipoProducto";
  private basePath: string="/stock-pwfe/tipoProducto";

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(
    private http: HttpClient
  ) {}

  // BORRAR SI ALGUIEN YA HIZO
  getsubcategoriaconCat(id: any) {
    const urlAux = '/stock-pwfe/tipoProducto';
    const valor = '{"idCategoria":{"idCategoria": ' + id + '}}';
    console.log(urlAux);
    return this.http.get(urlAux, {params: {
      ejemplo: valor
      }});
  }

  crearSubCategoria(subcategoria: Subcategoria): Observable<Subcategoria>{
    return this.http.post<Subcategoria>(this.basePath, subcategoria);
  }

  getSubCategorias(inicio,cantidad,filtrar,desc,categoria): Observable<ListaSubcategoria>{
    
    /*if(desc != ""){
      let ejemplo = JSON.stringify({descripcion:desc});
      console.log(ejemplo);
      return this.http.get<ListaSubcategoria>(this.basePath,{params:{
      inicio:inicio,cantidad:cantidad,orderBy:'descripcion', orderDir:'asc',
      like:'S',ejemplo:ejemplo}});

    }*/
    if(filtrar == true){
      let ejemplo = '{';

      if(desc != "" && desc != null){
        ejemplo += '"descripcion":"'+desc+'"';
        if(categoria){
          ejemplo += ',';
        }
      }

      if(categoria){
        ejemplo += '"idCategoria":{"idCategoria":' + categoria.idCategoria + '}'; 
      }

      ejemplo += '}'

      return this.http.get<ListaSubcategoria>(this.basePath,{params:{
        inicio:inicio,cantidad:cantidad,orderBy:'descripcion', orderDir:'asc',
        like:'S',ejemplo:ejemplo}});

    }
    return this.http.get<ListaSubcategoria>(this.basePath,{params:{
      inicio:inicio,cantidad:cantidad,orderBy:'descripcion', orderDir:'asc'}});
  }

  getSubCategorias2(idCategoria): Observable<ListaSubcategoria>{
    let params = new HttpParams();
    params = params.append("ejemplo", '{"idCategoria":{"idCategoria":'+idCategoria+'}}');
    return this.http.get<ListaSubcategoria>(this.basePath,{params});
  }

  getSubCategoria(id): Observable<Subcategoria>{
    return this.http.get<Subcategoria>(`${this.basePath}/${id}`);
  }

  UpdateSubCategoria(subcategoria: Subcategoria): Observable<Subcategoria>{
    //.idTipoProducto= id;
    //console.log('subcat: ',subcategoria);
    return this.http.put<Subcategoria>(this.basePath,subcategoria, this.httpOptions);
  }

  DeleteSubCategoria(id:number):Observable<{}>{
    return this.http.delete(`${this.basePath}/${id}`);
  }
}
