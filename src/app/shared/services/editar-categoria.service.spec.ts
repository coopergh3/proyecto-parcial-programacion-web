import { TestBed } from '@angular/core/testing';

import { EditarCategoriaService } from './editar-categoria.service';

describe('EditarCategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EditarCategoriaService = TestBed.get(EditarCategoriaService);
    expect(service).toBeTruthy();
  });
});
