import { TestBed } from '@angular/core/testing';

import { EliminarCategoriaService } from './eliminar-categoria.service';

describe('EliminarCategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EliminarCategoriaService = TestBed.get(EliminarCategoriaService);
    expect(service).toBeTruthy();
  });
});
