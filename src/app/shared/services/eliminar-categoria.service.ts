import { Injectable } from '@angular/core';
import { Categoria } from '../models/categoria';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EliminarCategoriaService {
  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/categoria/";
  private basePath: string="/stock-pwfe/categoria/";

  constructor(private http: HttpClient) { }
  eliminarCategoria(id): Observable<Number>{
    return this.http.delete<Number>(this.basePath+id);
  }
}
