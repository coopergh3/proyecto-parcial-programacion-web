import { TestBed } from '@angular/core/testing';

import { HorarioAtencionService } from './horario-atencion.service';

describe('HorarioAtencionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HorarioAtencionService = TestBed.get(HorarioAtencionService);
    expect(service).toBeTruthy();
  });
});
