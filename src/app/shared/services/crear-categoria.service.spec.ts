import { TestBed } from '@angular/core/testing';

import { CrearCategoriaService } from './crear-categoria.service';

describe('CrearCategoriaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrearCategoriaService = TestBed.get(CrearCategoriaService);
    expect(service).toBeTruthy();
  });
});
