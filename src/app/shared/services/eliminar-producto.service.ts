import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EliminarProductoService {

  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto";
  private basePath: string="/stock-pwfe/presentacionProducto";

  constructor(private http: HttpClient) { }
  
  eliminarProducto(idPresentacionProducto:number): Observable<Number>{
    return this.http.delete<Number>(`${this.basePath}/${idPresentacionProducto}`);
  }
}
