import { Injectable } from '@angular/core';
import { Ficha, FichaLista } from '../models/ficha';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Persona } from 'src/app/persona';
import { LoginService } from 'src/app/servicios/login.service';

@Injectable({
  providedIn: 'root'
})
export class FichaService {
  private basePath = '/stock-pwfe/fichaClinica';

  constructor(
    private http: HttpClient,
    private loginService: LoginService
    ) { }

  registrarFicha(ficha:Ficha):Observable<Ficha>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'usuario': this.loginService.getCurrentUser()// cambiar despues por el nombre del usuario que innicio sesion
      })};
    return this.http.post<Ficha>(this.basePath, ficha, httpOptions);
  }

  obtenerFichas():Observable<FichaLista>{
    return this.http.get<FichaLista>(this.basePath);
  }
  
  obtenerFicha(id):Observable<Ficha>{
    return this.http.get<Ficha>(this.basePath+"/"+id);
  }

  obtenerFichasParams(cliente:Persona, empleado:Persona, fechaDesdeCadena: string, fechaHastaCadena: string):Observable<FichaLista>{
    let params = new HttpParams();
    if(!cliente){
      if(empleado && fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{"idEmpleado":{"idPersona": ${empleado.idPersona}}, 
        "fechaDesdeCadena":"${fechaDesdeCadena}","fechaHastaCadena":"${fechaHastaCadena}"}`);
      }else if(empleado && fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{"idEmpleado":{"idPersona": ${empleado.idPersona}}, 
        "fechaDesdeCadena":"${fechaDesdeCadena}"}`);
      }else if(empleado && !fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{"idEmpleado":{"idPersona": ${empleado.idPersona}}, 
        "fechaHastaCadena":"${fechaHastaCadena}"}`);
      }else if(empleado && !fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{"idEmpleado":{"idPersona": ${empleado.idPersona}}}`);
      }else if(!empleado && fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{"fechaDesdeCadena":"${fechaDesdeCadena}","fechaHastaCadena":"${fechaHastaCadena}"}`);
      }else if(!empleado && fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{"fechaDesdeCadena":"${fechaDesdeCadena}"}`);
      }else if(!empleado && !fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{"fechaHastaCadena":"${fechaHastaCadena}"}`);
      }
    }else{
      if(empleado && fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{
          "idEmpleado":{"idPersona": ${empleado.idPersona}},
          "idCliente": {"idPersona": ${cliente.idPersona}},
          "fechaDesdeCadena":"${fechaDesdeCadena}",
          "fechaHastaCadena":"${fechaHastaCadena}"
        }`);
      }else if(empleado && fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{
          "idEmpleado":{"idPersona": ${empleado.idPersona}},
          "idCliente": {"idPersona": ${cliente.idPersona}},
          "fechaDesdeCadena":"${fechaDesdeCadena}"
        }`);
      }else if(empleado && !fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{
          "idEmpleado":{"idPersona": ${empleado.idPersona}},
          "idCliente": {"idPersona": ${cliente.idPersona}},
          "fechaHastaCadena":"${fechaHastaCadena}"
        }`);
      }else if(empleado && !fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{
          "idEmpleado":{"idPersona": ${empleado.idPersona}},
          "idCliente": {"idPersona": ${cliente.idPersona}}
        }`);
      }else if(!empleado && fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{
          "fechaDesdeCadena":"${fechaDesdeCadena}",
          "fechaHastaCadena":"${fechaHastaCadena}",
          "idCliente": {"idPersona": ${cliente.idPersona}}
        }`);
      }else if(!empleado && fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`"{
          fechaDesdeCadena":"${fechaDesdeCadena}",
          "idCliente": {"idPersona": ${cliente.idPersona}}
        }`);
      }else if(!empleado && !fechaDesdeCadena && fechaHastaCadena){
        params = params.set("ejemplo",`{
          "fechaHastaCadena":"${fechaHastaCadena}",
          "idCliente": {"idPersona": ${cliente.idPersona}}
        }`);
      }else if(!empleado && !fechaDesdeCadena && !fechaHastaCadena){
        params = params.set("ejemplo",`{
          "idCliente": {"idPersona": ${cliente.idPersona}}
        }`);
      }
    }
    console.log("en fichaService se envia: ", params);
    return this.http.get<FichaLista>(this.basePath, { params });
  }

  eliminarFicha(id): Observable<Number>{
    return this.http.delete<Number>(this.basePath+"/"+id);
  }

  editarFicha(ficha): Observable<Ficha>{
    return this.http.put<Ficha>(this.basePath, ficha);
  }
}
