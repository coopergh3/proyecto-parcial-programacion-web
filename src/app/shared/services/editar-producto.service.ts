import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpHeaderResponse, HttpParams } from '@angular/common/http';
import { ListaProductos,Producto,IdProducto,IdTipoProducto } from '../models/producto';
import {ListaExistenciaProductos, ExistenciaProducto, EditListaProductos, EditProducto} from '../models/producto';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditarProductoService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  httpOptionUserLogged ={
    headers: new HttpHeaders({ "usuario": "ana"})
  };

  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto";
  private basePath: string="/stock-pwfe/presentacionProducto";

  constructor(private http: HttpClient) { }
  
  /*Hay que pasarle idPresentacionProducto para que funcione*/ 
  editarProducto(idPresentacionProducto:number, producto:Producto, precioVenta:number, codigoProducto:number): Observable<any>{
    //return this.http.post<Producto>(this.basePath, producto);
    //let obj =  {"producto":producto,"precioVenta":precioVenta};
    let obj =  {
        "idPresentacionProducto":`${idPresentacionProducto}`,
        "codigo": codigoProducto,
        "flagServicio": "S",
        "nombre": producto.nombre,
        "descripcion": producto.descripcion,
        "idProducto": {
          "idProducto": producto.idProducto.idProducto,
          "idTipoProducto": {
            "idTipoProducto":producto.idProducto.idTipoProducto.idTipoProducto
          }
        },
        "existenciaProducto": {
            "precioVenta": precioVenta
        }
      }
      let json = JSON.stringify(obj);
      console.log(`json creado = ${json}`);
    //El backend recogerá un parametro json
    //let params = "json="+json;
    return this.http.put(this.basePath, json, this.httpOptions);
  }

  getPrecioProducto(idPresentacionProducto:number): Observable<ListaExistenciaProductos>{
    //console.log(`idPresentacionProducto llego ${idPresentacionProducto}`);
    // let url = 'https://gy7228.myfoscam.org:8443/stock-pwfe/existenciaProducto';
    let url = '/stock-pwfe/existenciaProducto';
    //console.log(url);
  
    return this.http.get<ListaExistenciaProductos>(url, {params: new HttpParams().
      set('ejemplo', `{"idPresentacionProductoTransient":${idPresentacionProducto}}`),
      headers: new HttpHeaders().
      set('usuario','ana')}
      );
  }

  getProductoByIdPresentacionProducto(idPresentacionProducto:number): Observable<EditListaProductos>{
    //console.log(`idPresentacionProducto llego ${idPresentacionProducto}`);
    let url = '/stock-pwfe/presentacionProducto';
    //console.log(url);
    return this.http.get<EditListaProductos>(url, {params: new HttpParams().
      set('ejemplo', `{"idPresentacionProducto":${idPresentacionProducto}}`)
    });
  }

}
