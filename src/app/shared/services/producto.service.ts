import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {
  ListaProductos,
  Producto,
  IdProducto,
  IdTipoProducto,
  SoloIdTipoProducto,
  ProductoTipoProducto,
  ProductoId
} from '../models/producto';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { MensajesProductosService } from '../services/mensajes-productos.service';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  // private productoUrl = "https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto";  // URL to web api
  private productoUrl = '/stock-pwfe/presentacionProducto';  // URL to web api
  // private productoUrlOrderByIdProducto = 'https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto?orderBy="idPresentacionProducto"';
  private productoUrlOrderByIdProducto = '/stock-pwfe/presentacionProducto?orderBy="idPresentacionProducto"';

  // private params = new HttpParams();
  constructor(
    private http: HttpClient,
    private mensajeProductoService: MensajesProductosService,
  ) { }

  /*getListaProductos(): Observable<Producto[]> {
    this.mensajeProductoService.add('ProductoService: fetched Productos y/o Servicios');
    return this.http.get<Producto[]>(this.productoUrl)
      .pipe(
        tap(_ => this.log('fetched Productos y/o Servicios')),
        catchError(this.handleError<Producto[]>('getListaProductos', []))
      );
  }
  */

  getListaProductos():Observable<ListaProductos> {
    //return this.http.get<ListaProductos>(this.productoUrlOrderByIdProducto);
    return this.http.get<ListaProductos>(this.productoUrl, {params: new HttpParams().
      set('inicio', '0').
      set('cantidad', '1').
      set('orderBy', 'idPresentacionProducto').
      set('orderDir', 'asc')
    });
  }

  getListaProductosPaginated(inicio:number,cantidad:number, orderBy:string, orderDir:string, ejemploString:string, opcionFiltrado?:string): Observable<any>{
    let url = `${this.productoUrl}`
    console.log(`Op! ejemploString=${ejemploString}`);
    /*this.params.set('inicio', inicio.toString());
    this.params.set('cantidad', cantidad.toString());
    this.params.set('orderBy', orderBy);
    this.params.set('orderDir', orderDir);*/
    //falta el query param que es de filtro, mirar like y ejemplo
    if(opcionFiltrado && opcionFiltrado==="nombre"){
      return this.http.get<any>(url, {params: new HttpParams().
        set('inicio', inicio.toString()).
        set('cantidad', cantidad.toString()).
        set('orderBy', orderBy).
        set('orderDir', orderDir).
        set('ejemplo',`{"nombre":"${ejemploString}"}`).//filtrando por nombre
        set('like','S')
      });
    }else if(opcionFiltrado && opcionFiltrado==="idTipoProducto"){
      return this.http.get<any>(url, {params: new HttpParams().
        set('inicio', inicio.toString()).
        set('cantidad', cantidad.toString()).
        set('orderBy', orderBy).
        set('orderDir', orderDir).
        set('ejemplo',`{"idProducto":{"idTipoProducto":{"idTipoProducto":${ejemploString} }}}`)//filtrando por idTipoProducto o "subcategoria"
      });
    }

    return this.http.get<any>(url, {params: new HttpParams().
        set('inicio', inicio.toString()).
        set('cantidad', cantidad.toString()).
        set('orderBy', orderBy).
        set('orderDir', orderDir)
      });


  }

  getPresentacionProducto(idTipo: number) {
    const tipo = new ProductoTipoProducto();
    tipo.idTipoProducto = {idTipoProducto: idTipo};

    const producto = new ProductoId();
    producto.idProducto = tipo;

    const json = JSON.stringify(producto);
    return this.http.get(this.productoUrl, {params: {
      ejemplo: json
    }});
  }
}
