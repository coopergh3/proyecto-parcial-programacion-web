import { Injectable } from '@angular/core';
import { CategoriaPut } from '../models/categoria';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EditarCategoriaService {
  private basePath: string="/stock-pwfe/categoria/";

  constructor(private http: HttpClient) { }
  editarCategoria(categoria:CategoriaPut): Observable<CategoriaPut>{
    return this.http.put<CategoriaPut>(this.basePath, categoria);
  }
}
