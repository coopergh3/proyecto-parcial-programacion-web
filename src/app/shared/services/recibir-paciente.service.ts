import { Injectable } from '@angular/core';
import { Paciente, PacientePut } from '../models/paciente';
import { ListaPaciente } from '../models/lista-pacientes';
import { Observable, of } from 'rxjs';
import {environment} from "../../../environments/environment";
import { HttpParams, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RecibirPacienteService {
  // private basePath: string="https://gy7228.myfoscam.org:8443/stock-pwfe/persona/";
  private basePath: string="/stock-pwfe/persona/";
  constructor(private httpClient: HttpClient) { }

  getPacientes(): Observable<ListaPaciente> {
    let params = new HttpParams();
    return this.httpClient.get<ListaPaciente>(this.basePath, {params});
  }
  getPaciente(id): Observable<PacientePut> {
    return this.httpClient.get<PacientePut>(this.basePath+id);
  }
}
