import {Persona} from '../../persona';

export class Agenda {
    idReserva: number;
    fecha: string;
    horaInicio: string;
    horaFin: string;
    fechaHoraCreacion: string;
    flagEstado: string;
    flagAsistio: string;
    observacion: string;
    idFichaClinica: string;
    idLocal: string;
    idCliente: Persona;
    idEmpleado: Persona;
    fechaCadena: string;
    fechaDesdeCadena: string;
    fechaHastaCadena: string;
    horaInicioCadena: string;
    horaFinCadena: string;
}
