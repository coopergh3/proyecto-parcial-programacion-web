import { HorarioExcepcion } from "./horario-excepcion";

export interface ListaHorariosExcepcion {
    lista: HorarioExcepcion[];
    totalDatos: number;
}
