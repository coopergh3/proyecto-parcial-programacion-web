export class Paciente {
    constructor(
        public idPersona: number,
        public nombre: string,
        public apellido: string,
        public telefono: string,
        public email: string,
        public ruc: string,
        public cedula: string,
        public tipoPersona: string,
        public fechaNacimiento: Date,
    ){}
}

export class PacientePut {
    constructor(
        public idPersona: number,
        public nombre: string,
        public apellido: string,
        public email: string,
        public telefono: string,
        public seguroMedico: string,
        public seguroMedicoNumero: number,
        public ruc: string,
        public cedula: string,
        public tipoPersona: string,
        public usuarioLogin: string,
        public idLocalDefecto: string,
        public flagVendedor: string,
        public observacion: string,
        public tipoCliente: string,
        public fechaHoraAprobContrato: string,
        public soloUsuariosDelSistema: string,
        public nombreCompleto: string,
        public limiteCredito: number,
        public fechaNacimiento: Date,
        public soloProximosCumpleanhos: string,
        public todosLosCampos: string
    ){}
}
export class PacientePutEnvio {
    constructor(
        public idPersona: number,
        public nombre: string,
        public apellido: string,
        public email: string,
        public telefono: string,
        public seguroMedico: string,
        public seguroMedicoNumero: number,
        public ruc: string,
        public cedula: string,
        public tipoPersona: string,
        public usuarioLogin: string,
        public idLocalDefecto: string,
        public flagVendedor: string,
        public observacion: string,
        public tipoCliente: string,
        public fechaHoraAprobContrato: string,
        public soloUsuariosDelSistema: string,
        public nombreCompleto: string,
        public limiteCredito: number,
        public fechaNacimiento: string,
        public soloProximosCumpleanhos: string,
        public todosLosCampos: string
    ){}
}