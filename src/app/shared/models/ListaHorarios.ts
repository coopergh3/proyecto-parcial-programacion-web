import { HorarioAtencion } from "./horario-atencion";

export interface ListaHorarios {
    lista: HorarioAtencion[];
    totalDatos: number;
}
