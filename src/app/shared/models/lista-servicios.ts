import {Producto} from './producto';

export interface ListaServicios{
     lista: Servicio[];
     totalDatos: number;
}


export interface Servicio {
      idServicio:number;
      fechaHora: string;
      fechaHoraCadena: string;
      descripcion:string;
      idFichaClinica:IdFichaClinica;
    

}

export interface IdFichaClinica{
     idFichaClinica:number;
     fechaHora:string;
     fechaHoraCadena:string;
     idLocal:IdLocal;  
     idCliente: IdCliente;
     idTipoProducto: IdTipoProducto;  
}

export interface IdCliente{
     idPersona:number;
     nombre:string;    
}

export interface IdLocal{
     idLocal:number;
     fechaHora:number;
     idEmpleado:IdEmpleado;    
}

export interface IdEmpleado{
     idPersona:number;
     nombre:string;

         
}

export interface IdTipoProducto{
    idTipoProducto:number;
    idCategoria: IdCategoria;
}

export interface IdCategoria{
     idCategoria:number;
     descripcion:string;
}

export interface DetalleServicio{
     idServicioDetalle: number;
     cantidad: number;
     idPresentacionProducto: Producto;
     precioVenta:number;
}