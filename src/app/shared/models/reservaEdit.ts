export class ReservaEdit {
    constructor(
        public idReserva: number,
        public observacion: string,
        public flagAsistio: string
    ) {}
}

