import { Producto } from "./producto";

export interface ListaReportes{
     lista: Reporte[];
     totalDatos: number;
}

export interface ListaReporteDetalles{
     lista: DetalleReporte[];
     totalDatos:number;
}

export interface DetalleReporte{
     idServicio: number;
     idServicioDetalle: number;
     cantidad:number;
     idPresentacionProducto:Producto;
     precioVenta: number;
     total: number;
     reporte: Reporte;
}

export interface Reporte {
      idServicio: number;
      fechaHora: string;
      presupuesto: number;
      fechaHoraCadena
      descripcion:string;
      idFichaClinica:IdFichaClinica;
    

}

export interface IdFichaClinica{
     idFichaClinica:number;
     fechaHora:string;
     fechaHoraCadena:string;
     idLocal:IdLocal;
     idEmpleado:IdEmpleado;  
     idCliente: IdCliente;
     idTipoProducto: IdTipoProducto;  
}

export interface IdCliente{
     idPersona:number;
     nombre:string; 
     apellido:string;   
}

export interface IdLocal{
     idLocal:number;
     fechaHora:number;
     idEmpleado:IdEmpleado;    
}

export interface IdEmpleado{
     idPersona:number;
     nombre:string;
     apellido:string;

         
}

export interface IdTipoProducto{
    idTipoProducto:number;
    descripcion: string;
    idCategoria: IdCategoria;
}

export interface IdCategoria{
     idCategoria:number;
     descripcion:string;
}
