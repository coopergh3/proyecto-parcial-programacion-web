import {PersonaPost} from './reservaPost';
import {IdLocalDefecto} from '../../idLocalDefecto';
import {Persona} from '../../persona';
import {PresentacionProducto, PresentacionProductoId} from './producto';

export class ComisionConTerapeuta {
    idPresentacionProducto: {idPresentacionProducto: number};
    idEmpleado: PersonaPost;
    porcentajeComision: number;
}

export class ComisionSinTerapeuta {
    idPresentacionProducto: {idPresentacionProducto: number};
    porcentajeComision: number;
}

export class ComisionEditar {
    idComisionEmpleado: number;
    idPresentacionProducto: PresentacionProductoId;
    idEmpleado: PersonaPost;
    porcentajeComision: number;

}

export class ComisionCompleto {
    idComisionEmpleado: number;
    fechaHora: string; // "2019-09-22 20:33:01"
    idLocal: IdLocalDefecto;
    idEmpleado: Persona;
    idPresentacionProducto: PresentacionProducto;
    porcentajeComision: number;
}

