import { Reserva } from './reserva';

export interface ListaReserva {
    lista: Reserva[];
    totalDatos: number;
}
