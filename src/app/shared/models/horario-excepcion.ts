import { IdLocalDefecto } from "../../idLocalDefecto";
import { Persona } from "src/app/persona";

export interface HorarioExcepcion {

    idHorarioExcepcion: number;
    fecha: string;
    horaApertura: string;
    horaCierre: string;
    flagEsHabilitar: string;
    idLocal: IdLocalDefecto;
    idEmpleado: Persona;
    intervaloMinutos: number;
    horaAperturaCadena: string;
    horaCierreCadena: string;
    fechaCadena: string;

}


export interface HorarioBody1{
    fechaCadena: string;
    horaAperturaCadena: string;
    horaCierreCadena: string;
    flagEsHabilitar: string;
    idEmpleado: Persona;
    intervaloMinutos: number;
}

export interface HorarioBody2{
    fechaCadena: string;
    flagEsHabilitar: string;
    idEmpleado: Persona;
    intervaloMinutos: number;
}

