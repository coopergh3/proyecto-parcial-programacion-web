/*Aquí se incluyen todas las clases necesarias para acceder directamente a los datos del json object
al invocar el metodo get para:  https://gy7228.myfoscam.org:8443/stock-pwfe/presentacionProducto 
*/

export interface ListaProductos {
     lista: Producto[];
     totalDatos: number;
}

export interface Producto {
     idPresentacionProducto: number;
         nombre: string;
          descripcion: string;
          idProducto: IdProducto;
}

export interface EditListaProductos{
     lista: EditProducto[];
     totalDatos: number;
}


export interface EditProducto {
     idPresentacionProducto:number;
     nombre: string;
     descripcion:string;
     idProducto:IdProducto;
     codigo:number;
}

export interface IdProducto{
     idProducto:number;
     idTipoProducto:IdTipoProducto;
    
}

export interface IdTipoProducto {
    idTipoProducto: number;
}

export interface ListaExistenciaProductos{
     lista: ExistenciaProducto[];
     totalDatos:number;
}

export interface ExistenciaProducto{
     precioVenta: number;
}

export class SoloIdProducto {
    idProducto: number;
}

export class SoloIdTipoProducto {
    idTipoProducto: number;
}

export class ProductoTipoProducto {
    idTipoProducto: SoloIdTipoProducto;
}

export class ProductoId {
    // @ts-ignore
    idProducto: ProductoTipoProducto;
}

export class ExistenciaProducto {
    precioVenta: number;
}

export class PresentacionProductoId {
    idPresentacionProducto: {idPresentacionProducto: number};
}


export class PresentacionProducto {
    codigo: number;
    flagServicio: string; // "S"
    idProducto: SoloIdProducto;
    nombre: string; // "TRATAMIENTO RODILLA"
    existenciaProducto: ExistenciaProducto;
}
