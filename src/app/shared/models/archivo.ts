export class Archivos{
    constructor(
        public idFichaArchivo: number,
        public nombre: string,
        public urlImagen: string
    ){}
}

export class ListaArchivos{
    constructor(
        public lista: Archivos[],
        public totalDatos: number
    ){}
}
