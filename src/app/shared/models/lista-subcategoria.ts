import { Subcategoria } from './subcategoria';

export class ListaSubcategoria {
    public lista: Subcategoria[];
    public totalDatos: number;
}
