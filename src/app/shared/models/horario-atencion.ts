import { IdLocalDefecto } from '../../idLocalDefecto';
import { Persona } from '../../persona';


export interface HorarioAtencion {
    idPersonaHorarioAgenda: number;
    dia: number;
    horaApertura: string;
    horaCierre: string;
    intervaloMinutos: number;
    idLocal: IdLocalDefecto;
    idEmpleado: Persona;
    horaAperturaCadena: string;
    horaCierreCadena: string;
    diaCadena: string;

}
