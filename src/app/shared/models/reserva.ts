import { IdLocalDefecto } from '../../idLocalDefecto';
import { Persona } from '../../persona';

export class Reserva {
    idReserva: number;
    fecha: string; // "2019-09-03"
    horaInicio: string; // "10:00:00"
    horaFin: string; // "10:15:00"
    fechaHoraCreacion: string; // "2019-08-27 08:31:43"
    flagEstado: string; // "R"
    flagAsistio: string;
    observacion: string;
    idFichaClinica: string;
    idLocal: IdLocalDefecto;
    idCliente: Persona;
    idEmpleado: Persona;
    fechaCadena: string; // "20190903"
    fechaDesdeCadena: string; // "20190903"
    fechaHastaCadena: string; // "20190903"
    horaInicioCadena: string; // "1000"
    horaFinCadena: string; // "1015"
}
