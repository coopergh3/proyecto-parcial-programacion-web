import { Persona } from "src/app/persona";
import { Categoria } from "./categoria";


export interface Ficha {
    idFichaClinica: number,
    fechaHora: string,
    motivoConsulta: string,
    diagnostico: string,
    observacion: string,
    idLocal: string, 
    idEmpleado: Persona,
    idCliente: Persona,
    idTipoProducto: Producto,
    fechaHoraCadena: String,
    fechaDesdeCadena: string,
    fechaHastaCadena: string
}

export interface Producto {
    idTipoProducto: number,
    descripcion: string,
    flagVisible: string,
    idCategoria: Categoria,
    posicion: number
}

export interface FichaLista{
    lista: Ficha[],
    totalDatos: number
}