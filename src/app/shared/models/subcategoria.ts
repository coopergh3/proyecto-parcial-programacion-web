import {Categoria} from './categoria';

export class Subcategoria {
    public idTipoProducto: number;
    public descripcion: string;
    public idCategoria: Categoria;
}
