export class ReservaPost {
    constructor(
        public fechaCadena: string,
        public horaInicioCadena: string,
        public horaFinCadena: string,
        public idEmpleado: PersonaPost,
        public idCliente: PersonaPost,
        public observacion: string,
    ) {}
}

export class PersonaPost {
    constructor(
        public idPersona: string
    ) {}
}

