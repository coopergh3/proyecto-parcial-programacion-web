import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Ficha } from 'src/app/shared/models/ficha';
import { Router, ActivatedRoute } from '@angular/router';
import { FichaService } from 'src/app/shared/services/ficha.service';
import swal from 'sweetalert2';
import { EnviarArchivoService } from 'src/app/shared/services/enviar-archivo.service';
import { switchMap } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-editar-ficha',
  templateUrl: './editar-ficha.component.html',
  styleUrls: ['./editar-ficha.component.css']
})
export class EditarFichaComponent implements OnInit {
  private ficha: Ficha;
  private id: number;

  editarFichaForm = new FormGroup({
    observacion: new FormControl(''),
    archivo: new FormControl(),
  });
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fichaService: FichaService,
    private formBuilder: FormBuilder,
    private enviarArchivo: EnviarArchivoService,
    private tituloService: Title
  ) { }

  ngOnInit() {
    this.tituloService.setTitle("Editar ficha");
    this.route.params.pipe(
      switchMap((params)=> {
        console.log(params);
        this.id = params.idFicha;
        return this.fichaService.obtenerFicha(this.id);
      })
    ).subscribe(
      data =>{
        this.ficha = data;
        this.editarFichaForm = this.formBuilder.group({
          observacion: this.ficha.observacion
        })
      },
      error => console.log(error)
    )
  }

  onSubmit(){
    this.editarFichaForm.removeControl('archivo');
    this.ficha.observacion = this.editarFichaForm.value.observacion;
    this.ficha.fechaHora = null;
    this.ficha.idEmpleado.fechaHoraAprobContrato = null;
    this.ficha.idEmpleado.fechaNacimiento = null;
    this.ficha.idCliente.fechaNacimiento = null;
    this.fichaService.editarFicha(this.ficha).subscribe(
      data => {
        this.enviar(this.archivoInput, data.idFichaClinica);
        this.editadoCorrectamente();
        this.router.navigate(['ficha','lista-fichas']);
      },
      error => this.errorRecibido(error)
    )
  }

  editadoCorrectamente() {
    console.log("Se creo correctamente la ficha.");
    swal({
        title: "Categoria actualizada!",
        text: "Se edito correctamente la ficha!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al editar la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

/* MANEJO DE ARCHIVOS */

@ViewChild('archivoInput', {static:false}) archivoInput;
public isLoadingArchivo: boolean=false;
public errorArchivo: boolean=false;
public archivoName: string;

public enviar(fileInput, idFichaClinica){
  let file: File= fileInput.nativeElement.files[0];
  this.enviarArchivo.enviarArchivo(file, idFichaClinica).subscribe(
    (resultPath) => {
      this.isLoadingArchivo=false;
      console.log("Se subio correctamente el archivo");
    },
    (error) => {
        this.errorArchivo=true;
        this.vaciarInputArchivo();
      console.log("Ha ocurrido un error al subir el archivo");
    }
  );
    
  }

vaciarInputArchivo(){
  this.isLoadingArchivo=false;
  this.archivoInput.nativeElement.value='';
  this.archivoName='';
}

public cargarArchivo(){
    if (this.archivoInput.nativeElement && this.archivoInput.nativeElement.files[0]) {
    this.errorArchivo=false;
    this.isLoadingArchivo=true;
    this.archivoName=this.archivoInput.nativeElement.files[0].name;
  } 
}


}
