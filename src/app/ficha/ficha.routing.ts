import { Routes } from '@angular/router';

import { RegistrarFichaComponent } from './registrar-ficha/registrar-ficha.component';
import { ListaFichasComponent } from './lista-fichas/lista-fichas.component';
import { VerFichaComponent } from './ver-ficha/ver-ficha.component';
import { EditarFichaComponent } from './editar-ficha/editar-ficha.component';

export const FichaRoutes: Routes = [
    {
        path: '',
        children: [{
            path: 'lista-fichas',
            component: ListaFichasComponent
        }]
    },{
        path: '',
        children: [{
            path: 'ver-ficha/:idFicha',
            component: VerFichaComponent
        }]
    },{
        path: '',
        children: [{
            path: 'editar-ficha/:idFicha',
            component: EditarFichaComponent
        }]
    }
];

