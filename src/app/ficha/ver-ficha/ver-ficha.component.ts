import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FichaService } from 'src/app/shared/services/ficha.service';
import { Ficha } from 'src/app/shared/models/ficha';
import { ListarServiciosService } from 'src/app/servicio/listar-servicios.service';
import { MatTableDataSource } from '@angular/material';
import { ListaServicios, Servicio } from 'src/app/shared/models/lista-servicios';
import { tap, flatMap, mergeMap, switchMap } from 'rxjs/operators';
import { EnviarArchivoService } from 'src/app/shared/services/enviar-archivo.service';
import swal from 'sweetalert2';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-ver-ficha',
  templateUrl: './ver-ficha.component.html',
  styleUrls: ['./ver-ficha.component.css']
})
export class VerFichaComponent implements OnInit {
  private id: number;
  private ficha;
  private listaServicios;
  private listaArchivos;
  serviciosArray: Servicio[];
  dataSource: MatTableDataSource<Servicio>;
  displayedColumns: string[] = ['fecha', 'presupuesto', 'acciones'];


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fichaService: FichaService,
    private serviciosService: ListarServiciosService,
    private archivoService: EnviarArchivoService,
    private tituloService: Title
    ) { }

  ngOnInit() {
    this.tituloService.setTitle("Ver ficha");
    this.route.params.pipe(
      switchMap((params) =>{
        console.log("switchMap 1:", params);
        this.id = params.idFicha;
        return this.fichaService.obtenerFicha(this.id);
      }),
      switchMap((data) => {
        console.log("switchMap 2:", data);
        this.ficha = data; 
        return this.serviciosService.getServiciosFicha(this.id);
      }),
      switchMap((data) => {
        console.log("switchMap 3:", data);
        this.listaServicios = {} as ListaServicios;
        this.listaServicios.lista = {} as Servicio[];
        this.listaServicios.lista = data.lista;
        this.serviciosArray = this.listaServicios.lista;
        //console.log('arrayServicios=>', this.serviciosArray);
        this.dataSource = new MatTableDataSource(this.serviciosArray);
        this.listaServicios = data
        return this.archivoService.getArchivos(this.id);
      })
    ).subscribe(
      data=>{
        console.log("switchMap 4: ", data);
        this.listaArchivos = data.lista;
      }
    );
  }

  openModalEliminar(id){
    console.log("idFichaArchiv: ",id);
    swal({
      title: 'Desea eliminar el archivo?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.archivoService.eliminarArchivos(id).subscribe(
          data =>{
            this.modalEliminadoConExito();
          },
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'El archivo han sido eliminado con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    }).then((results) =>{
      window.location.reload();
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar el archivo!",
      text: "Ocurrio un error al intentar eliminar el archivo.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  pantallaEdicion(id){
    this.router.navigate(['servicio','editar-servicios',id]);
  }

}
