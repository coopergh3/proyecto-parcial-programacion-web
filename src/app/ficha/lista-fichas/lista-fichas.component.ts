import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, Inject, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ListaServicios, Servicio, IdFichaClinica,
  IdCliente, IdLocal, IdEmpleado, IdTipoProducto, IdCategoria
} from '../../shared/models/lista-servicios';

import { tap, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { merge, fromEvent } from 'rxjs';
import swal from 'sweetalert2';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Persona } from '../../persona'
import { ListaPersona } from '../../listaPersona'
import { FichaService } from 'src/app/shared/services/ficha.service';
import { Ficha, FichaLista } from 'src/app/shared/models/ficha';
import { ListarServiciosService } from 'src/app/servicio/listar-servicios.service';
import { SeleccionarPersonaDialog, SeleccionarEmpleadoDialog } from 'src/app/servicio/ver-servicios/ver-servicios.component';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { DialogPersonaComponent } from 'src/app/reserva/dialog-persona/dialog-persona.component';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-lista-fichas',
  templateUrl: './lista-fichas.component.html',
  styleUrls: ['./lista-fichas.component.css']
})
export class ListaFichasComponent implements OnInit {
  displayedColumns: string[] = ['fecha', 'profesional', 'cliente', 'categoria', 'subcategoria',
                               'acciones'];
  dataSource: MatTableDataSource<Ficha>;
  listaFichas: FichaLista;
  fichasArray: Ficha[];
  formGroup: FormGroup;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild('input', { static: false }) input: ElementRef;

  /*Filtrado */
  opcionSeleccionada = 'Empleado';
  clienteSeleccionado: Persona;
  empleadoSeleccionado: Persona;
  empleadoFiltrado: Persona;
  clienteFiltrado: Persona;

  public listaFiltro = [
    { value: 'Empleado', viewValue: 'Empleado' },
    { value: 'Cliente', viewValue: 'Cliente' }
  ];

  mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dic']; // Enum para los meses del 1 al 12 el 0 esta de relleno
  seleccionado: any[4]; // Guardar filtros de busqueda para separar paginado del filtro
  fechaDesde: Date;
  fechaHasta: Date;
  hoy: Date;



  /*End Filtrado */

  constructor(private fichasService: FichaService,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog,
    private changeDetectorRefs: ChangeDetectorRef,
    private tituloService: Title) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista de fichas");
    this.formGroup = new FormGroup({
      fechaDesde: new FormControl(''),
      fechaHasta: new FormControl(''),
      fecha: new FormControl(''),
      horaInicio: new FormControl(''),
      horaFin: new FormControl(''),
    });
    this.fichasService.obtenerFichas().subscribe(
      data => {
        console.log(data);
        this.listaFichas = {} as FichaLista;
        this.listaFichas.lista = {} as Ficha[];
        this.listaFichas.lista = data.lista;
        this.fichasArray = this.listaFichas.lista;
        //console.log('arrayServicios=>', this.serviciosArray);
        this.dataSource = new MatTableDataSource(this.fichasArray);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error => console.log(error)
    );
  }

  buscar() {
    const opcion = this.opcionSeleccionada;
    let desde;
    let hasta;
    if(this.formGroup.get('fechaDesde').value){
      desde = this.getFormatoFecha(this.formGroup.get('fechaDesde').value.toString());
    }
    if(this.formGroup.get('fechaHasta').value){
      hasta = this.getFormatoFecha(this.formGroup.get('fechaHasta').value.toString());
    }
    console.log(`fechaDesde=${desde} , fechaHasta=${hasta}`);
    let id;
    if (opcion === 'Empleado') {
      this.fichasService.obtenerFichasParams(this.clienteFiltrado, this.empleadoFiltrado, desde, hasta).subscribe(
        data => {
          console.log('data de Empleado=', data);
          this.dataSource.data = data.lista;

        },
        error => console.log("el error es =====>", error)
      );
    } else {
      id = this.clienteFiltrado.idPersona;
      this.fichasService.obtenerFichasParams(this.clienteFiltrado,this.clienteFiltrado, desde, hasta).subscribe(
        data => {
          console.log(`data de Empleado = ${data}`);
          this.dataSource.data = data.lista;
        },
        error => console.log("el error es =====>", error)
      );
    }

  }


  reset() {
    if (this.opcionSeleccionada === 'Empleado') {
      this.clienteFiltrado = null;
    } else {
      this.empleadoFiltrado = null;
    }
    this.empleadoSeleccionado = null;
    this.clienteSeleccionado = null;
  }

  limpiar() {
    this.clienteFiltrado = null;
    this.empleadoFiltrado = null;
    this.empleadoSeleccionado = null;
    this.clienteSeleccionado = null;
    console.log('antes del patchValue...');
    console.log('fecha desde=',this.formGroup.controls['fechaDesde'].value);
    console.log('fecha hasta=',this.formGroup.controls['fechaHasta'].value);
    this.formGroup.patchValue({
      fechaDesde: '',
      fechaHasta: ''
    });
    console.log('Despues del patchValue....');
    console.log('fecha desde=',this.formGroup.controls['fechaDesde'].value);
    console.log('fecha hasta=',this.formGroup.controls['fechaHasta'].value);
  }

  checkFechaDesde(e) {
    const desde = new Date(e.value.toString());
    const hasta = new Date(this.formGroup.get('fechaHasta').value.toString());

    if (desde > hasta) {
      this.formGroup.get('fechaHasta').setValue(desde);
    }
    this.fechaDesde = e.value;
  }

  getFormatoFecha(s: string): string {
    console.log("en getFormatoFecha, entra:", s);
    const anho = s.toString().substring(11, 15);
    let mesElegido = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mesElegido = (+mesElegido < 10) ? '0' + mesElegido : mesElegido;
    const dia = s.toString().substring(8, 10);
    console.log("en getFormatoFecha, sale:", anho + mesElegido + dia);
    return (anho + mesElegido + dia);
  }

  /**Parte delos dialogs**/
  openDialogCliente(): void {
      const dialogRef = this.dialog.open(DialogPersonaComponent, {
        width: '250px',
        data: { persona: this.clienteSeleccionado }/*{name: this.name, animal: this.animal}*/
      });

      dialogRef.afterClosed().subscribe(result => {
        console.log('Cliente =>', result);
        this.clienteSeleccionado = this.clienteFiltrado = result;
      });
  }
  

  openDialogEmpleado(): void {
    if (this.opcionSeleccionada === "Empleado") {
      const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
        width: '250px',
        data: { persona: this.empleadoSeleccionado }/*{name: this.name, animal: this.animal}*/
      });
      dialogRef.afterClosed().subscribe(result => {
        console.log('Empleado =>', result);
        this.empleadoSeleccionado = this.empleadoFiltrado = result;

      });
      //si no se trata de una persona normal
    }
  }

  openModalEliminar(id){
    swal({
      title: 'Desea eliminar esta categoria?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.fichasService.eliminarFicha(id).subscribe(
          data =>{
            this.modalEliminadoConExito();
          },
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'La categoria ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    }).then((results) =>{
      window.location.reload();
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar la categoria!",
      text: "Ocurrio un error al intentar eliminar la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  pantallaEdicion(id){
    this.router.navigate(['ficha','editar-ficha', id]);
  }

  agregarFicha(){
    this.router.navigate(['ficha','registrar-ficha']);
  }

  pantallaVer(id){
    this.router.navigate(['ficha','ver-ficha', id]);
  }

  agregarServicio(id){
    this.router.navigate(['servicio','crear-servicios', id])
  }

}