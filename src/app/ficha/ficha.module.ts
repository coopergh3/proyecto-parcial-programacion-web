import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrarFichaComponent } from './registrar-ficha/registrar-ficha.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../app.module';
import { MatTableModule, MatSortModule } from '@angular/material';
import { FichaRoutes } from './ficha.routing';
import { DialogPersonaComponent } from '../reserva/dialog-persona/dialog-persona.component';
import { DialogEmpleadoComponent } from '../reserva/dialog-empleado/dialog-empleado.component';
import { SeleccionarPersonaDialog, SeleccionarEmpleadoDialog } from '../servicio/ver-servicios/ver-servicios.component';
import { ListaFichasComponent } from './lista-fichas/lista-fichas.component';
import { VerFichaComponent } from './ver-ficha/ver-ficha.component';
import { EditarFichaComponent } from './editar-ficha/editar-ficha.component';

@NgModule({
  declarations: [ListaFichasComponent, VerFichaComponent, EditarFichaComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(FichaRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
  ],
})
export class FichaModule { }
