import { Component, OnInit, ViewChild } from '@angular/core';
import { Persona } from 'src/app/persona';
import { DialogPersonaComponent } from 'src/app/reserva/dialog-persona/dialog-persona.component';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { SubcategoriaService } from 'src/app/shared/services/subcategoria.service';
import { FichaService } from 'src/app/shared/services/ficha.service';
import swal from 'sweetalert2';
import { Ficha } from 'src/app/shared/models/ficha';
import { Router, ActivatedRoute } from '@angular/router';
import { SeleccionarPersonaDialog, SeleccionarEmpleadoDialog } from 'src/app/servicio/ver-servicios/ver-servicios.component';
import { tap } from 'rxjs/operators';
import { AgendaService } from 'src/app/servicios/agenda.service';
import { Reserva } from 'src/app/shared/models/reserva';
import { EnviarArchivoService } from 'src/app/shared/services/enviar-archivo.service';
import { Title } from '@angular/platform-browser';
import { ReservaEdit } from 'src/app/shared/models/reservaEdit';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'app-registrar-ficha',
  templateUrl: './registrar-ficha.component.html',
  styleUrls: ['./registrar-ficha.component.css']
})
export class RegistrarFichaComponent implements OnInit {
  private pacienteSeleccionado: Persona = null;
  private persona = null;
  private personaId = null;
  private personaNombre = null;
  private fisio: Persona = null;
  private fisioId = null;
  private fisioNombre = null;
  private categorias;
  private subcategorias;
  private ficha: Ficha;
  private idReserva: number;
  private reserva: Reserva;
  private reservaEditado: ReservaEdit;
  


  registrarFichaForm = new FormGroup({
    idCategoria: new FormControl(''),
    motivoConsulta: new FormControl(''),
    diagnostico: new FormControl(''),
    observacion: new FormControl(''),
    idTipoProducto: new FormControl(''),
    archivo: new FormControl(),
  });

  constructor(
    private dialog: MatDialog, 
    private recibirCategoriaService: RecibirCategoriaService,
    private recibirSubcategoriaService: SubcategoriaService,
    private fichaService: FichaService,
    private router: Router,
    private route: ActivatedRoute,
    private agendaService: AgendaService,
    private enviarArchivo: EnviarArchivoService,
    private tituloService: Title
    ) { }

  ngOnInit() {
    this.tituloService.setTitle("Registrar ficha");
    this.route.params.subscribe( params => this.idReserva = params.id);
    this.agendaService.getReserva(this.idReserva).subscribe(
      data =>{ 
        this.reserva = data;
        this.reservaEditado.idReserva = this.reserva.idReserva;
        this.reservaEditado.observacion = this.reserva.observacion;
        this.reservaEditado.flagAsistio = this.reserva.flagAsistio;
        this.fisio = this.reserva.idEmpleado;
        this.persona = this.reserva.idCliente;
      }
    );
    this.getCategorias();
  }

  getCategorias(): void {
    this.recibirCategoriaService.getCategorias().subscribe(
      data =>{ 
        this.categorias = data.lista;
        console.log("categoria en registrar-ficha",data);
      }
    );
  }

  getSubcategorias(idCategoria): void {
    console.log(idCategoria);
    this.recibirSubcategoriaService.getSubCategorias2(idCategoria).subscribe(
      data =>{ this.subcategorias = data.lista; console.log("subcategoria en registrar-ficha",data);}
    );
    console.log("subcategoria: ", this.subcategorias);
    console.log("persona: ",this.persona);
    console.log("fisio: ",this.fisio);

  }

  onSubmit(){
    console.log("registrar ficha form",this.registrarFichaForm);
    /*console.log("objeto ficha",this.ficha);
    this.ficha.diagnostico = this.registrarFichaForm.value.diagnostico;
    this.ficha.motivoConsulta = this.registrarFichaForm.value.motivoConsulta;
    this.ficha.observacion = this.registrarFichaForm.value.observacion;
    this.ficha.idTipoProducto = this.registrarFichaForm.value.idTipoProducto;
    this.ficha.idCliente = this.persona;
    console.log("antes de enviar", this.ficha);*/
    this.registrarFichaForm.removeControl('idCategoria');
    this.registrarFichaForm.removeControl('archivo');
    this.reserva.idEmpleado.fechaHoraAprobContrato = null;
    this.reserva.idEmpleado.fechaNacimiento = null;
    this.reserva.idCliente.fechaNacimiento = null;

    this.fisio.fechaHoraAprobContrato = null;
    this.fisio.fechaNacimiento = null;
    this.registrarFichaForm.value.idCliente = this.persona;
    this.registrarFichaForm.value.idEmpleado = this.fisio;
    this.fichaService.registrarFicha(this.registrarFichaForm.value).pipe(
      tap((data)=>{
        this.creadoCorrectamente();
        this.reservaEditado.flagAsistio = "S";
        this.agendaService.editReserva(this.reservaEditado);
      })
    ).subscribe(
      data => {
        this.enviar(this.archivoInput, data.idFichaClinica);
        
        this.router.navigate(['ficha','lista-fichas']);
      },
      error => console.log("No se pudo actualizar la reserva.")
      )
    
    /*.subscribe(
      data => {
        this.creadoCorrectamente();
        this.router.navigate(['ficha','lista-fichas']);
      },
      error => this.errorRecibido(error)
    )*/
  }

  creadoCorrectamente() {
    console.log("Se creo correctamente la ficha.");
    swal({
        title: "Categoria creada!",
        text: "Se creo correctamente la ficha!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  openDialogCliente(): void {
        const dialogRef = this.dialog.open(DialogPersonaComponent, {
            width: '250px',
            data: {id: this.personaId , nombre: this.personaNombre}
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed in cliente');
            if (result !== undefined) {
              console.log("result cliente=",result);
                this.persona = result;
            }
        });
  }

  openDialogFisioterapeuta(): void {
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
        width: '250px',
        data: {id: this.fisioId , nombre: this.fisioNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
        console.log('The dialog was closed in fisioterapeuta');
        if (result !== undefined) {
          console.log("result fisio: ", result);
            this.fisio = result;
        }
    });
  }

   /* MANEJO DE ARCHIVOS */

  @ViewChild('archivoInput', {static:false}) archivoInput;
  public isLoadingArchivo: boolean=false;
  public errorArchivo: boolean=false;
  public archivoName: string = 'Seleccione un archivo';

  public enviar(fileInput, idFichaClinica){
    let file: File= fileInput.nativeElement.files[0];
    this.enviarArchivo.enviarArchivo(file, idFichaClinica).subscribe(
      (resultPath) => {
        this.isLoadingArchivo=false;
        console.log("Se subio correctamente el archivo");
      },
      (error) => {
          this.errorArchivo=true;
          this.vaciarInputArchivo();
        console.log("Ha ocurrido un error al subir el archivo");
      }
    );
      
    }

  vaciarInputArchivo(){
    this.isLoadingArchivo=false;
    this.archivoInput.nativeElement.value='';
    this.archivoName='Seleccione un archivo';
  }

  public cargarArchivo(){
      if (this.archivoInput.nativeElement && this.archivoInput.nativeElement.files[0]) {
      this.errorArchivo=false;
      this.isLoadingArchivo=true;
      this.archivoName=this.archivoInput.nativeElement.files[0].name;
    } 
  }
  

}
