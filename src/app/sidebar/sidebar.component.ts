import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import {LoginService} from '../servicios/login.service';
import {Router} from '@angular/router';

declare const $: any;

// Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

// Menu Items
export const ROUTES: RouteInfo[] = [/*{
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },*/ {
        path: '/categoria',
        title: 'Categorias',
        type: 'sub',
        icontype: 'list',
        collapse: 'categorias',
        children: [
            { path: 'crear-categoria', title: 'Crear Categoria', ab: 'CC' },
            { path: 'ver-categoria', title: 'Listado de Categorias', ab: 'VC' },
        ]
    }, {
        path: '/subcategoria',
        title: 'Subcategorias',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'subcategorias',
        children: [
            {path: 'crear-subcategoria', title: 'Crear Subcategoria', ab: 'CSC'},
            {path: 'ver-subcategoria', title: 'Ver Subcategoria', ab: 'VSC'},
        ]
    },

    {
        path: '/productos',
        title: 'Productos',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'productos',
        children: [
            { path: 'crear-productos', title: 'Crear Producto', ab: 'CP' },
            //{ path: 'editar-productos', title: 'Editar Producto', ab: 'MP' },
            { path: 'productos', title: 'Ver|editar|eliminar productos', ab: 'VP' },
            //{ path: 'eliminar-producto', title: 'Eliminar Producto', ab: 'EP' },
        ]
    },

    {
        path: '/servicio',
        title: 'servicio',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'servicio',
        children: [
            //{ path: 'crear-productos', title: 'Crear Producto', ab: 'CP' },
            //{ path: 'editar-productos', title: 'Editar Producto', ab: 'MP' },
            { path: 'ver-servicios', title: 'Ver|editar|eliminar servicios', ab: 'VP' },
            //{ path: 'eliminar-producto', title: 'Eliminar Producto', ab: 'EP' },
        ]
    }, {
        path: '/paciente',
        title: 'Pacientes',
        type: 'sub',
        icontype: 'contacts',
        collapse: 'pacientes',
        children: [
            {path: 'crear-paciente', title: 'Crear Paciente', ab: 'CP'},
            //{path: 'modificar-paciente', title: 'Modificar Paciente', ab: 'MP'},
            {path: 'ver-paciente', title: 'Lista de Pacientes', ab: 'LP'},
            //{path: 'eliminar-paciente', title: 'Eliminar Paciente', ab: 'EP'},
        ]
    },{
        path: '/horario-atencion',
        title: 'HorarioAtencion',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'horario-atencion',
        children: [
            {path: 'crear-horario', title: 'Crear Horario', ab: 'CH'},
            {path: 'ver-horarios', title: 'Ver Horario', ab: 'VH'},
        ]
    },{
        path: '/horario-excepcion',
        title: 'HorarioExcepcion',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'horario-excepcion',
        children: [
            {path: 'agregar-registro', title: 'Agregar registro', ab: 'AR'},
            {path: 'ver-horarios', title: 'Ver Horarios', ab: 'VH'},
        ]
    },{
        path: '/reserva',
        title: 'Reservas',
        type: 'sub',
        icontype: 'assignment',
        collapse: 'reservas',
        children: [
            {path: 'agenda', title: 'Ver agenda', ab: 'VA'},
            {path: 'reservar', title: 'Reservar turno', ab: 'RT'},
        ]
    }, {
        path: '/comision',
        title: 'Comision',
        type: 'sub',
        icontype: 'list',
        collapse: 'comision',
        children: [
            { path: 'asignarComision', title: 'Crear comisión', ab: 'CC' },
            { path: 'listarComision', title: 'Listar comisión', ab: 'LC' },
        ]
    }, {
        path: '/ficha',
        title: 'Fichas',
        type: 'sub',
        icontype: 'label_important',
        collapse: 'fichas',
        children: [
            { path: 'lista-fichas', title: 'Listado de Fichas', ab: 'LF' },
        ]
    },
    {
        path: '/reporte',
        title: 'Reportes',
        type: 'sub',
        icontype: 'timeline',
        collapse: 'reporte',
        children: [
            { path: 'ver-reportes', title: 'Ver reportes Resumidos', ab: 'VRR' },
            { path: 'ver-reportesd', title: 'Ver reportes Detallados', ab: 'VRD' },
        ]
    }/*,{
        path: '/components',
        title: 'Components',
        type: 'sub',
        icontype: 'apps',
        collapse: 'components',
        children: [
            {path: 'buttons', title: 'Buttons', ab:'B'},
            {path: 'grid', title: 'Grid System', ab:'GS'},
            {path: 'panels', title: 'Panels', ab:'P'},
            {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
            {path: 'notifications', title: 'Notifications', ab:'N'},
            {path: 'icons', title: 'Icons', ab:'I'},
            {path: 'typography', title: 'Typography', ab:'T'}
        ]
    },{
        path: '/forms',
        title: 'Forms',
        type: 'sub',
        icontype: 'content_paste',
        collapse: 'forms',
        children: [
            {path: 'regular', title: 'Regular Forms', ab:'RF'},
            {path: 'extended', title: 'Extended Forms', ab:'EF'},
            {path: 'validation', title: 'Validation Forms', ab:'VF'},
            {path: 'wizard', title: 'Wizard', ab:'W'}
        ]
    },{
        path: '/tables',
        title: 'Tables',
        type: 'sub',
        icontype: 'grid_on',
        collapse: 'tables',
        children: [
            {path: 'regular', title: 'Regular Tables', ab:'RT'},
            {path: 'extended', title: 'Extended Tables', ab:'ET'},
            {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
        ]
    },{
        path: '/maps',
        title: 'Maps',
        type: 'sub',
        icontype: 'place',
        collapse: 'maps',
        children: [
            {path: 'google', title: 'Google Maps', ab:'GM'},
            {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
            {path: 'vector', title: 'Vector Map', ab:'VM'}
        ]
    },{
        path: '/widgets',
        title: 'Widgets',
        type: 'link',
        icontype: 'widgets'


},/* {
    path: '/reporte',
    title: 'Reportes',
    type: 'link',
    icontype: 'timeline'

}, {
    path: '/charts',
    title: 'Charts',
    type: 'link',
    icontype: 'timeline'

    },{
        path: '/calendar',
        title: 'Calendar',
        type: 'link',
        icontype: 'date_range'
    },{
        path: '/pages',
        title: 'Pages',
        type: 'sub',
        icontype: 'image',
        collapse: 'pages',
        children: [
            {path: 'pricing', title: 'Pricing', ab:'P'},
            {path: 'timeline', title: 'Timeline Page', ab:'TP'},
            {path: 'login', title: 'Login Page', ab:'LP'},
            {path: 'register', title: 'Register Page', ab:'RP'},
            {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
            {path: 'user', title: 'User Page', ab:'UP'}
        ]
    }*/
];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    /*private user: string;
    private usuario: string;*/
    public menuItems: any[];
    ps: any;
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    }

    constructor(private loginService: LoginService, private router: Router) {}

    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
    }
    updatePS(): void  {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

    getUser(): string {
        return this.loginService.getCurrentUser();
    }

    getNombreCompleto(): string {
        return this.loginService.getUsuario();
    }

    logout() {
        this.loginService.logoutUser();
        setTimeout(() => {
            this.router.navigate(['login']);
        }, 500);
    }
}
