import { Component, OnInit } from '@angular/core';
import { Categoria } from '../../shared/models/categoria';
import { CrearCategoriaService } from 'src/app/shared/services/crear-categoria.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-crear-categoria',
  templateUrl: './crear-categoria.component.html',
  styleUrls: ['./crear-categoria.component.css']
})
export class CrearCategoriaComponent implements OnInit {
  crearCategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
  });

  constructor(private crearCategoriaService: CrearCategoriaService,
    private router:Router,
    private tituloService: Title) { }

    ngOnInit() {
      this.tituloService.setTitle("Crear Categoria");
    }
  onSubmit() {
    this.crearCategoriaService.crearCategoria(this.crearCategoriaForm.value).subscribe(
      data =>{
        this.creadoCorrectamente(data);
        this.router.navigate(['categoria','ver-categoria']);
      },
      error => this.errorRecibido(error)
    )
  }

  creadoCorrectamente(data: Categoria) {
    console.log("Se creo correctamente la categoria"+data);
    swal({
        title: "Categoria creada!",
        text: "Se creo correctamente la categoria "+data.descripcion+"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }


}
