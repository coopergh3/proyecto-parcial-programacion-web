import { Component, OnInit } from '@angular/core';
import { Categoria, CategoriaPut } from '../../shared/models/categoria';
import { EditarCategoriaService } from 'src/app/shared/services/editar-categoria.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2';
import { ActivatedRoute, Router } from '@angular/router';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-modificar-categoria',
  templateUrl: './modificar-categoria.component.html',
  styleUrls: ['./modificar-categoria.component.css']
})
export class ModificarCategoriaComponent implements OnInit {
  private id: number;
  private cat: Categoria;
  private categoriaParaEnviar: CategoriaPut;
  editarCategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
  });

  constructor(private editarCategoriaService: EditarCategoriaService,
    private obtenerCategoriaServicio: RecibirCategoriaService,
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private tituloService: Title) { }

    ngOnInit() {
      this.tituloService.setTitle("Editar Categoria");
      this.route.params.subscribe( params => this.id = params.id);
      this.obtenerCategoriaServicio.getCategoria(this.id).subscribe(
        data =>{
          this.categoriaParaEnviar = data;
          this.editarCategoriaForm = this.formBuilder.group({
            idCategoria: this.categoriaParaEnviar.idCategoria,
            descripcion: this.categoriaParaEnviar.descripcion
          })
        }  
      );
    }

  onSubmit() {
    this.categoriaParaEnviar.descripcion = this.editarCategoriaForm.value.descripcion;
    this.editarCategoriaService.editarCategoria(this.categoriaParaEnviar).subscribe(
      data => {
        this.editadoCorrectamente(data);
        this.router.navigate(['categoria','ver-categoria']);
      },
      error => this.errorRecibido(error)
    )
  }



  editadoCorrectamente(data: Categoria) {
    console.log("Se editó correctamente la categoria "+data);
    swal({
        title: "Categoria modificada!",
        text: "Se edito correctamente la categoria "+data.descripcion+" !",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al modificar la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

}
