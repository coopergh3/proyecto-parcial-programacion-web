import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { CategoriaRoutes } from './categoria.routing';
import { CrearCategoriaComponent } from './crear-categoria/crear-categoria.component';
import { VerCategoriaComponent } from './ver-categoria/ver-categoria.component';
import { EliminarCategoriaComponent } from './eliminar-categoria/eliminar-categoria.component';
import { ModificarCategoriaComponent } from './modificar-categoria/modificar-categoria.component';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatSortModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CategoriaRoutes),
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    MatSortModule,
  ],
  declarations: [
    CrearCategoriaComponent, 
    VerCategoriaComponent, 
    EliminarCategoriaComponent, 
    ModificarCategoriaComponent,
  ]
})
export class CategoriaModule { }
