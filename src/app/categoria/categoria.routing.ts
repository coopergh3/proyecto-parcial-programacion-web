import { Routes } from '@angular/router';

import { CrearCategoriaComponent } from './crear-categoria/crear-categoria.component';
import { EliminarCategoriaComponent } from './eliminar-categoria/eliminar-categoria.component';
import { ModificarCategoriaComponent } from './modificar-categoria/modificar-categoria.component';
import { VerCategoriaComponent } from './ver-categoria/ver-categoria.component';
import { CategoriaResolver } from '../shared/services/categoria.resolver';

export const CategoriaRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'crear-categoria',
            component: CrearCategoriaComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'eliminar-categoria',
            component: EliminarCategoriaComponent
        }]
    }, {
      path: '',
      children: [ {
        path: 'editar-categoria/:id',
        component: ModificarCategoriaComponent,
        }]
    }, {
        path: '',
        children: [ {
            path: 'ver-categoria',
            component: VerCategoriaComponent
        }]
    }
];

