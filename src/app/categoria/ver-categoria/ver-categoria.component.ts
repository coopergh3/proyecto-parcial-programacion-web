import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RecibirCategoriaService } from 'src/app/shared/services/recibir-categoria.service';
import { EliminarCategoriaService } from 'src/app/shared/services/eliminar-categoria.service';
import { ListaCategoria } from 'src/app/shared/models/lista-categorias';
import { Categoria, CategoriaPut } from 'src/app/shared/models/categoria';
import swal from 'sweetalert2';
import { FormGroup, FormControl } from '@angular/forms';
import { EditarCategoriaService } from 'src/app/shared/services/editar-categoria.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator, MatTab } from '@angular/material';
import { tap, catchError, finalize, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, of, fromEvent, forkJoin } from 'rxjs';
import { CollectionViewer } from '@angular/cdk/collections';
import { Title } from '@angular/platform-browser';

declare var $: any;

export class TableData {
  headerRow: String[];
  dataRows: String[][];
}

@Component({
  selector: 'app-ver-categoria',
  templateUrl: './ver-categoria.component.html',
  styleUrls: ['./ver-categoria.component.css']
})
export class VerCategoriaComponent implements OnInit {
  public tableData: TableData;
  public searchTerm: string;
  public displayedColumns: string[] = ['idCategoria', 'descripcion', 'acciones']
  dataSource2: MatTableDataSource<any>;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild('filtroCategoria', {static: false}) filtroCategoria: ElementRef;


  public inicio: number = 0;
  public cantidad: number = 7;
  public pagina: number = 1;
  public orderBy: string = 'idCategoria';
  public sentido: string = 'asc';
  public totalDatos: number;
  public numPaginas: number; 

  public dataSource: CategoriaDataSource;

  public categorias: Categoria[];
  editarCategoriaForm = new FormGroup({
    descripcion: new FormControl(''),
  });
  
  constructor(
    private recibirCategoriaService: RecibirCategoriaService,
    private eliminarCategoriaService: EliminarCategoriaService,
    private editarCategoriaService: EditarCategoriaService,
    private router: Router,
    private route: ActivatedRoute,
    private tituloService: Title) { }

  ngOnInit() {
    this.tituloService.setTitle("Lista de Categorias");
    this.obtenerCategorias();
    console.log(this.route.snapshot);
    this.categorias = this.route.snapshot.data['categoria'];
    console.log("route: ", this.route.snapshot.data['categoria']);
    console.log("this.categoria", this.categorias);
    this.dataSource = new CategoriaDataSource(this.recibirCategoriaService);
    this.obtenerCategorias();
    this.dataSource.loadCategorias(0, 10, 'idCategoria','desc');
      /*this.obtenerCategorias();*/
  }

  ngAfterViewInit() {

    fromEvent(this.filtroCategoria.nativeElement,'keyup')
    .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
            this.paginator.pageIndex = 0;
            this.loadCategoriaPage();
        })
    )
    .subscribe();
    this.paginator.page.subscribe(data=>this.loadCategoriaPage());
    this.sort.sortChange.subscribe(data=>this.loadCategoriaPage());
  }

  loadCategoriaPage(){
    console.log("paginator=", this.paginator)
    console.log("sort= ", this.sort);
    this.dataSource.loadCategorias(
      this.paginator.pageIndex, 
      this.paginator.pageSize,
      this.sort.active,
      this.sort.direction,
      this.filtroCategoria.nativeElement.value
      ) 
  }


  goToPage(pagina: number){
    this.pagina = pagina;
    this.inicio = this.cantidad * (pagina - 1 );
    this.obtenerCategorias();
  }

  obtenerCategorias(){
    this.recibirCategoriaService.getCategorias2(this.inicio,this.cantidad,this.orderBy,this.sentido,'').subscribe(
      data => {
        console.log("data = ", data);
        this.categorias = data.lista;
        this.paginator.length = data.totalDatos;
        console.log("data.lista: ", data.lista);
        console.log("totalDatos en data: ", data.totalDatos);
        this.numPaginas = Math.round(this.totalDatos / this.cantidad);
      },
      error => this.errorRecibido(error)
    );
  }

  applyFilter(filterValue: string) {
    this.dataSource2.filter = filterValue.trim().toLowerCase();
  }

  ordenarID(sentidoOrden: string){
    this.sentido = sentidoOrden;
    this.obtenerCategorias();
  }

  errorRecibido(error){
    swal({
      title: "No se pudo obtener las categorias!",
      text: "Ocurrio un error al intentar obtener las categorias.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  getCategorias(data): String[][]{
    return data.lista.map((cat: Categoria)=>{
      return [cat.idCategoria, cat.descripcion];
    });
  }

  openModalEliminar(id){
    swal({
      title: 'Desea eliminar esta categoria?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.eliminarCategoriaService.eliminarCategoria(id).subscribe(
          data =>{
            this.modalEliminadoConExito();
            window.location.reload();
          },
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    swal({
        title: 'Eliminado!',
        text: 'La categoria ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar la categoria!",
      text: "Ocurrio un error al intentar eliminar la categoria.",
      showConfirmButton: true
    }).catch(swal.noop)
  }
  pantallaEdicion(id){
    this.router.navigate(['categoria','editar-categoria', id]);
  }

}


export class CategoriaDataSource implements DataSource<ListaCategoria> {

  private categoriaSubject = new BehaviorSubject<any>([]);
  private loadingSubject = new BehaviorSubject<boolean>(false);
  
  public loading$ = this.loadingSubject.asObservable();

  constructor(private categoriaService: RecibirCategoriaService) {}

  connect(collectionViewer: CollectionViewer): Observable<ListaCategoria[]> {
      return this.categoriaSubject.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
      this.categoriaSubject.complete();
      this.loadingSubject.complete();
  }

  loadCategorias(inicio = 0, cantidad = 10, orderBy = 'idCategoria', orderDir = 'asc', filtro = '') {

      this.loadingSubject.next(true);

      this.categoriaService.getCategorias2(inicio, cantidad, orderBy, orderDir, filtro).pipe(
          catchError(() => of([])),
          finalize(() => this.loadingSubject.next(false))
      )
      .subscribe(listaCategorias =>{
        console.log("en loadCategorias de paginator:", listaCategorias);
        this.categoriaSubject.next(listaCategorias.lista);
      });
  }    
}
