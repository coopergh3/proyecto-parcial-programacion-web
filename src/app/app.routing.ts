import { Routes } from '@angular/router';
import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './guards/auth.guard';
import { CategoriaResolver } from './shared/services/categoria.resolver';

export const AppRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    }, {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    }, {
        path: '',
        component: AdminLayoutComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                loadChildren: './dashboard/dashboard.module#DashboardModule'
            }, {
                path: 'categoria',
                loadChildren: './categoria/categoria.module#CategoriaModule'
            }, {
                path: 'subcategoria',
                loadChildren: './subcategoria/subcategoria.module#SubcategoriaModule'
            }, {
                path: 'horario-atencion',
                loadChildren: './horario-atencion/horario-atencion.module#HorarioAtencionModule'
            }, {
                path: 'horario-excepcion',
                loadChildren: './horario-excepcion/horario-excepcion.module#HorarioExcepcionModule'
            }, {
                path: 'paciente',
                loadChildren: './paciente/paciente.module#PacienteModule'
            },
            // añadi esta linea acá para probar si aparcece Productos
            {
                path: 'productos',
                loadChildren: './productos/productos.module#ProductosModule'
            }, {
                path: 'comision',
                loadChildren: './comision/comision.module#ComisionModule'
            }, {
                path: 'servicio',
                loadChildren: './servicio/servicio.module#ServicioModule'
            },
            {
                path: 'reporte',
                loadChildren: './reporte/reporte.module#ReporteModule'
            },
            {
                path: 'reserva',
                loadChildren: './reserva/reserva.module#ReservaModule'
            }, {
                path: 'ficha',
                loadChildren: './ficha/ficha.module#FichaModule'
            }/*,{
                path: 'components',
                loadChildren: './components/components.module#ComponentsModule'
            }, {
                path: 'forms',
                loadChildren: './forms/forms.module#Forms'
            }, {
                path: 'tables',
                loadChildren: './tables/tables.module#TablesModule'
            }, {
                path: 'maps',
                loadChildren: './maps/maps.module#MapsModule'
            }, {
                path: 'widgets',
                loadChildren: './widgets/widgets.module#WidgetsModule'
            }, {
                path: 'charts',
                loadChildren: './charts/charts.module#ChartsModule'
            }, {
                path: 'calendar',
                loadChildren: './calendar/calendar.module#CalendarModule'
            }, {
                path: '',
                loadChildren: './userpage/user.module#UserModule'
            }, {
                path: '',
                loadChildren: './timeline/timeline.module#TimelineModule'
            }*/
        ]
    }/*, {
        path: '',
        component: AuthLayoutComponent,
        canActivate: [AuthGuard],
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    }*/
];
