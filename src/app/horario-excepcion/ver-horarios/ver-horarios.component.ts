import { Component, OnInit } from '@angular/core';
import { HorarioExcepcion } from '../../shared/models/horario-excepcion';
import { HorarioExcepcionService } from '../../shared/services/horario-excepcion.service';

import swal from 'sweetalert2';
import { FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Persona } from 'src/app/persona';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogPersonaComponent} from '../../reserva/dialog-persona/dialog-persona.component';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { Title } from '@angular/platform-browser';


declare var $: any;
export class TableData {
  headerRow: String[];
  dataRows: HorarioExcepcion[];
}

export class PersonaData {
  idPersona: number;
  nombre: string;
}

@Component({
  selector: 'app-ver-horarios',
  templateUrl: './ver-horarios.component.html',
  styleUrls: ['./ver-horarios.component.css']
})
export class VerHorariosComponent implements OnInit {

  public tableData: TableData;
  public horarios: HorarioExcepcion[];
  public selectedHorario : HorarioExcepcion;

  filtradoForm = new FormGroup({
    fecha: new FormControl(''),
  });
  
  personaAux : PersonaData;
  empleadoId = null;
  empleadoNombre = '';
  filtrar = false;

  Empleados : Persona[] = [];

  //paginacion
  public inicio: number = 0;
  public cantidad: number = 5;
  public pagina: number = 1;
  public totalDatos: number;
  public numPaginas: number; 

  
  mes;
  hoy:Date;
  

  constructor(
    private horarioExcepcionService: HorarioExcepcionService,
    private router: Router,
    private dialog: MatDialog,
    private tituloService: Title
  ) { }

  ngOnInit() {
    this.tituloService.setTitle("Ver Horarios Excepcion");
    this.mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    /*this.hoy = new Date();
    this.hoy.setHours(0); this.hoy.setMinutes(0); this.hoy.setSeconds(0); this.hoy.setMilliseconds(0);
    console.log(this.hoy)
    this.filtradoForm.get('fecha').setValue(this.hoy);*/
    this.getHorariosExcepcion();
    

  }

  getHorariosExcepcion(){
    let fechaC = "";
    if(this.filtradoForm.get('fecha').value){
      fechaC =  this.getFormatoFecha(this.filtradoForm.get('fecha').value.toString());
      //this.filtradoForm.get('fecha').setValue(fechaC);
      console.log(fechaC);
    }
      
    this.horarioExcepcionService.getHorarios(this.inicio,this.cantidad,this.filtrar,fechaC,this.empleadoId).subscribe(
      data => {
        this.tableData = {
          headerRow: ['ID', 'Fecha','HoraApertura', 'HoraCierre','IntervaloMinutos','flagEsHabilitar','Empleado','Acción'],
          dataRows: data.lista,
        }

        this.totalDatos = data.totalDatos;
        //console.log(this.totalDatos)

        this.numPaginas = Math.ceil(this.totalDatos / this.cantidad);
        //console.log(this.numPaginas);
      },
    );
  }
  goToPage(pagina: number){
    this.pagina = pagina;
    this.inicio = this.cantidad * (pagina - 1 );
    this.getHorariosExcepcion();
  }

  buscar() {
    this.filtrar = true;
    this.inicio = 0;
    this.pagina = 1;
    this.getHorariosExcepcion();
  }

  onReset(){
    this.filtrar = false;
    this.personaAux = null;
    this.empleadoNombre = "";
    this.empleadoId = null;
    this.filtradoForm.reset();
    this.getHorariosExcepcion();
  }


  redirigir(id:number, page: string){
    if(page == 'add'){
      this.router.navigate(['horario-excepcion/agregar-registro']);
    }else{
      this.router.navigate(['horario-excepcion/editar-horario', id = id]);
    }
  }
  openModalEliminar(id){
    swal({
      title: 'Desea eliminar este horario?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      confirmButtonText: 'Si',
      cancelButtonText: 'Cancelar',
      buttonsStyling: false
    }).then((result) => {
      if (result.value) {
        this.horarioExcepcionService.DeleteHorarioExcepcion(id).subscribe(
          data => this.modalEliminadoConExito(),
          error => this.errorEliminacion()
        );

      }
    })
  }
  modalEliminadoConExito(){
    this.getHorariosExcepcion();
    swal({
        title: 'Eliminado!',
        text: 'El horario de atención se ha sido eliminada con exito.',
        type: 'success',
        confirmButtonClass: "btn btn-success",
        buttonsStyling: false
    })
  }
  errorEliminacion(){
    swal({
      title: "No se pudo eliminar el horario!",
      text: "Ocurrio un error al intentar eliminar el horario.",
      showConfirmButton: true
    }).catch(swal.noop)
  }

  openDialog(): void {
    
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;
      }
    });
  }

  getFormatoFecha(s: string): string {
    // const s = e.value.toString();
    const anho = s.toString().substring(11, 15);
    console.log(anho);
    let mes = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    console.log(mes);
    mes = (mes < 10) ? '0' + mes : mes;
    const dia = s.toString().substring(8, 10);
    console.log(dia);

    return (anho + mes + dia);
  }


}
