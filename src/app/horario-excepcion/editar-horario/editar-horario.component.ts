import { Component, OnInit } from '@angular/core';
import { HorarioExcepcionService } from '../../shared/services/horario-excepcion.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HorarioExcepcion, HorarioBody1 } from '../../shared/models/horario-excepcion';
import swal from 'sweetalert2';
import { Location } from '@angular/common';
import { Persona } from '../../persona';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-editar-horario',
  templateUrl: './editar-horario.component.html',
  styleUrls: ['./editar-horario.component.css']
})
export class EditarHorarioComponent implements OnInit {

  id: number;
  Horario: HorarioExcepcion;

  tipoHorario = [
    {clave: "habilitación",valor: "S"},
    {clave: "deshabilitación", valor: "N"}
  ]
  

  editarRegistroAtencionForm = new FormGroup({
    idHorarioExcepcion: new FormControl(''),
    fechaCadena: new FormControl(''),
    horaAperturaCadena: new FormControl(''),
    horaCierreCadena: new FormControl(''),
    flagEsHabilitar: new FormControl(''),
    idEmpleado: new FormControl(''),
    intervaloMinutos: new FormControl(''),
  });
  nombreEmpleado: String;
  mes;

  constructor(
    private horarioExcepcionService: HorarioExcepcionService,
    private route: ActivatedRoute,
    private location: Location,
    private tituloService: Title
  ) { }

  ngOnInit() {
    this.tituloService.setTitle("Editar Horario Excepcion");
    this.mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.route.paramMap.subscribe(params => {
      this.id = +params.get('id');
    });


    this.getHorarioExcepcion(this.id);
  }

  onSubmit(){

    let fechaC =  this.getFormatoFecha(this.editarRegistroAtencionForm.get('fechaCadena').value.toString());
    this.editarRegistroAtencionForm.get('fechaCadena').setValue(fechaC);


    
    this.horarioExcepcionService.UpdateHorario(this.editarRegistroAtencionForm.value).subscribe(
      res => this.editadoCorrectamente(res),
      error => this.errorRecibido(error)
    )
  }

  editadoCorrectamente(data: HorarioExcepcion){
    console.log("Se ha editado correctamente el horairo de excepción"+ data.fechaCadena);
    swal({
          title: "Horario Modificado!",
          text: "Se edito correctamente el horario+data.idHorarioExcepcion",
          buttonsStyling: false,
          confirmButtonClass: "btn btn-success",
          type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    swal({
      title: "No se pudo editar!",
      text: "Ocurrio un error al editar el horario: "+error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }

  timeFormat(time): String{
    return time.slice(0,5);
  }

  getHorarioExcepcion(id){
    this.horarioExcepcionService.getHorario(id).subscribe(res => 
      { this.Horario = res;
        this.nombreEmpleado = res.idEmpleado.nombreCompleto;
        console.log(res.fecha);
        let fecha = new Date(res.fecha+'T00:00:00');
        //fecha.setHours(0); fecha.setMinutes(0); fecha.setSeconds(0); fecha.setMilliseconds(0);
        console.log(fecha);
        this.editarRegistroAtencionForm.setValue({
          flagEsHabilitar: res.flagEsHabilitar,
          idHorarioExcepcion: res.idHorarioExcepcion,
          fechaCadena: fecha,
          horaAperturaCadena : this.timeFormat(res.horaApertura),
          horaCierreCadena: this.timeFormat(res.horaCierre),
          intervaloMinutos: res.intervaloMinutos,
          idEmpleado: res.idEmpleado,
        })
        console.log(this.editarRegistroAtencionForm.value);
  
      },
        error => {console.log(error);}
      )
  }

  goBack(){
    this.location.back();
  }  

  getFormatoFecha(s: string): string {
    // const s = e.value.toString();
    const anho = s.toString().substring(11, 15);
    console.log(anho);
    let mes = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    console.log(mes);
    mes = (mes < 10) ? '0' + mes : mes;
    const dia = s.toString().substring(8, 10);
    console.log(dia);

    return (anho + mes + dia);
  }


}
