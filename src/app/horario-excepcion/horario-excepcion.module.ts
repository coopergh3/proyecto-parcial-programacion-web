import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HorarioExcepcionRoutingModule } from './horario-excepcion-routing.module';
import { VerHorariosComponent } from './ver-horarios/ver-horarios.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app.module';
import { SubcategoriaModule } from '../subcategoria/subcategoria.module';
import { ReservaModule } from '../reserva/reserva.module';
import { AgregarRegistroComponent } from './agregar-registro/agregar-registro.component';
import { EditarHorarioComponent } from './editar-horario/editar-horario.component';

@NgModule({
  declarations: [VerHorariosComponent, AgregarRegistroComponent, EditarHorarioComponent],
  imports: [
    CommonModule,
    HorarioExcepcionRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SubcategoriaModule,
    ReservaModule
  ]
})
export class HorarioExcepcionModule { }
