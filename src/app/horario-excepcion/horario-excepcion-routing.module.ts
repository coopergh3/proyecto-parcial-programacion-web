import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerHorariosComponent } from './ver-horarios/ver-horarios.component';
import { AgregarRegistroComponent } from './agregar-registro/agregar-registro.component';
import { EditarHorarioComponent } from './editar-horario/editar-horario.component';

const routes: Routes = [
  {
    path: '',
    children: [ {
      path: 'agregar-registro',
      component: AgregarRegistroComponent
    }]
  },
  {
    path: '',
    children: [ {
      path: 'ver-horarios',
      component: VerHorariosComponent
    }]
  },
  {
    path: '',
    children: [ {
      path: 'editar-horario/:id',
      component: EditarHorarioComponent
    }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HorarioExcepcionRoutingModule { }
