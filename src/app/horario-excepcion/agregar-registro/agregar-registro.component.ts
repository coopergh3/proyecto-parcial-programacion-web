import { Component, OnInit } from '@angular/core';
import { HorarioExcepcion, HorarioBody1, HorarioBody2 } from '../../shared/models/horario-excepcion';
import { HorarioExcepcionService } from '../../shared/services/horario-excepcion.service';
import { FormGroup, FormControl } from '@angular/forms';
import swal from 'sweetalert2';
import { ListaPersona } from 'src/app/listaPersona';
import { Persona } from 'src/app/persona';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogPersonaComponent} from '../../reserva/dialog-persona/dialog-persona.component';
import { DialogEmpleadoComponent } from 'src/app/reserva/dialog-empleado/dialog-empleado.component';
import { Title } from '@angular/platform-browser';

export class PersonaData {
  idPersona: number;
  nombre: string;
}


@Component({
  selector: 'app-agregar-registro',
  templateUrl: './agregar-registro.component.html',
  styleUrls: ['./agregar-registro.component.css']
})
export class AgregarRegistroComponent implements OnInit {

  Empleados : Persona[];
  body : HorarioExcepcion;
  esHabilitar = true;
  hoy: Date;
  mes;

  //dialog
  personaAux : PersonaData;
  empleadoId = null;
  empleadoNombre = '';

  tipoHorario = [
    {clave: "habilitación",valor: "S"},
    {clave: "deshabilitación", valor: "N"}
  ]
  

  agregarRegistroAtencionForm = new FormGroup({
    fechaCadena: new FormControl(''),
    horaAperturaCadena: new FormControl(''),
    horaCierreCadena: new FormControl(''),
    flagEsHabilitar: new FormControl(''),
    idEmpleado: new FormControl(''),
    intervaloMinutos: new FormControl(''),
  });

  agregarRegistroNoAtencionForm = new FormGroup({
    fechaCadena: new FormControl(''),
    flagEsHabilitar: new FormControl(''),
    idEmpleado: new FormControl(''),
  });

  constructor(
    private horarioExcepcionService: HorarioExcepcionService,
    private dialog: MatDialog,
    private tituloService: Title
  ) {}

  ngOnInit() {
    this.tituloService.setTitle("Agregar Horario Excepcion");

    this.mes = ['.', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    this.hoy = new Date();
    this.hoy.setHours(0); this.hoy.setMinutes(0); this.hoy.setSeconds(0); this.hoy.setMilliseconds(0);
    console.log(this.hoy)
    this.agregarRegistroAtencionForm.get('fechaCadena').setValue(this.hoy);

    this.agregarRegistroAtencionForm.get('flagEsHabilitar').setValue("S");

    this.agregarRegistroAtencionForm.get('flagEsHabilitar').valueChanges.subscribe(
      flag => {       
        console.log(flag);
      }
    );

  }

  getEmpleado(id){
    this.horarioExcepcionService.getEmpleado(id).subscribe(
      data => { this.agregarRegistroAtencionForm.get("idEmpleado").setValue(data); },
      error => { console.log(error); }
    )
  }

  onSubmit() {
    //this.body = this.crearHorarioForm.value as HorarioAtencion;
    
    
    let fechaC =  this.getFormatoFecha(this.agregarRegistroAtencionForm.get('fechaCadena').value.toString());
    this.agregarRegistroAtencionForm.get('fechaCadena').setValue(fechaC);
    
    //this.body.horaAperturaCadena = this.body.horaAperturaCadena.replace(/:/g,'');
    console.log(this.agregarRegistroAtencionForm.value);
    this.horarioExcepcionService.agregarRegistro(this.agregarRegistroAtencionForm.value).subscribe(
      data => this.creadoCorrectamente(data),
      error => this.errorRecibido(error)
    );
  }

  creadoCorrectamente(data: HorarioBody1){
    console.log("Se ha añadido correctamente nuevo horario de atención"+data);
    swal({
        title: "Horario añadido!",
        text: "Se añadido correctamente el horario de excepcion en fecha:"+ data.fechaCadena+"!",
        buttonsStyling: false,
        confirmButtonClass: "btn btn-success",
        type: "success"
    }).catch(swal.noop)
  }

  errorRecibido(error){
    console.log(error);
    swal({
      title: "No se pudo crear!",
      text: "Ocurrio un error al crear el Horario: "+ error.error,
      showConfirmButton: true
    }).catch(swal.noop)
  }

  getFormatoFecha(s: string): string {
    // const s = e.value.toString();
    const anho = s.toString().substring(11, 15);
    let mes = this.mes.indexOf(s.toString().substring(4, 7)).toString();
    mes = (mes < 10) ? '0' + mes : mes;
    const dia = s.toString().substring(8, 10);

    return (anho + mes + dia);
  }

  openDialog(): void {
    
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;
        this.getEmpleado(result.id);
      }
    });
  
  }
}
