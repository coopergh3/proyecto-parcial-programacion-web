import { Component, OnInit, OnDestroy, ViewChild, HostListener, AfterViewInit } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { NavItem, NavItemType } from '../../md/md.module';
import { Subscription } from 'rxjs/Subscription';
import { Location, LocationStrategy, PathLocationStrategy, PopStateEvent } from '@angular/common';
import 'rxjs/add/operator/filter';
import { NavbarComponent } from '../../shared/navbar/navbar.component';
import PerfectScrollbar from 'perfect-scrollbar';

declare const $: any;

//Metadata
export interface RouteInfo {
  path: string;
  title: string;
  type: string;
  icontype: string;
  collapse?: string;
  children?: ChildrenItems[];
}

export interface ChildrenItems {
  path: string;
  title: string;
  ab: string;
  type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [{
      path: '/dashboard',
      title: 'Dashboard',
      type: 'link',
      icontype: 'dashboard'
  }, {
      path: '/categorias',
      title: 'Categorias',
      type: 'sub',
      icontype: 'label_important',
      collapse: 'categorias',
      children: [
          {path: 'crear-categoria', title: 'Crear Categoria', ab: 'CC'},
          {path: 'modificar-categoria', title: 'Editar Categoria', ab: 'MC'},
          {path: 'ver-categoria', title: 'Ver Categoria', ab: 'VC'},
          {path: 'eliminar-categoria', title: 'Eliminar Categoria', ab: 'EC'},
      ]
  }/*, {
      path: '/components',
      title: 'Components',
      type: 'sub',
      icontype: 'apps',
      collapse: 'components',
      children: [
          {path: 'buttons', title: 'Buttons', ab:'B'},
          {path: 'grid', title: 'Grid System', ab:'GS'},
          {path: 'panels', title: 'Panels', ab:'P'},
          {path: 'sweet-alert', title: 'Sweet Alert', ab:'SA'},
          {path: 'notifications', title: 'Notifications', ab:'N'},
          {path: 'icons', title: 'Icons', ab:'I'},
          {path: 'typography', title: 'Typography', ab:'T'}
      ]
  },{
      path: '/forms',
      title: 'Forms',
      type: 'sub',
      icontype: 'content_paste',
      collapse: 'forms',
      children: [
          {path: 'regular', title: 'Regular Forms', ab:'RF'},
          {path: 'extended', title: 'Extended Forms', ab:'EF'},
          {path: 'validation', title: 'Validation Forms', ab:'VF'},
          {path: 'wizard', title: 'Wizard', ab:'W'}
      ]
  },{
      path: '/tables',
      title: 'Tables',
      type: 'sub',
      icontype: 'grid_on',
      collapse: 'tables',
      children: [
          {path: 'regular', title: 'Regular Tables', ab:'RT'},
          {path: 'extended', title: 'Extended Tables', ab:'ET'},
          {path: 'datatables.net', title: 'Datatables.net', ab:'DT'}
      ]
  },{
      path: '/maps',
      title: 'Maps',
      type: 'sub',
      icontype: 'place',
      collapse: 'maps',
      children: [
          {path: 'google', title: 'Google Maps', ab:'GM'},
          {path: 'fullscreen', title: 'Full Screen Map', ab:'FSM'},
          {path: 'vector', title: 'Vector Map', ab:'VM'}
      ]
  },{
      path: '/widgets',
      title: 'Widgets',
      type: 'link',
      icontype: 'widgets'

  },{
      path: '/charts',
      title: 'Charts',
      type: 'link',
      icontype: 'timeline'

  },{
      path: '/calendar',
      title: 'Calendar',
      type: 'link',
      icontype: 'date_range'
  },{
      path: '/pages',
      title: 'Pages',
      type: 'sub',
      icontype: 'image',
      collapse: 'pages',
      children: [
          {path: 'pricing', title: 'Pricing', ab:'P'},
          {path: 'timeline', title: 'Timeline Page', ab:'TP'},
          {path: 'login', title: 'Login Page', ab:'LP'},
          {path: 'register', title: 'Register Page', ab:'RP'},
          {path: 'lock', title: 'Lock Screen Page', ab:'LSP'},
          {path: 'user', title: 'User Page', ab:'UP'}
      ]
  }*/
];

@Component({
  selector: 'app-layout',
  templateUrl: './admin-layout.component.html'
})

export class AdminLayoutComponent implements OnInit, AfterViewInit {
    public menuItems: any[];
    public navItems: NavItem[];
    private _router: Subscription;
    private lastPoppedUrl: string;
    private yScrollStack: number[] = [];
    url: string;
    location: Location;

    @ViewChild('sidebar', {static: false}) sidebar: any;
    @ViewChild(NavbarComponent, {static: false}) navbar: NavbarComponent;
    constructor( private router: Router, location: Location ) {
      this.location = location;
    }
    ngOnInit() {
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
        const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
        this.location.subscribe((ev:PopStateEvent) => {
            this.lastPoppedUrl = ev.url;
        });
         this.router.events.subscribe((event:any) => {
            if (event instanceof NavigationStart) {
               if (event.url != this.lastPoppedUrl)
                   this.yScrollStack.push(window.scrollY);
           } else if (event instanceof NavigationEnd) {
               if (event.url == this.lastPoppedUrl) {
                   this.lastPoppedUrl = undefined;
                   window.scrollTo(0, this.yScrollStack.pop());
               }
               else
                   window.scrollTo(0, 0);
           }
        });
        this._router = this.router.events.filter(event => event instanceof NavigationEnd).subscribe((event: NavigationEnd) => {
             elemMainPanel.scrollTop = 0;
             elemSidebar.scrollTop = 0;
        });
        const html = document.getElementsByTagName('html')[0];
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            let ps = new PerfectScrollbar(elemMainPanel);
            ps = new PerfectScrollbar(elemSidebar);
            html.classList.add('perfect-scrollbar-on');
        }
        else {
            html.classList.add('perfect-scrollbar-off');
        }

        this.navItems = [
          { type: NavItemType.NavbarLeft, title: 'Dashboard', iconClass: 'fa fa-dashboard' },

          {
            type: NavItemType.NavbarRight,
            title: '',
            iconClass: 'fa fa-bell-o',
            numNotifications: 5,
            dropdownItems: [
              { title: 'Notification 1' },
              { title: 'Notification 2' },
              { title: 'Notification 3' },
              { title: 'Notification 4' },
              { title: 'Another Notification' }
            ]
          },
          {
            type: NavItemType.NavbarRight,
            title: '',
            iconClass: 'fa fa-list',

            dropdownItems: [
              { iconClass: 'pe-7s-mail', title: 'Messages' },
              { iconClass: 'pe-7s-help1', title: 'Help Center' },
              { iconClass: 'pe-7s-tools', title: 'Settings' },
               'separator',
              { iconClass: 'pe-7s-lock', title: 'Lock Screen' },
              { iconClass: 'pe-7s-close-circle', title: 'Log Out' }
            ]
          },
          { type: NavItemType.NavbarLeft, title: 'Search', iconClass: 'fa fa-search' },

          { type: NavItemType.NavbarLeft, title: 'Account' },
          {
            type: NavItemType.NavbarLeft,
            title: 'Dropdown',
            dropdownItems: [
              { title: 'Action' },
              { title: 'Another action' },
              { title: 'Something' },
              { title: 'Another action' },
              { title: 'Something' },
              'separator',
              { title: 'Separated link' },
            ]
          },
          { type: NavItemType.NavbarLeft, title: 'Log out' }
        ];
    }
    ngAfterViewInit() {
        this.runOnRouteChange();
    }
    public isMap() {
        if (this.location.prepareExternalUrl(this.location.path()) === '/maps/fullscreen') {
            return true;
        } else {
            return false;
        }
    }
    runOnRouteChange(): void {
      if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
        const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
        const elemMainPanel = <HTMLElement>document.querySelector('.main-panel');
        let ps = new PerfectScrollbar(elemMainPanel);
        ps = new PerfectScrollbar(elemSidebar);
        ps.update();
      }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
}
