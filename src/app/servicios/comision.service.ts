import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LoginService} from './login.service';
import {ComisionCompleto, ComisionConTerapeuta, ComisionEditar, ComisionSinTerapeuta} from '../shared/models/comision';
import {Empleado} from '../usuario.model';
import {PresentacionProductoId} from '../shared/models/producto';
import {Observable} from 'rxjs';

declare class ListaComision {
  lista: ComisionCompleto[];
  totalDatos: number;
}

@Injectable({
  providedIn: 'root'
})
export class ComisionService {

  constructor(private http: HttpClient, private login: LoginService) { }

  urlComision = '/stock-pwfe/comisionEmpleado';

  getComision(idEmp: number, idPresentacion: number): Observable<ListaComision> {
    let empleado = null;
    let empleadoJSON = null;
    let presentacion = null;
    let presentacionJSON = null;
    let noNulo = null;

    if (idEmp !== null) {
      empleado = new Empleado();
      empleado.idEmpleado = {idPersona: idEmp};
      empleadoJSON = JSON.stringify(empleado);
      noNulo = empleadoJSON;
    }

    if (idPresentacion !== null) {
      presentacion = new PresentacionProductoId();
      presentacion.idPresentacionProducto = {idPresentacionProducto: idPresentacion};
      presentacionJSON = JSON.stringify(presentacion);
      noNulo = presentacionJSON;
    }

    let parametro;

    if (empleadoJSON !== null && presentacionJSON !== null) {
      parametro = {};
      parametro.ejemplo = [empleadoJSON, presentacionJSON];

      console.log(parametro);
    } else if (empleadoJSON !== null || presentacionJSON !== null) {
      parametro = {
        ejemplo: noNulo
      };
    } else { // los dos nulos
      return this.http.get<ListaComision>(this.urlComision);
    }

    return this.http.get<ListaComision>(this.urlComision, {params: parametro});
  }

  putComision(comision: ComisionEditar) {
    return this.http.put(this.urlComision, comision);
  }

  postComision(comision: ComisionSinTerapeuta) {
    const user: string = this.login.getCurrentUser();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'usuario': user
      })};
    console.log(user);

    const json = JSON.stringify(comision);
    console.log(json);
    return this.http.post(this.urlComision, json, httpOptions);
  }

  postComisionTerapeuta(comision: ComisionConTerapeuta) {
    const user: string = this.login.getCurrentUser();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'usuario': user
      })};

    console.log(user);

    const json = JSON.stringify(comision);
    console.log(json);
    return this.http.post(this.urlComision, json, httpOptions);
  }

}
