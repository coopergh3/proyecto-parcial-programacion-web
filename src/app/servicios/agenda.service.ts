import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import { ListaReserva } from '../shared/models/listaReserva';
import { Agenda } from '../shared/models/agenda';
import {ListaPersona} from '../listaPersona';
import { Reserva } from '../shared/models/reserva';
import {catchError} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material';
import {Persona} from '../persona';
import { Ficha } from '../shared/models/ficha';
import {ReservaPost, PersonaPost} from '../shared/models/reservaPost';
import {ReservaEdit} from '../shared/models/reservaEdit';
import {LoginService} from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AgendaService {
  private urlReserva = '/stock-pwfe/reserva';
  private urlPersona = '/stock-pwfe/persona';

  constructor(private http: HttpClient, private login: LoginService) { }

  getReservas(): Observable<ListaReserva> {
    return this.http.get<ListaReserva>(this.urlReserva);
  }
  getReserva(id): Observable<Reserva>{
    return this.http.get<Reserva>(this.urlReserva+"/"+id);
  }

  getPersona(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.urlPersona, {params: {
      orderBy: 'nombre',
      orderDir: 'asc'
    }});
  }

  getEmpleado(): Observable<ListaPersona> {
    return this.http.get<ListaPersona>(this.urlPersona, {params: {
      ejemplo: '{"soloUsuariosDelSistema":true}',
      orderBy: 'nombre',
      orderDir: 'asc'
    }});
  }

  postReserva(reserva: ReservaPost) {
    const user: string = this.login.getCurrentUser();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'usuario': user
      })};

    const json = JSON.stringify(reserva);
    console.log(json);

    return this.http.post(this.urlReserva, json, httpOptions).pipe(
        catchError(this.handleError)
    );

  }

  getReservasFiltro(idEmp: string, idCli: any, fechaDesde: string, fechaHasta: string, inicio, cantidad): Observable<ListaReserva> {
    const url = '/stock-pwfe/reserva';

    let aux = '{';

    if (idEmp !== null) {
        aux += '"idEmpleado":{"idPersona":' + idEmp + '},';
    }

    if (idCli !== null) {
        aux += '"idCliente":{"idPersona":' + idCli + '},';
    }

    aux += '"fechaDesdeCadena":"' + fechaDesde + '",';
    aux += '"fechaHastaCadena":"' + fechaHasta + '"}';

    return this.http.get<ListaReserva>(url, {params: {
        'ejemplo': aux,
        inicio: inicio,
        cantidad: cantidad,
        orderBy: 'idTipoProducto',
        orderDir: 'asc'}}
    ).pipe(
        catchError(this.handleError)
    );
  }

  getReservasAgregar(idEmp: any, fecha: string, check: boolean): Observable<Agenda[]> {
    let url = '/stock-pwfe/persona/';

    if (idEmp !== null) {
      url += idEmp.toString();
      url += '/agenda?';

      url += 'fecha=' + fecha;

      if (!check) {
        url += '&disponible=S';
      }

      return this.http.get<Agenda[]>(url).pipe(
          catchError(this.handleError)
      );
    }
  }

  deleteReserva(id: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })};

    console.log(this.urlReserva + '/' + id.toString());
    return this.http.delete(this.urlReserva + '/' + id.toString())
        .pipe(
            catchError(this.handleError)
        );
  }

  editReserva(reservaEdit: ReservaEdit) {
    const user: string = this.login.getCurrentUser();
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'usuario': user
    })};
    const json = JSON.stringify(reservaEdit);
    console.log('EditarReserva: ' + json);
    return this.http.put(this.urlReserva, json, httpOptions)
        .pipe(
            catchError(this.handleError)
        );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Ocurrio un error:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
          `El backend ha retornado ${error.status}, ` +
          `el body era: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
        'Algo malo paso; intente de nuevo mas tarde.');
  }
}
