import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {ListaPersona} from '../listaPersona';
import { User } from '../usuario.model';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private urlLogin = '/stock-pwfe/persona';


  constructor(private http: HttpClient) { }

  hacerLogin(valor: string): Observable<ListaPersona> {

    const aux = '{"usuarioLogin":"' + valor + '"}';
    const params = new HttpParams().set('ejemplo', aux);
    console.log(aux);

    console.log(aux);

    return this.http.get<ListaPersona>(this.urlLogin, {params: params});
  }

  setUser(user: string): void {
    // const user_string = JSON.stringify(new User(user));
    localStorage.setItem('user', user);
  }

  setUsuario(usuario): void {
    localStorage.setItem('usuario', usuario);
  }

  getUsuario(): string {
    return localStorage.getItem('usuario');
  }

  setUsuarioId(id): void {
    localStorage.setItem('id', id);
  }

  getUsuarioId(): string {
    return localStorage.getItem('id');
  }

  getCurrentUser(): string {
    const user_string = localStorage.getItem('user');
    if (!isNullOrUndefined(user_string)) {
      // return JSON.parse(user_string);
      return user_string;
    } else {
      return null;
    }
  }

  logoutUser() {
    // const accessToken = localStorage.getItem('accessToken');
    localStorage.removeItem('usuario');
    localStorage.removeItem('user');
  }
}
