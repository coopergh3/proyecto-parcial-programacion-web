import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

import { LoginService } from '../servicios/login.service';
import {Observable} from 'rxjs';
import { ListaPersona } from '../listaPersona';
import { User } from '../usuario.model';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  validUsernameLogin = false;
  validPasswordLogin = false;

  private autenticado = false;
  private intento = false;

  register: FormGroup;
  login: FormGroup;
  type: FormGroup;

  persona: ListaPersona;

  constructor(
      private formBuilder: FormBuilder,
      private loginService: LoginService,
      private router: Router,
      private tituloService: Title
  ) {
    if (this.loginService.getCurrentUser() !== null) {
      this.router.navigate(['reserva', 'agenda']);
    }
  }



  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field).valid && form.get(field).touched;
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'has-error': this.isFieldValid(form, field),
      'has-feedback': this.isFieldValid(form, field)
    };
  }

  isAutenticado() {
    return this.autenticado;
  }

  // Se presiono el boton de Ingresar almenos una vez. Se ha autenticado o no
  isPresionadoLogin() {

    if (this.intento && !this.autenticado) {
      return true;
    } else {
      return false;
    }
  }

  ngOnInit() {
    this.tituloService.setTitle("Login");
    this.login = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  onLogin(username: string) {
    this.intento = true;
    this.autenticado = true;
    this.loginService.hacerLogin(username).subscribe(pers => {
      this.persona = pers;
      if (this.persona.totalDatos > 0) {
        if (this.persona.lista[0].usuarioLogin !== null) {
          this.autenticado = true;
          // Inicio de sesion
          this.loginService.setUser(this.persona.lista[0].usuarioLogin.toString());
          this.loginService.setUsuario(this.persona.lista[0].nombreCompleto.toString());
            this.loginService.setUsuarioId(this.persona.lista[0].idPersona.toString());
          this.router.navigate(['reserva', 'agenda']);
        }
      } else {
        this.autenticado = false;
      }
    });
  }

  getUserLoggedIn() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  usernameValidationLogin(e) {
    if (e.length > 1) {
      this.validUsernameLogin = true;
    } else {
      this.validUsernameLogin = false;
    }
  }

  passwordValidationLogin(e) {
    if (e.length > 5) {
      this.validPasswordLogin = true;
    } else {
      this.validPasswordLogin = false;
    }
  }

}
