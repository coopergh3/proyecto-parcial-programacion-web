import {RouterModule, Routes} from '@angular/router';

import { VerComisionComponent } from './ver-comision/ver-comision.component';
import { AsignarComisionComponent } from './asignar-comision/asignar-comision.component';
import {NgModule} from '@angular/core';

export const ComisionRoutes: Routes = [
    {
        path: '',
        children: [ {
            path: 'listarComision',
            component: VerComisionComponent
        }]
    }, {
        path: '',
        children: [ {
            path: 'asignarComision',
            component: AsignarComisionComponent
        }]
    }
];
