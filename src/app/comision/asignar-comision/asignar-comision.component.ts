import {ComisionCompleto, ComisionConTerapeuta, ComisionSinTerapeuta} from '../../shared/models/comision';
import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import 'rxjs/add/operator/map';

import { AgendaService } from '../../servicios/agenda.service';
import {Reserva} from '../../shared/models/reserva';
import {Persona} from '../../persona';

import {MatDialog} from '@angular/material/dialog';
import {PersonaData} from '../../reserva/reservar/reservar.component';
import {DialogEmpleadoComponent} from '../../reserva/dialog-empleado/dialog-empleado.component';

import {DialogEditarComponent} from '../../reserva/dialog-editar/dialog-editar.component';
import {ReservaEdit} from '../../shared/models/reservaEdit';
import {EditarDatos} from '../../reserva/agenda/agenda.component';
import {Categoria} from '../../shared/models/categoria';
import {Subcategoria} from '../../shared/models/subcategoria';
import {RecibirCategoriaService} from '../../shared/services/recibir-categoria.service';
import {SubcategoriaService} from '../../shared/services/subcategoria.service';
import {ProductoService} from '../../shared/services/producto.service';
import {PresentacionProducto, PresentacionProductoId} from '../../shared/models/producto';
import {ComisionService} from '../../servicios/comision.service';
import {IdLocalDefecto} from '../../idLocalDefecto';
import {PersonaPost} from '../../shared/models/reservaPost';

export class PresentacionProductoLista {
  idPresentacionProducto: number;
  descripcion: string;
}

@Component({
  selector: 'app-asignar-comision',
  templateUrl: './asignar-comision.component.html',
  styleUrls: ['./asignar-comision.component.css']
})
export class AsignarComisionComponent implements OnInit {
  personaAux: PersonaData;

  categoriaSeleccionada: number = null;
  subcategoriaSeleccionada: number = null;
  presentacionSeleccionada: number = null;
  campoComision: number = null;

  empleadoId = null;
  empleadoNombre = '';

  public listaCategorias: {}[] = [];
  public listaSubCategorias: {}[] = [];
  public listaPresentacion: {}[] = [];
  public lista: string[][] = [];

  constructor(private agendaService: AgendaService,
              private categoriaService: RecibirCategoriaService,
              public dialog: MatDialog,
              private subcategoriaService: SubcategoriaService,
              private productoService: ProductoService,
              private comisionService: ComisionService,
  ) { }

  @Output() paginaEmitter: EventEmitter<number> = new EventEmitter();
  ngOnInit() {
    this.categoriaService.getCategorias().subscribe(data => {
      this.listaCategorias = this.getCategorias(data);
    }, error => {
      console.log(error);
    });
  }

  getCategorias(data) {
    return data.lista.map((cat: Categoria) => {
      return {id: cat.idCategoria, nombre: cat.descripcion};
    });
  }

  getSubCategorias(data) {
    return data.lista.map((sub: Subcategoria) => {
      return {id: sub.idTipoProducto, nombre: sub.descripcion};
    });
  }

  getPresentacion(data) {
    return data.lista.map((pres: PresentacionProductoLista) => {
      return {id: pres.idPresentacionProducto, nombre: pres.descripcion};
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;
      }
    });
  }

  getSubLista() {
    this.presentacionSeleccionada = null;
    this.subcategoriaSeleccionada = null;
    this.subcategoriaService.getsubcategoriaconCat(this.categoriaSeleccionada).subscribe(
        data => {
          this.listaSubCategorias = this.getSubCategorias(data);
          console.log(data);
        }, error => {
          console.log(error);
        });
  }

  getPresentacionLista() {
    this.presentacionSeleccionada = null;
    this.productoService.getPresentacionProducto(this.subcategoriaSeleccionada).subscribe(
        data => {
          this.listaPresentacion = this.getPresentacion(data);
          console.log('Presentaciones:');
          console.log(data);
        }, error => {
          console.log(error);
        });
  }

  reset() {
    this.empleadoId = null;
    this.empleadoNombre = null;

    this.categoriaSeleccionada = null;
    this.subcategoriaSeleccionada = null;
    this.presentacionSeleccionada = null;

    this.campoComision = null;
  }

  refrescar() {
    setTimeout(() => {
      this.reset();
    }, 500);
  }

  agregar() {
    console.log('Agregar:');
    console.log(this.empleadoId);
    console.log(this.presentacionSeleccionada);
    this.redondear();
    console.log(this.campoComision);

    if (this.empleadoId === null) {

      const comisionObj = new ComisionSinTerapeuta();
      comisionObj.idPresentacionProducto = {idPresentacionProducto: this.presentacionSeleccionada};
      comisionObj.porcentajeComision = this.campoComision;
      this.comisionService.postComision(comisionObj).subscribe(data => console.log(data));
    } else {

      const persona = new PersonaPost(this.empleadoId);
      const comisionObj = new ComisionConTerapeuta();

      comisionObj.idPresentacionProducto = {idPresentacionProducto: this.presentacionSeleccionada};
      comisionObj.idEmpleado = persona;
      comisionObj.porcentajeComision = this.campoComision;
      this.comisionService.postComisionTerapeuta(comisionObj).subscribe(data => console.log(data));
    }

    this.refrescar();
  }

  redondear() {
    this.campoComision = Math.round(this.campoComision * 100) / 100;
  }
}
