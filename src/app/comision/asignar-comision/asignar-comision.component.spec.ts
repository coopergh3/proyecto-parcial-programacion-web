import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarComisionComponent } from './asignar-comision.component';

describe('AsignarComisionComponent', () => {
  let component: AsignarComisionComponent;
  let fixture: ComponentFixture<AsignarComisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarComisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarComisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
