import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import 'rxjs/add/operator/map';

import { AgendaService } from '../../servicios/agenda.service';
import {Reserva} from '../../shared/models/reserva';
import {Persona} from '../../persona';

import {MatDialog} from '@angular/material/dialog';
import {PersonaData} from '../../reserva/reservar/reservar.component';
import {DialogEmpleadoComponent} from '../../reserva/dialog-empleado/dialog-empleado.component';

import {DialogEditarComponent} from '../../reserva/dialog-editar/dialog-editar.component';
import {ReservaEdit} from '../../shared/models/reservaEdit';
import {EditarDatos} from '../../reserva/agenda/agenda.component';
import {Categoria} from '../../shared/models/categoria';
import {Subcategoria} from '../../shared/models/subcategoria';
import {RecibirCategoriaService} from '../../shared/services/recibir-categoria.service';
import {SubcategoriaService} from '../../shared/services/subcategoria.service';
import {ProductoService} from '../../shared/services/producto.service';
import {PresentacionProducto, PresentacionProductoId} from '../../shared/models/producto';
import {ComisionService} from '../../servicios/comision.service';
import {ComisionCompleto} from '../../shared/models/comision';
import {IdLocalDefecto} from '../../idLocalDefecto';

declare class DataTable {
  headerRow: string[];
  dataRows: string[][];
}

declare class ListaComision {
    lista: ComisionCompleto[];
    totalDatos: number;
}

export class PresentacionProductoLista {
    idPresentacionProducto: number;
    descripcion: string;
}

@Component({
  selector: 'app-ver-comision',
  templateUrl: './ver-comision.component.html',
  styleUrls: ['./ver-comision.component.css']
})
export class VerComisionComponent implements OnInit {

  headerRow: string[] = ['Fecha', 'Producto', 'Empleado', 'Porcentaje'];
  personaAux: PersonaData;

  categoriaSeleccionada: number = null;
  subcategoriaSeleccionada: number = null;
  presentacionSeleccionada: number = null;

  filtrarPor = 'empleado';
  botonFiltro = 'presentación';

  empleadoId = null;
  empleadoNombre = '';
  seleccionado: any[]; // Guardar filtros de busqueda para separar paginado del filtro

  public inicio = 0;
  public cantidad = 5;
  public pagina = 1;
  public totalDatos: number;
  public numPaginas: number;

  public dataTable: DataTable;
  public listaCategorias: {}[] = [];
  public listaSubCategorias: {}[] = [];
  public listaPresentacion: {}[] = [];
  public lista: string[][] = [];

  constructor(private agendaService: AgendaService,
              private categoriaService: RecibirCategoriaService,
              public dialog: MatDialog,
              private subcategoriaService: SubcategoriaService,
              private productoService: ProductoService,
              private comisionService: ComisionService,
  ) { }

  @Output() paginaEmitter: EventEmitter<number> = new EventEmitter();
  ngOnInit() {

    this.categoriaService.getCategorias().subscribe(data => {
      this.listaCategorias = this.getCategorias(data);
      /*console.log(this.listaCategorias);*/
    }, error => {
      console.log(error);
    });

    this.dataTable = {
      headerRow: this.headerRow,
      dataRows: []
    };

    this.seleccionado = [null, null, null, null];
  }

  getCategorias(data) {
    return data.lista.map((cat: Categoria) => {
      return {id: cat.idCategoria, nombre: cat.descripcion};
    });
  }

  getSubCategorias(data) {
    return data.lista.map((sub: Subcategoria) => {
      return {id: sub.idTipoProducto, nombre: sub.descripcion};
    });
  }

  getPresentacion(data) {
    return data.lista.map((pres: PresentacionProductoLista) => {
      return {id: pres.idPresentacionProducto, nombre: pres.descripcion};
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogEmpleadoComponent, {
      width: '250px',
      data: {id: this.empleadoId, nombre: this.empleadoNombre}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in empleado');
      if (result !== undefined) {
        this.personaAux = result;
        this.empleadoNombre = result.nombre;
        this.empleadoId = result.id;

        this.categoriaSeleccionada = null;
        this.subcategoriaSeleccionada = null;
        this.presentacionSeleccionada = null;

        this.buscar();
      }
    });
  }

  siguiente() {
    this.pagina++;
    this.pasarPagina();
  }

  anterior() {
    this.pagina--;
    this.pasarPagina();
  }

  pasarPagina() {
    this.solicitud(this.seleccionado[0], this.seleccionado[3], false);
  }

  getComisiones(data): string[][] {
    return data.lista.map((com: ComisionCompleto) => {
        const empleado = (com.idEmpleado !== null) ? com.idEmpleado.nombreCompleto : null;
      return [
        com.fechaHora.toString().substring(0, 10),
        com.idPresentacionProducto.nombre,
        empleado,
        com.porcentajeComision,
      ];
    });
  }

  getSubLista() {
    this.empleadoNombre = null;
    this.empleadoId = null;

    this.presentacionSeleccionada = null;
    this.subcategoriaSeleccionada = null;
    this.subcategoriaService.getsubcategoriaconCat(this.categoriaSeleccionada).subscribe(
    data => {
      this.listaSubCategorias = this.getSubCategorias(data);
      console.log(data);
    }, error => {
      console.log(error);
    });
  }

  getPresentacionLista() {
    this.empleadoNombre = null;
    this.empleadoId = null;

    this.presentacionSeleccionada = null;
    this.productoService.getPresentacionProducto(this.subcategoriaSeleccionada).subscribe(
    data => {
      this.listaPresentacion = this.getPresentacion(data);
      console.log('Presentaciones:');
      console.log(data);
    }, error => {
      console.log(error);
    });
  }

  reset() {
    this.empleadoId = null;
    this.empleadoNombre = null;

    this.categoriaSeleccionada = null;
    this.subcategoriaSeleccionada = null;
    this.presentacionSeleccionada = null;

    this.seleccionado = [null, null, null, null, null];
  }

  refrescar() {
    setTimeout(() => {
      this.solicitud(this.seleccionado[0], this.seleccionado[3], false);
    }, 500);
  }

  buscar() {
    this.seleccionado = [
      this.empleadoId,
      this.categoriaSeleccionada,
      this.subcategoriaSeleccionada,
      this.presentacionSeleccionada,
    ];

    console.log(this.presentacionSeleccionada);

    this.solicitud(this.empleadoId, this.presentacionSeleccionada, true);
  }

  solicitud(empleadoId: any, presentacionId: any, actualizarCantPagina: boolean) {

    this.comisionService.getComision(empleadoId, presentacionId).subscribe(data => {

      console.log(data);

      const auxComision: ListaComision = data;

      this.dataTable = {
        headerRow: this.headerRow,
        dataRows: (auxComision.totalDatos === 0) ? [] : this.getComisiones(data),
      };

      if (actualizarCantPagina) {
        this.inicio = 0;
        this.totalDatos = data.totalDatos;
        this.pagina = (this.totalDatos > this.cantidad) ? 1 : 0;
        this.numPaginas = Math.round(this.totalDatos / this.cantidad);

        this.lista = [];
        for (let i = 0; i < this.cantidad && i < this.totalDatos; i++) {
            this.lista.push(this.dataTable.dataRows[this.inicio + i]);
        }
      } else {
        this.inicio = this.cantidad * (this.pagina - 1);

        this.lista = [];
        for (let i = 0; i < (this.cantidad + this.inicio) && (this.inicio + i) < this.totalDatos; i++) {
            this.lista.push(this.dataTable.dataRows[this.inicio + i]);
        }
      }
    }, err => {
      console.log(err);
    });
  }

  openDialogEditar(id: number, observacion: string, asistio: boolean): void {
    const dialogRef = this.dialog.open(DialogEditarComponent, {
      width: '250px',
      data: {observacion: observacion, asistio: asistio}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed in editar');
      if (result !== undefined) {
        const aux: EditarDatos = result;

        let asistencia = 'N';
        if (aux.asistio) {
          asistencia = 'S';
        }

        let obs = '';
        if (aux.observacion !== null) {
          obs = aux.observacion;
        }
        const reservaEdit: ReservaEdit = new ReservaEdit(id, obs, asistencia);
        this.agendaService.editReserva(reservaEdit).subscribe(data => console.log(data));

        this.refrescar();

        return;
      }
    });
  }

  filtrar() {
    if (this.filtrarPor === 'empleado') {
      this.filtrarPor = 'presentacion';
      this.botonFiltro = 'empleado';
    } else {
      this.filtrarPor = 'empleado';
      this.botonFiltro = 'presentación';
    }
    this.reset();
  }

  eliminar(id: any) {
    this.agendaService.deleteReserva(id).subscribe(data => console.log(data));

    this.refrescar();
  }

  editar(id: any, observacion: string, asistio: string) {
    const auxAsist = (asistio === 'S');
    this.openDialogEditar(id, observacion, auxAsist);
  }

  nuevaFicha(id: any) {
    console.log(id);

    // this.refrescar();
  }
}
