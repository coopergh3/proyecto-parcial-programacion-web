import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ComisionRoutes} from './comision.routing';
import { VerComisionComponent } from './ver-comision/ver-comision.component';
import { AsignarComisionComponent } from './asignar-comision/asignar-comision.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../app.module';
import {RouterModule} from '@angular/router';
import {DialogEmpleadoComponent} from '../reserva/dialog-empleado/dialog-empleado.component';
import {ReservaModule} from '../reserva/reserva.module';
import {DialogPersonaComponent} from '../reserva/dialog-persona/dialog-persona.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ComisionRoutes),
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ReservaModule,
  ],
  declarations: [VerComisionComponent, AsignarComisionComponent],
  entryComponents: [VerComisionComponent, AsignarComisionComponent, DialogEmpleadoComponent, DialogPersonaComponent]
})
export class ComisionModule { }
